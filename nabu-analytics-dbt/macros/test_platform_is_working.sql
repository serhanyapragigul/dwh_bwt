{%- test platform_is_working(model) -%}
/* Starting from v0.20.0, DBT fails a test if any table is returned in result.
The aim of this specific test is to check for any records in last hour. 
For this reason, the logic is reversed:
- If records are found -> return empty table -> force DBT to pass the test
- If no records found -> return table with one row -> force DBT to fail the test*/

{%- call statement('get_event_count', fetch_result=True) -%}
      SELECT COUNT(*) AS event_count
      FROM {{ model }} 
      WHERE kafka_insert_time BETWEEN TIMESTAMP_SUB(CURRENT_TIMESTAMP(), INTERVAL 1 HOUR) AND CURRENT_TIMESTAMP() 
            AND  CAST(partition_time AS DATE) = CURRENT_DATE()            
{%- endcall -%}
{%- if execute -%}
      {%- set event_count = load_result('get_event_count')['data'][0][0] -%}
{%- else -%}
      {%- set event_count = -1 -%}
{%- endif -%}
{%- if event_count > 0 -%}
      /* This query needs to return empty table for a test to pass.
      For tests executed with '--store-failures' flag, DBT puts this query inside a subquery of 'create table' statement.
      BigQuery throws error when columns in a 'create table' statement have no names -> 'random_column' name is used. */
      SELECT 1 AS random_column 
      FROM {{ model }}
      WHERE 1 = 0
{%- else -%}
      SELECT 'no_records' AS test_results
{%- endif -%}

{%- endtest -%}