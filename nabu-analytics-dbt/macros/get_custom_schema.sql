-- Custom schema name pattern
-- In Production:
-- If a custom schema is provided, a model's schema name should match the custom schema, rather than being concatenated to the target schema.
-- If no custom schema is provided, a model's schema name should match the target schema.
-- In other environments:
-- Build all models in the target schema, as in, ignore custom schema configurations.

{% macro generate_schema_name(custom_schema_name, node) -%}
    {{ generate_schema_name_for_env(custom_schema_name, node) }}
{%- endmacro %}