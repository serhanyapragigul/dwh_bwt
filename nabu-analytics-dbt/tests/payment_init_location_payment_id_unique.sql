{{
    config(
        enabled = true,
        severity = 'error',
        tags = 'logic_test' 
    )
}}
SELECT  piloc.payment_id, 
        COUNT(*) AS cnt
FROM (
    SELECT	JSON_EXTRACT_SCALAR(response, '$.paymentInitLocation') AS payment_init_location,  
            payment_id
    FROM {{ ref('dwh_payment_trace') }}
    WHERE type = 'PAYMENT_REQUESTED' 
        AND status = 'SUCCESSFUL' --payment_init_location is currently available for succesfull requested payments with payment type = DEPOSIT
            AND JSON_EXTRACT_SCALAR(response, '$.type') = 'DEPOSIT'
                AND JSON_EXTRACT_SCALAR(response, '$.paymentInitLocation') IS NOT NULL
) AS piloc -- payment_init_location
GROUP BY piloc.payment_id
HAVING count(*) > 1