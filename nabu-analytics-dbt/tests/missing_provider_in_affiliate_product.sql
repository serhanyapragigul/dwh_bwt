{{
    config(
        enabled = true,
        severity = 'error',
        tags = 'logic_test' 
    )
}}

WITH affiliate_product_classifications AS (
    SELECT  DISTINCT provider_type AS provider_name,
            main_classification_name 
    FROM {{ ref('dwh_affiliate_product') }}
),
d_game_classifications AS (
    SELECT  DISTINCT g.provider_name,
            g.main_classification_name
    FROM {{ ref('d_game') }} AS g
)
SELECT  provider_name,
        main_classification_name
FROM d_game_classifications
EXCEPT DISTINCT 
SELECT  provider_name,  
        main_classification_name 
FROM affiliate_product_classifications