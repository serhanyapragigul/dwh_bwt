{{
    config(
        enabled = true,
        severity = 'error',
        tags = 'daily_test' 
    )
}}

WITH dwh_layer_bonus_values AS (
    SELECT DISTINCT platform_bonus_status
    FROM {{ ref('dwh_bonus_status_mapping') }}
),
raw_layer_bonus_values AS (
    SELECT DISTINCT TRIM(body.status) AS platform_bonus_status
    FROM {{ source('raw', 'bonuses') }}
    WHERE   (type = 'CREATED') -- We want to detect 'CREATED' events with null bonus status
        OR (type = 'UPDATED' AND body.status is not null) --We don't want to detect 'UPDATED' events with null bonus status
),
raw_against_dwh AS (  
    SELECT  platform_bonus_status
    FROM    raw_layer_bonus_values
    EXCEPT DISTINCT
    SELECT  platform_bonus_status
    FROM    dwh_layer_bonus_values
),
final AS (
    SELECT platform_bonus_status 
    FROM raw_against_dwh
)

SELECT  *
FROM    final