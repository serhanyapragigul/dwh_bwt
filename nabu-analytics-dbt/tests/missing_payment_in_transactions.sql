{{
    config(
        enabled = true,
        severity = 'error',
        tags = 'logic_test' 
    )
}}

SELECT  payment_id 
FROM   {{ ref('f_payment') }} 
WHERE   ((payment_type = "DEPOSIT" 
            AND payment_status = "CAPTURED") 
                OR (payment_type = "WITHDRAWAL" 
                    AND payment_status = "APPROVED")) 
                        AND payment_id NOT IN (
                                SELECT internal_reference 
                                FROM {{ ref('f_transaction') }}
                                WHERE transaction_type IN ("DEPOSIT","WITHDRAWAL")
                            )
        AND payment_id NOT IN (
            SELECT ignore_key FROM {{ ref('dq_testignore') }}
            WHERE test_name in ('{{ model['alias'] }}', '{{ '.'.join(model['unique_id'].split('.')[0:3]) }}')
        )