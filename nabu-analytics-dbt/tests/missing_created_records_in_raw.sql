{{
    config(
        tags = 'daily_test' 
    )
}}

SELECT
    type,
    LAST_VALUE(body.status IGNORE NULLS) OVER (PARTITION BY body.id ORDER BY body.updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS status,
    body.providerType,
    body.id,
    body.lastTransaction.type AS last_trs_type,
    body.updated as body_updated,
    kafkaData.insertTime AS kafka_insert_time
FROM {{ source('raw', 'bets') }}
WHERE 
    /*Need to check only new bets from fresh partitions. 
    There are a lot of bets cancelled in November 2020, which don't have matching CREATED type record, so some kind of filterring by date is required */
    _PARTITIONTIME > TIMESTAMP_SUB(current_timestamp() , INTERVAL 30 day)
        AND body.id NOT IN (SELECT body.id FROM {{ source('raw', 'bets') }} WHERE TYPE = 'CREATED')