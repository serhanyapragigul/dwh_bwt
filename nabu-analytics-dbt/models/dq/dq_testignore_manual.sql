{{ 
        config(
            schema='dq', 
            materialized='table'
            ) 
}}

SELECT 
    ignore_key,
    test_name,
    is_permanent,
    reason
FROM {{ ref('dq_testignore_manual_seed') }}