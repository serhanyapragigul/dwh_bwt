{{	config(
		schema='netrefer_out', 
		materialized='view',
        tags=['daily']
	)
}}

SELECT  p.reference AS CustomerID,
        p.country_code AS CountryCode,
        p.affiliate_id AS AffiliateID,
        FORMAT_DATETIME('%Y-%m-%d %H:%M:%S', CAST(p.registration_date AS DATETIME)) AS RegistrationDate,
        abr.brand_id AS BrandID,
        p.user_ip AS RegistrationIP,
        CAST(p.registration_date AS DATE) AS data_date
FROM (
    SELECT  player_id,
            business_unit_id,
            reference,
            country_code,
            affiliate_id,
            affiliate_tag,
            registration_date,
            user_ip,
            ROW_NUMBER() OVER (PARTITION BY affiliate_tag ORDER BY registration_date) AS btag_rn
    FROM    {{ ref('d_player') }} 
) AS p
    LEFT JOIN {{ ref('dwh_affiliate_brand') }} AS abr
    ON abr.brand_uuid = p.business_unit_id
WHERE   (p.affiliate_id IN ('658512','658514') -- Google or Direct
/*  only first btag appearance goes to registrations
    Netrefer assumes that btag is unique
    Platform allows as many registrations as you wish by the same link (=same btag)
    Thus, we send all registrations from the second one on to registrations_direct but with correct affiliate_id
*/
            OR p.btag_rn > 1)
                AND abr.brand_id IS NOT NULL --filtering out new franchises which are not officialy launched yet, e.g., jackburst.com