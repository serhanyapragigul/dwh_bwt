  {{ config(schema='raw_reports')}}

with 
exchange_rates as (
	select 
        r.baseCurrency,
        r.currency,
        r.exchangeRate,
        r.rateStartDate,
        r.rateEndDate
	from (
		select
			x.baseCurrency
			, x.currency
			, cast(x.exchangeRate as numeric) as exchangeRate
			, cast(x.updated as timestamp) as rateStartDate
			, timestamp_sub(cast(LEAD(x.updated, 1, '9999-12-31') OVER (PARTITION BY x.baseCurrency, x.currency ORDER BY x.updated ASC) as timestamp), interval 1 millisecond) as rateEndDate
		from ( 
			select distinct 
				body.baseCurrency
				, body.updated
				, rates.key as currency
				, rates.value as exchangeRate
			from {{ source('raw', 'exchange_rates') }}
			left join unnest (body.rates) as rates
			where rates.key = 'EUR'
		) x
	) r
)


select 
	p.lastUpdated
	, p.created
	, p.paymentId
	, p.playerId
	, p.businessUnitId
	, bu.businessUnitName
	, p.paymentAccountId
	, pa.accountStatus
	, p.providerId
	, p.subProvider
	, p.methodType
	, pa.externalAccountId
	, p.paymentType
	, p.firstStatus
	, p.latestStatus
	, p.lastReason
	, p.isCaptured
	, p.capturedDate
	, p.amount
	, p.fee
	, p.currency
	, safe_cast(p.amount * erc.exchangeRate as numeric)  as amountEur
	, safe_cast(p.fee * erc.exchangeRate as numeric)  as feeEur
	, p.baseAmount
	, p.baseCurrency
	, safe_cast(p.baseAmount * erb.exchangeRate as numeric)  as baseAmountEur
	, p.selectedAmount
	, p.selectedFee
	, p.selectedCurrency
	, safe_cast(p.selectedAmount * ers.exchangeRate as numeric)  as selectedAmountEur
	, safe_cast(p.selectedFee * ers.exchangeRate as numeric)  as selectedFeeEur
	, p.kafkaInsertTime
	, p.kafkaPartition
	, p.kafkaOffset
	, p.kafkaTopic
from (
	select distinct 
		body.id as paymentId
		, LAST_VALUE (body.playerId IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as playerId
		, LAST_VALUE (body.businessUnitId IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as businessUnitId
		, LAST_VALUE (body.providerId IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as providerId
		, LAST_VALUE (body.subProvider IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as subProvider
		, LAST_VALUE (body.methodType IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as methodType
		, LAST_VALUE (body.type IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as paymentType
		, FIRST_VALUE (body.updated IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as created
		, FIRST_VALUE (body.status IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as firstStatus
		, LAST_VALUE (body.updated IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as lastUpdated
		, LAST_VALUE (body.status IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as latestStatus
		, LAST_VALUE (body.reason IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as lastReason
		, LAST_VALUE (body.status IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) = 'CAPTURED' as isCaptured
		, LAST_VALUE (IF(body.status = 'CAPTURED', body.updated, null) IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as capturedDate
		, SAFE_CAST(LAST_VALUE (body.amount IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as numeric) as amount
		, SAFE_CAST(LAST_VALUE (body.fee IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as numeric) as fee
		, LAST_VALUE (body.currency IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as currency
		, SAFE_CAST(LAST_VALUE (body.baseAmount IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as numeric) as baseAmount
		, LAST_VALUE (body.baseCurrency IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as baseCurrency
		, SAFE_CAST(LAST_VALUE (body.selectedAmount IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as numeric) as selectedAmount
		, SAFE_CAST(LAST_VALUE (body.selectedFee IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as numeric) as selectedFee
		, LAST_VALUE (body.selectedCurrency IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as selectedCurrency
		, LAST_VALUE (body.paymentAccountId IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as paymentAccountId
		, LAST_VALUE (kafkadata.insertTime IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as kafkaInsertTime
		, LAST_VALUE (kafkadata.partition IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as kafkaPartition
		, LAST_VALUE (kafkadata.offset IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as kafkaOffset
		, kafkadata.topic as kafkaTopic
	from {{ source('raw', 'payments_v2') }}
	where kafkadata.insertTime >= coalesce(
		(select max(kafka_insert_ts) from {{ source('raw_reports', 'rr_report_log') }} where process_name = 'payments_report_15_mins' and successful limit 1)
		, timestamp_sub(current_timestamp(), interval 15 minute))
) p

left join (
	select distinct
		body.id as businessUnitId
		, LAST_VALUE (body.name IGNORE NULLS ) OVER (PARTITION BY body.id ORDER by body.updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as businessUnitName
	from {{ source('raw', 'business_units') }}
) bu
	on p.businessUnitId = bu.businessUnitId

left join (
	select distinct 
		body.id as paymentAccountId
		, body.accountStatus as accountStatus
		, body.updated as statusStartDate
		, body.externalAccountId
		, LEAD(body.updated, 1, CURRENT_TIMESTAMP()) OVER (PARTITION BY body.id ORDER by body.updated asc) as statusEndDate
	from {{ source('raw', 'payment_accounts_v2') }} 
) pa
	on p.paymentAccountId = pa.paymentAccountId
	and p.lastUpdated >= pa.statusStartDate
	and p.lastUpdated < pa.statusEndDate

left join exchange_rates erc
	on p.currency = erc.baseCurrency and coalesce(p.capturedDate, p.created) between erc.rateStartDate and erc.rateEndDate

left join exchange_rates erb
	on p.baseCurrency = erb.baseCurrency and coalesce(p.capturedDate, p.created) between erb.rateStartDate and erb.rateEndDate
	
left join exchange_rates ers
	on p.selectedCurrency = ers.baseCurrency and coalesce(p.capturedDate, p.created) between ers.rateStartDate and ers.rateEndDate 

