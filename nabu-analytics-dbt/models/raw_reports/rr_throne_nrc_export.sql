  {{ config(schema='raw_reports')}}

SELECT 
body.playerid AS Guid,
body.businessUnitId AS Buid,
bu.business_unit_name AS Buid_Name, -- join from dm.business_unit
body.email AS User_Email,
body.firstName AS User_Name,
body.lastName AS User_Surname,
body.phoneCode AS Phone_Code,
body.phoneNumber AS Phone_Number,
body.created AS Registration_Date,
body.userCountry AS User_Country,
CASE WHEN p.testAccount THEN 'Y' ELSE 'N' END AS Test_Account -- join from raw.players
 FROM {{ source('raw', 'player_registrations') }} AS reg

 LEFT JOIN (
    SELECT DISTINCT 
        body.id,
        body.testAccount
    FROM {{ source('raw', 'players') }}
  ) AS p
 ON reg.body.playerid = p.id

 LEFT JOIN {{ref('d_business_unit')}} AS bu 
 ON reg.body.businessUnitId = bu.business_unit_id
  AND reg.body.created BETWEEN bu.start_date AND bu.end_date
