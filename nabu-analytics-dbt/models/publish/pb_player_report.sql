{{ config(schema='publish', materialized='table')}}

SELECT player.registration_date,
	   player.player_id,
	   player.business_unit_id,
	   player.reference,
	   player.first_name,
	   player.last_name,
	   REGEXP_REPLACE(NORMALIZE(CONCAT(player.first_name, ' ', player.last_name), NFD), r"\pM", '') AS name_romanised,
	   player.email,
	   player.phone_number,
	   player.gender,
	   player.player_status,
	   player.address,
	   player.birth_date,
	   player.city,
	   player.language,
	   player.brand_name,
	   player.merchant_name,
	   player.franchise_name as franchise,
	   player.test_account,
	   player.user_ip,
	   player.product_segment,
	   player.sub_segment,
	   player.is_bonus_abuser,
	   CASE WHEN player.sb_realm_player_color != 'N/A' 
	   			THEN player.sb_realm_player_color
			WHEN lifetime_bet_sportsbook_EUR > 0 
				THEN 'N/D'
			ELSE 'N/A'
	   END AS sb_realm_player_color,
	   session.mobile_phone,
	   session.desktop,
	   session.tablet,
	   session.unknown,
	   session.ebook_reader,
	   session.mobile_device,
	   session.tv_device,
	   session.registration_device,
	   player.kyc_status,
	   player.kyc_verified_date,
	   player.all_statuses,
	   player.country_code as player_country_code,
	   player.country_name as player_country_name, 
	   CASE WHEN first_deposit_date is not NULL THEN TRUE ELSE FALSE END as is_ndc,
	   rt.provider_name as first_deposit_provider_name,
	   CASE WHEN (CASE WHEN first_deposit_date is not NULL THEN TRUE ELSE FALSE END) IS TRUE AND (bet.last_active_date > rt.first_deposit_date) THEN DATE_DIFF(DATE (CURRENT_DATE()), DATE(bet.last_active_date), DAY) ELSE NULL END as days_since_last_active,
	   CASE WHEN (CASE WHEN first_deposit_date is not NULL THEN TRUE ELSE FALSE END) IS TRUE AND (bet.last_active_date > rt.first_deposit_date) THEN bet.last_active_date ELSE NULL END as last_active_date,
	   played.first_played_product as first_played_product,
	   played.first_played_subproduct as first_played_subproduct,
	   played.first_played_product_date as first_played_product_date,
	   played.second_played_product as second_played_product,
	   played.second_played_subproduct as second_played_subproduct,
	   played.second_played_product_date as second_played_product_date,
	   played.third_played_product as third_played_product,
	   played.third_played_subproduct as third_played_subproduct,
	   played.third_played_product_date as third_played_product_date,
	   player.value_segment,
	   player.value_seg_30d_highest,
	   player.value_seg_ever_highest,
	   player.postal_code,
	   player.currency_code,
	   player.affiliate_tag,
	   player.affiliate_id,
	   /* player_segmentation */
       ps.payment_first_method_type, 
       ps.payment_first_method_score, 
       ps.payment_second_method_type, 
       ps.payment_second_method_score, 
       ps.payment_recommendation,
	   ps.payment_first_generic_method_offer,
       ps.payment_second_generic_method_offer,
       ps.payment_third_generic_method_offer,
       ps.churn_last_evaluation_day,
       ps.churn_inactive_days,
       ps.churn_segment, 
       ps.churn_segment_previous, 
       ps.most_staked_sport,
       ps.most_staked_league,
       ps.most_staked_market,
       ps.most_staked_team,
       ps.sb_sub_odd_preference,
       ps.sb_sub_stake_preference,
       ps.sb_sub_bet_type,
       ps.sb_sub_played_sport_variety,
       ps.sb_sub_execution_date,
       ps.sb_sub_execution_time, 

		affiliate_channel.channel,
		session.last_login_date,
		DATE_DIFF(DATE (CURRENT_DATE()), DATE(last_login_date), DAY) as days_since_last_login,
		DATE_DIFF(DATE (CURRENT_DATE()), DATE(rt.first_deposit_date), DAY) as days_since_first_deposit,
		rt.deposit_amount_EUR,
		rt.withdrawal_amount_EUR,
		rt.deposit_count,
		rt.deposit_count_captured,
		rt.avg_deposit_captured_EUR,
		rt.withdrawal_count,
		rt.withdrawal_count_captured,
		rt.avg_withdrawal_captured_EUR,
		transact.balance_EUR,
		rt.lifetime_bet_goc_EUR,
		rt.lifetime_bet_sportsbook_EUR,
		rt.lifetime_bet_goc_lotto_EUR,
		rt.lifetime_bet_goc_live_casino_EUR,
		rt.lifetime_bet_goc_casino_EUR,
		rt.lifetime_win_amount_goc_EUR,
		rt.lifetime_win_amount_sb_EUR,
		CASE WHEN rt.LastActiveGameOfChance>rt.first_deposit_date THEN rt.LastActiveGameOfChance ELSE NULL END as LastActiveGameOfChance,
		rt.lifetime_game_win_eur,
		rt.lifetime_turnover_eur,
		rt.lifetime_accounting_revenue_eur,
		rt.last_played_sportsbook_date,
		rt.first_deposit_date,
		rt.last_deposit_date,
		rt.first_withdrawal_date,
		rt.last_withdrawal_date,
		rt.last_deposit_amount_EUR,
		rt.first_deposit_amount_EUR,
		rt.first_withdrawal_amount_EUR,
		rt.first_deposit_method,
		rt.active_days_on_website,
		bonus.total_bonus_amount_eur,
		bonus.lifetime_given_bonus_amt_eur,
		bonus.first_bonus_configuration_id,
		bonus.first_bonus_id, 
		bonus.first_bonus_payment_id,
		bonus.first_bonus_amount_eur,
		--payment.provider_name as first_bonus_provider_name,
		COALESCE(rt.free_bet_bonus_cost_eur,0.0) as free_bet_bonus_cost_eur,
		COALESCE(rt.free_spin_bonus_cost_eur,0.0) as free_spin_bonus_cost_eur,
		COALESCE(rt.risk_free_bet_bonus_cost_eur,0.0) as risk_free_bet_bonus_cost_eur,
		COALESCE(rt.bonus_gamewin_eur,0.0) as bonus_gamewin,
		COALESCE(rt.bonus_winnings_eur,0.0) as bonus_winnings,
		COALESCE(rt.adjustment_bonus_eur,0.0) as discount_payouts,
		
		COALESCE(rt.total_bonus_cost_eur,0.0) as total_bonus_cost, -- Need to rename to total_bonus_cost_eur
		mostPlayed.avg_odds,
		mostPlayed.biggest_odds_win_sb,
		fold.fold_count,
		case when rt.night_bettor > rt.day_bettor and rt.night_bettor>primetime_bettor then 'night_bettor'
			 when rt.day_bettor > rt.night_bettor and rt.day_bettor > rt.primetime_bettor then 'day_bettor'
			 when rt.primetime_bettor > rt.day_bettor and rt.primetime_bettor > rt.night_bettor then 'primetime_bettor'
			 when rt.night_bettor IS NULL and rt.day_bettor IS NULL and rt.primetime_bettor IS NULL then NULL
			 ELSE 'Other' END AS bettor_type,
		mostPlayed.ratio_mobile,
		mostPlayed.ratio_desktop,
		rt.average_bet_stake_eur_sb,
		rt.biggest_bet_amount_eur_sb,
		rt.biggest_win_amount_eur_sb,
		bet.biggest_win_coupon,
		rt.nr_days_played_sb,
		bet.top_played_day,
		rt.lifetime_bet_count,
		rt.lifetime_bet_count_SB,
		rt.lifetime_bet_count_goc,
		rt.lifetime_bet_count_goc_casino_EUR,
		rt.lifetime_bet_count_goc_live_casino_EUR,
		rt.lifetime_bet_count_goc_lotto_EUR,
		mostPlayed.most_played_team,
		mostPlayed.most_played_sport,
		mostPlayed.most_played_league,
		mostPlayed.most_played_market,
		COALESCE(rt.bonus_ratio_7d, 0) AS bonus_ratio_7d,
		COALESCE(rt.bonus_ratio_14d, 0) AS bonus_ratio_14d,
		COALESCE(rt.bonus_ratio_30d, 0) AS bonus_ratio_30d,
		COALESCE(rt.bonus_ratio_lifetime, 0) AS bonus_ratio_lifetime

  FROM {{ ref('d_player') }}  AS player

  LEFT 
  JOIN {{ ref('d_player_segmentation')}} ps
    ON player.player_id = ps.player_id
   AND player.business_unit_id = ps.business_unit_id
 

left JOIN
(select
	DISTINCT
	player_id,
	business_unit_id,
	SUM(mobile_phone) OVER(partition by player_id, business_unit_id) as mobile_phone,
	SUM(desktop) OVER(partition by player_id, business_unit_id) as desktop,
	SUM(tablet) OVER(partition by player_id,  business_unit_id) as tablet,
	SUM(unknown) OVER(partition by player_id, business_unit_id) as unknown,
	SUM(ebook_reader) OVER(partition by player_id,  business_unit_id) as ebook_reader,
	SUM(mobile_device) OVER(partition by player_id,  business_unit_id) as mobile_device,
	SUM(tv_device) OVER(partition by player_id,  business_unit_id) as tv_device,
	last_login_date,
	registration_device
from(	
select 
	DISTINCT 
	player_id,
	session_id,
	business_unit_id, 
	COUNT(CASE WHEN device_type = 'Mobile Phone' THEN device_type else NULL END) OVER(partition by player_id, session_id, business_unit_id) as mobile_phone,
	COUNT(CASE WHEN device_type = 'Desktop' THEN device_type else NULL END) OVER(partition by player_id, session_id, business_unit_id) as desktop,
	COUNT(CASE WHEN device_type = 'Tablet' THEN device_type else NULL END) OVER(partition by player_id, session_id, business_unit_id) as tablet,
	COUNT(CASE WHEN device_type = 'Unknown' THEN device_type else NULL END) OVER(partition by player_id, session_id, business_unit_id) as unknown,
	COUNT(CASE WHEN device_type = 'Ebook Reader' THEN device_type else NULL END) OVER(partition by player_id, session_id, business_unit_id) as ebook_reader,
	COUNT(CASE WHEN device_type = 'Mobile Device' THEN device_type else NULL END) OVER(partition by player_id, session_id, business_unit_id) as mobile_device,
	COUNT(CASE WHEN device_type = 'TV Device' THEN device_type else NULL END) OVER(partition by player_id, session_id, business_unit_id) as tv_device,
	max(session_start) OVER(partition by player_id, business_unit_id) as last_login_date,
	FIRST_VALUE(device_type) OVER(PARTITION BY player_id,business_unit_id ORDER BY session_start ASC) as registration_device
		
from {{ ref('f_session') }} ))as session
on player.player_id = session.player_id and player.business_unit_id = session.business_unit_id

LEFT JOIN 
(
	SELECT	DISTINCT 
			player_id,
			business_unit_id,
			SUM(amount_eur) OVER (bonus_window) AS total_bonus_amount_eur,
			SUM(CASE WHEN claim_timestamp IS NOT NULL THEN amount_eur ELSE 0 END) OVER (bonus_window) AS lifetime_given_bonus_amt_eur,
			FIRST_VALUE(CASE WHEN claim_timestamp IS NOT NULL THEN bonus_configuration_id END IGNORE NULLS) OVER (bonus_window) AS first_bonus_configuration_id,
			FIRST_VALUE(CASE WHEN claim_timestamp IS NOT NULL THEN bonus_id END IGNORE NULLS) OVER (bonus_window) AS first_bonus_id, 
			FIRST_VALUE(CASE WHEN claim_timestamp IS NOT NULL THEN payment_id END IGNORE NULLS) OVER (bonus_window) AS first_bonus_payment_id,
			FIRST_VALUE(CASE WHEN claim_timestamp IS NOT NULL THEN amount_eur END IGNORE NULLS) OVER (bonus_window) AS first_bonus_amount_eur,
       FROM {{ ref('f_bonus') }} 
	WINDOW bonus_window AS (PARTITION BY player_id, business_unit_id ORDER BY claim_timestamp ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
) AS bonus
	ON player.player_id = bonus.player_id 
		AND player.business_unit_id = bonus.business_unit_id


left JOIN 
(SELECT  
		distinct
		player_id,
		business_unit_id,
		LAST_VALUE(balance_EUR IGNORE NULLS ) OVER (PARTITION BY player_id,business_unit_id ORDER by date_created asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as balance_eur		
from {{ ref('f_transaction') }} as transact) as transact
on player.player_id = transact.player_id and player.business_unit_id = transact.business_unit_id


--Select the first/second/third product played a player and the first subproduct in each product
LEFT JOIN
(
	SELECT  DISTINCT 
			player_id,
			business_unit_id,
			FIRST_VALUE(product) OVER (bet_window) as first_played_product,
			FIRST_VALUE(first_played_date) OVER (bet_window) as first_played_product_date,
			FIRST_VALUE(title) OVER (bet_window) as first_played_subproduct,
			NTH_VALUE(product, 2) OVER (bet_window) as second_played_product,
			NTH_VALUE(first_played_date, 2) OVER (bet_window) as second_played_product_date,
			NTH_VALUE(title, 2) OVER (bet_window) as second_played_subproduct,
			NTH_VALUE(product, 3) OVER (bet_window) as third_played_product,
			NTH_VALUE(first_played_date, 3) OVER (bet_window) as third_played_product_date,
			NTH_VALUE(title, 3) OVER (bet_window) as third_played_subproduct
	FROM 
	(
		SELECT  DISTINCT 
				player_id,
				business_unit_id,
				FIRST_VALUE(first_played_date) OVER (PARTITION BY player_id, business_unit_id, product ORDER BY first_played_date) AS first_played_date,
				product,
				FIRST_VALUE(title) OVER (PARTITION BY player_id, business_unit_id, product ORDER BY first_played_date) AS title
		from (
			select 		
				distinct
				player_id,
				business_unit_id,
				min(date_created) AS first_played_date,
				CASE WHEN game.sub_classification_name = 'Poker' THEN 'Poker'
						WHEN game.sub_classification_name = 'SPORTSBOOK' THEN 'SPORTSBOOK'
						ELSE 'Game of Chance' END AS product,
				game.title
			FROM {{ ref('f_bet') }} AS bet 
			JOIN  {{ ref('d_game') }} AS game
			ON bet.game_id = game.game_id
			GROUP BY player_id, business_unit_id, product, title
		)
	)
	WINDOW bet_window AS (PARTITION BY player_id, business_unit_id ORDER BY first_played_date ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
) as played
on player.player_id = played.player_id and player.business_unit_id = played.business_unit_id


--Calculates which fold (number of selections in a bet) is most popular
LEFT JOIN
--cteA counts the fold for each bet
(with cteA as (
select player_id, business_unit_id, coupon_id, bet_id, count(*) as fold
from {{ ref('f_sb_coupon') }}
group by player_id, business_unit_id, coupon_id, bet_id), 

--cteB counts how many there are of each fold
cteB as (
select player_id, business_unit_id, fold, count(*) as fold_count
from cteA
group by player_id,business_unit_id, fold)

--This select selects the most recurring fold, if more than one fold reoccurs the same amount of time, the highest fold is chosen
select player_id,business_unit_id, max(fold) as fold_count
from cteB b
where fold_count = (select max(fold_count) from cteB where player_id = b.player_id)
GROUP by player_id, business_unit_id) as fold
on player.player_id = fold.player_id and player.business_unit_id = fold.business_unit_id


LEFT JOIN
(SELECT
		distinct
		player_id,
		business_unit_id,
		FIRST_VALUE(sb_coupon_id IGNORE NULLS) OVER (PARTITION BY player_id,business_unit_id ORDER BY max_win desc, date_created ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as biggest_win_coupon,

		--nr_days_played_sb,
		FIRST_VALUE(day IGNORE NULLS) OVER(PARTITION BY player_id ORDER BY nr_times_played_sb_day desc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as top_played_day,
		LAST_VALUE(last_active_date) OVER(PARTITION BY player_id, business_unit_id ORDER BY last_active_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_active_date,


		
FROM (
select 
		player_id,
		sb_coupon_id,
		business_unit_id,
		date_created,
		DATETIME_TRUNC(datetime(date_created) , DAY) as day,
		MAX(CASE WHEN provider_type = 'SPORTSBOOK' THEN total_win_amount_eur ELSE NULL END) OVER(PARTITION BY player_id,sb_coupon_id) as max_win,
		
		--COUNT(DISTINCT CASE WHEN provider_type = 'SPORTSBOOK' THEN DATETIME_TRUNC(datetime(date_created) , DAY) ELSE NULL END) OVER(PARTITION BY player_id,business_unit_id) as nr_days_played_sb,
		COUNT(CASE WHEN provider_type = 'SPORTSBOOK' THEN DATETIME_TRUNC(datetime(date_created) , DAY) ELSE NULL END) OVER(PARTITION BY player_id,business_unit_id, DATETIME_TRUNC(datetime(date_created) , DAY)) as nr_times_played_sb_day,
		CASE WHEN bet_amount > 0 THEN date_created ELSE NULL END as last_active_date,
		

from
{{ ref('f_bet') }})

) as bet
on player.player_id = bet.player_id and player.business_unit_id = bet.business_unit_id



LEFT JOIN
	(select 
	distinct
			player_id,
			business_unit_id,
		FIRST_VALUE(participant IGNORE NULLS ) OVER (PARTITION BY player_id,business_unit_id ORDER BY numberofbets_participant,date_coupon_placed ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS most_played_team,
		FIRST_VALUE(sport_name IGNORE NULLS ) OVER (PARTITION BY player_id,business_unit_id ORDER BY numberofbets_sport_name,date_coupon_placed ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS most_played_sport,
		FIRST_VALUE(competition_name IGNORE NULLS ) OVER (PARTITION BY player_id,business_unit_id ORDER BY numberofbets_competition_name,date_coupon_placed ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS most_played_league,
		FIRST_VALUE(market_name IGNORE NULLS ) OVER (PARTITION BY player_id,business_unit_id ORDER BY numberofbets_market,date_coupon_placed ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS most_played_market,
		round(CAST(mobile as NUMERIC)/total,2) as ratio_mobile,
		round(CAST(desktop as NUMERIC)/total,2) as ratio_desktop,
		avg_odds,
		biggest_odds_win_sb,
	from (

		select  player_id,
				business_unit_id,
				date_coupon_placed,
				participant,
				COUNT(participant) over (PARTITION BY player_id,business_unit_id,participant) numberofbets_participant,
				sport_name,
				COUNT(sport_name) over (PARTITION BY player_id,business_unit_id,sport_name) numberofbets_sport_name,
				competition_name,
				COUNT(competition_name) over (PARTITION BY player_id,business_unit_id,competition_name) numberofbets_competition_name,
				market_name,
				COUNT(market_name) over (PARTITION BY player_id,business_unit_id,market_name) numberofbets_market,
				COUNT(CASE WHEN channel = 'MOBILE' THEN channel ELSE NULL END) over (PARTITION BY player_id,business_unit_id) as mobile,
				COUNT(CASE WHEN channel = 'DESKTOP' THEN channel ELSE NULL END) over (PARTITION BY player_id,business_unit_id) as desktop,
				COUNT(*) over(PARTITION BY player_id,business_unit_id) as total,
				AVG(distinct combinedodds_value) over(partition by player_id, business_unit_id) as avg_odds,
				MAX(CASE WHEN bet_final_status = 'WON' THEN combinedodds_value ELSE NULL END) over(partition by player_id, business_unit_id) as biggest_odds_win_sb,
		from {{ ref('f_sb_coupon') }}
	)
) as mostPlayed
on player.player_id = mostPlayed.player_id and player.business_unit_id = mostPlayed.business_unit_id 

LEFT JOIN
(SELECT affiliate_general_id, channel FROM {{ ref('d_affiliate_channel') }}) affiliate_channel  
on player.affiliate_id = affiliate_channel.affiliate_general_id

LEFT JOIN
	(SELECT
		distinct
		-- grouping values
		player_id,
		business_unit_id,
		--winnings
		SUM(CASE WHEN provider_type != 'SPORTSBOOK'AND final_status IN ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS') THEN winnings_eur ELSE 0 END) OVER (PARTITION BY player_id,business_unit_id) AS lifetime_win_amount_goc_EUR,
		SUM(CASE WHEN provider_type = 'SPORTSBOOK' AND final_status IN ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS') THEN winnings_eur ELSE 0 END) OVER (PARTITION BY player_id,business_unit_id) AS lifetime_win_amount_sb_EUR,
		SUM(CASE WHEN final_status in ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS') THEN gamewin_eur ELSE 0 END) OVER(PARTITION BY player_id,business_unit_id) AS lifetime_game_win_eur,
		MAX(CASE WHEN final_status !='OPEN' and provider_type = 'SPORTSBOOK' THEN winnings_eur ELSE NULL END) OVER(PARTITION BY player_id,business_unit_id) AS biggest_win_amount_eur_sb,

		--bets
		SUM(CASE WHEN provider_type != 'SPORTSBOOK'AND final_status IN ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS') THEN turnover_eur ELSE 0 END) OVER (PARTITION by player_id,business_unit_id) AS lifetime_bet_goc_EUR,
		SUM(CASE WHEN provider_type = 'SPORTSBOOK'AND final_status IN ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS') THEN turnover_eur ELSE 0 END) OVER( PARTITION by player_id,business_unit_id) AS lifetime_bet_sportsbook_EUR,
		SUM(CASE WHEN final_status in ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS') THEN turnover_eur ELSE 0 END) OVER(PARTITION by player_id,business_unit_id) AS lifetime_turnover_eur,
		AVG(CASE WHEN provider_type = 'SPORTSBOOK' THEN turnover_eur else NULL end) OVER(PARTITION by player_id,business_unit_id) AS average_bet_stake_eur_sb,
		MAX(CASE WHEN final_status !='OPEN' and provider_type = 'SPORTSBOOK' THEN turnover_eur ELSE NULL END) OVER(PARTITION by player_id,business_unit_id) AS biggest_bet_amount_eur_sb,
		SUM(CASE WHEN main_classification_name = 'Lotto' THEN turnover_eur ELSE NULL END) OVER (PARTITION by player_id,business_unit_id) AS lifetime_bet_goc_lotto_EUR,
		SUM(CASE WHEN main_classification_name = 'Live Casino' THEN turnover_eur ELSE NULL END) OVER (PARTITION by player_id,business_unit_id) AS lifetime_bet_goc_live_casino_EUR,
		SUM(CASE WHEN main_classification_name = 'Casino' THEN turnover_eur ELSE NULL END) OVER (PARTITION by player_id,business_unit_id) AS lifetime_bet_goc_casino_EUR,
		--Bet count
		COUNT(CASE WHEN record_type = 'BET' THEN record_type ELSE NULL END) OVER (PARTITION by player_id,business_unit_id) AS lifetime_bet_count,
		COUNT(CASE WHEN record_type = 'BET' AND provider_type = 'SPORTSBOOK' THEN record_type ELSE NULL END) OVER (PARTITION by player_id,business_unit_id) AS lifetime_bet_count_SB,
		COUNT(CASE WHEN record_type = 'BET' AND provider_type != 'SPORTSBOOK' THEN record_type ELSE NULL END) OVER (PARTITION by player_id,business_unit_id) AS lifetime_bet_count_goc,
		COUNT(CASE WHEN main_classification_name = 'Lotto' THEN main_classification_name ELSE NULL END) OVER (PARTITION by player_id,business_unit_id) AS lifetime_bet_count_goc_lotto_EUR,
		COUNT(CASE WHEN main_classification_name = 'Live Casino' THEN main_classification_name ELSE NULL END) OVER (PARTITION by player_id,business_unit_id) AS lifetime_bet_count_goc_live_casino_EUR,
		COUNT(CASE WHEN main_classification_name = 'Casino' THEN main_classification_name ELSE NULL END) OVER (PARTITION by player_id,business_unit_id) AS lifetime_bet_count_goc_casino_EUR,
		MAX(CASE WHEN provider_type != 'SPORTSBOOK' AND final_status IN ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS') AND turnover_eur > 0 THEN date_created ELSE NULL END) OVER (PARTITION BY player_id,business_unit_id) AS LastActiveGameOfChance,
		MAX(CASE WHEN provider_type = 'SPORTSBOOK' THEN date_created ELSE NULL END) OVER (PARTITION BY player_id,business_unit_id) AS last_played_sportsbook_date,
		SUM(CASE WHEN provider_type = 'SPORTSBOOK' AND TIME(DATETIME_ADD(date_created, INTERVAL 3 HOUR)) >='00:00:00' AND TIME(DATETIME_ADD(date_created, INTERVAL 3 HOUR)) < '08:00:00' THEN 1 ELSE 0 END) OVER (PARTITION BY player_id,business_unit_id) AS night_bettor,
		SUM(CASE WHEN provider_type = 'SPORTSBOOK' AND TIME(DATETIME_ADD(date_created, INTERVAL 3 HOUR)) >='08:00:00' AND TIME(DATETIME_ADD(date_created, INTERVAL 3 HOUR)) < '16:00:00' THEN 1 ELSE 0 END) OVER (PARTITION BY player_id,business_unit_id) AS day_bettor,
		SUM(CASE WHEN provider_type = 'SPORTSBOOK' AND TIME(DATETIME_ADD(date_created, INTERVAL 3 HOUR)) >='16:00:00' THEN 1 ELSE 0 END) OVER (PARTITION BY player_id,business_unit_id) AS primetime_bettor,
		COUNT(DISTINCT CASE WHEN provider_type = 'SPORTSBOOK' THEN DATETIME_TRUNC(datetime(date_created) , DAY) ELSE NULL END) OVER(PARTITION BY player_id,business_unit_id) as nr_days_played_sb,

		--bonus
		SUM(free_bet_bc_eur) OVER (PARTITION by player_id,business_unit_id) AS free_bet_bonus_cost_eur,
		SUM(free_spin_bc_eur) OVER (PARTITION by player_id,business_unit_id) AS free_spin_bonus_cost_eur,
		SUM(risk_free_bet_bc_eur) OVER (PARTITION by player_id,business_unit_id) AS risk_free_bet_bonus_cost_eur,
		SUM(accounting_revenue_eur) OVER (PARTITION by player_id,business_unit_id) AS lifetime_accounting_revenue_eur,
		COALESCE(SUM(bonus_game_win_eur) OVER (PARTITION BY player_id, business_unit_id), 0.0) AS bonus_gamewin_eur,
		COALESCE(SUM(final_winnings_allocation_eur) OVER (PARTITION BY player_id, business_unit_id), 0.0) AS bonus_winnings_eur,

	--withdrawal/deposit
		SUM(deposit_captured_amt_eur) OVER (PARTITION BY player_id, business_unit_id) AS deposit_amount_EUR,
		SUM(withdrawal_captured_amt_eur) OVER (PARTITION BY player_id, business_unit_id) AS withdrawal_amount_EUR,
		SUM(adjustment_bonus_eur) OVER (PARTITION BY player_id, business_unit_id) AS adjustment_bonus_eur,
		SUM(total_bonus_cost_eur) OVER (PARTITION BY player_id, business_unit_id) AS total_bonus_cost_eur,
		
		COUNT(CASE WHEN record_type = 'PAYMENT' AND transaction_type = 'DEPOSIT' THEN transaction_type ELSE NULL END) OVER (PARTITION BY player_id, business_unit_id) AS deposit_count,
		COUNT(CASE WHEN record_type = 'PAYMENT' AND transaction_type = 'DEPOSIT' AND final_status = 'CAPTURED' THEN transaction_type ELSE NULL END) OVER (PARTITION BY player_id, business_unit_id) AS deposit_count_captured,
		AVG(CASE WHEN final_status = 'CAPTURED' AND transaction_type = 'DEPOSIT' THEN deposit_captured_amt_eur ELSE null end) OVER (PARTITION BY player_id, business_unit_id) AS avg_deposit_captured_EUR,
		COUNT(CASE WHEN transaction_type = 'WITHDRAWAL' THEN transaction_type else null end) OVER (PARTITION BY player_id, business_unit_id) AS withdrawal_count,
		COUNT(CASE WHEN transaction_type = 'WITHDRAWAL' AND final_status = 'CAPTURED' THEN transaction_type ELSE null END) OVER (PARTITION BY player_id, business_unit_id) AS withdrawal_count_captured,
		AVG(CASE WHEN final_status = 'CAPTURED' AND transaction_type = 'WITHDRAWAL' THEN withdrawal_captured_amt_eur ELSE null END) OVER (PARTITION BY player_id, business_unit_id) AS avg_withdrawal_captured_EUR,
		MIN(ndc_date) over(partition by player_id, business_unit_id) as first_deposit_date,
		MAX(CASE WHEN final_status = 'CAPTURED' AND transaction_type = 'DEPOSIT' THEN transaction_date ELSE null END) over(partition by player_id, business_unit_id) as last_deposit_date,
		MIN(CASE WHEN final_status = 'CAPTURED' AND transaction_type = 'WITHDRAWAL' THEN transaction_date ELSE null END) over(partition by player_id, business_unit_id) as first_withdrawal_date,
		MAX(CASE WHEN final_status = 'CAPTURED' AND transaction_type = 'WITHDRAWAL' THEN transaction_date ELSE null END) over(partition by player_id, business_unit_id) as last_withdrawal_date,
		FIRST_VALUE(CASE WHEN final_status = 'CAPTURED' AND transaction_type = 'WITHDRAWAL' THEN withdrawal_captured_amt_eur ELSE null END IGNORE NULLS) over(partition by player_id, business_unit_id order by transaction_date asc  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) first_withdrawal_amount_EUR,
		LAST_VALUE(CASE WHEN final_status = 'CAPTURED' AND transaction_type = 'DEPOSIT' THEN deposit_captured_amt_eur ELSE null END IGNORE NULLS) over(partition by player_id, business_unit_id order by transaction_date asc  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_deposit_amount_EUR,
		FIRST_VALUE(CASE WHEN final_status = 'CAPTURED' AND transaction_type = 'DEPOSIT' THEN deposit_captured_amt_eur ELSE null END IGNORE NULLS) over(partition by player_id, business_unit_id order by transaction_date asc  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as first_deposit_amount_EUR,
		FIRST_VALUE(CASE WHEN final_status = 'CAPTURED' AND transaction_type = 'DEPOSIT' THEN payment_method_type  ELSE null END IGNORE NULLS) over(partition by player_id, business_unit_id order by transaction_date asc  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as first_deposit_method,
		LAST_VALUE (CASE WHEN record_type = 'NDC' THEN provider_type END IGNORE NULLS ) over(partition by player_id, business_unit_id order by transaction_date asc  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS provider_name,
		COUNT(DISTINCT CASE WHEN record_type IN ('BET', 'PAYMENT') THEN date(date_created) END) over (partition by player_id, business_unit_id) as active_days_on_website,
		--bonus_ratio should be calculated using local currency to eliminate slight hourly exchange rate fluctuations which may lead to having ratio > 1 when given bonus amount in lcy equals to deposit captured amount in lcy
		SAFE_DIVIDE(
			SUM(
				CASE 
					WHEN transaction_date >= TIMESTAMP_SUB(TIMESTAMP_TRUNC('{{ run_started_at }}', DAY), INTERVAL 7 DAY) AND transaction_date < TIMESTAMP_TRUNC('{{ run_started_at }}', DAY) THEN given_bonus_amt_eur / transaction_exchange_rate
					ELSE 0
				END
				) OVER (PARTITION BY player_id, business_unit_id),
			SUM(
				CASE 
					WHEN transaction_date >= TIMESTAMP_SUB(TIMESTAMP_TRUNC('{{ run_started_at }}', DAY), INTERVAL 7 DAY) AND transaction_date < TIMESTAMP_TRUNC('{{ run_started_at }}', DAY) THEN deposit_captured_amt_eur / transaction_exchange_rate
					ELSE 0
				END
				) OVER (PARTITION BY player_id, business_unit_id)
			) AS bonus_ratio_7d,
		SAFE_DIVIDE(
			SUM(
				CASE 
					WHEN transaction_date >= TIMESTAMP_SUB(TIMESTAMP_TRUNC('{{ run_started_at }}', DAY), INTERVAL 14 DAY) AND transaction_date < TIMESTAMP_TRUNC('{{ run_started_at }}', DAY) THEN given_bonus_amt_eur / transaction_exchange_rate
					ELSE 0
				END
				) OVER (PARTITION BY player_id, business_unit_id),
			SUM(
				CASE 
					WHEN transaction_date >= TIMESTAMP_SUB(TIMESTAMP_TRUNC('{{ run_started_at }}', DAY), INTERVAL 14 DAY) AND transaction_date < TIMESTAMP_TRUNC('{{ run_started_at }}', DAY) THEN deposit_captured_amt_eur / transaction_exchange_rate
					ELSE 0
				END
				) OVER (PARTITION BY player_id, business_unit_id)
			) AS bonus_ratio_14d,
		SAFE_DIVIDE(
			SUM(
				CASE 
					WHEN transaction_date >= TIMESTAMP_SUB(TIMESTAMP_TRUNC('{{ run_started_at }}', DAY), INTERVAL 30 DAY) AND transaction_date < TIMESTAMP_TRUNC('{{ run_started_at }}', DAY) THEN given_bonus_amt_eur / transaction_exchange_rate
					ELSE 0
				END
				) OVER (PARTITION BY player_id, business_unit_id),
			SUM(
				CASE 
					WHEN transaction_date >= TIMESTAMP_SUB(TIMESTAMP_TRUNC('{{ run_started_at }}', DAY), INTERVAL 30 DAY) AND transaction_date < TIMESTAMP_TRUNC('{{ run_started_at }}', DAY) THEN deposit_captured_amt_eur / transaction_exchange_rate
					ELSE 0
				END
				) OVER (PARTITION BY player_id, business_unit_id)
			) AS bonus_ratio_30d,
			SAFE_DIVIDE(SUM(given_bonus_amt_eur / transaction_exchange_rate) OVER (PARTITION BY player_id, business_unit_id), SUM(deposit_captured_amt_eur / transaction_exchange_rate) OVER (PARTITION BY player_id, business_unit_id)) AS bonus_ratio_lifetime	

	FROM {{ ref('f_reporting_transactions') }}
	WHERE	((record_type = 'BET' and final_status IN ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS'))
					OR (record_type = 'ADJUSTMENT' and final_status = 'APPROVED')
						OR record_type in ('PAYMENT', 'NDC', 'NRC', 'BONUS'))
							AND NOT test_account
) rt
ON player.player_id = rt.player_id 
AND player.business_unit_id = rt.business_unit_id



