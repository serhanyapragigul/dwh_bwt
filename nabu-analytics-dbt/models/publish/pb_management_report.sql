{{
    config(
        schema='publish',
        materialized='table',
        cluster_by = ['reporting_date','player_id']
        )
}}
        --materialized='incremental',
        --unique_key = 'reporting_id',

SELECT  FARM_FINGERPRINT(
            CONCAT( rt.player_id, 
                    TIMESTAMP_TRUNC(rt.transaction_date, DAY), 
                    rt.product,
                    COALESCE(CASE
                                WHEN rt.main_classification_name = 'SPORTSBOOK' THEN rt.sb_game_phase
                                ELSE rt.main_classification_name
                            END, 'NULL'),
                    COALESCE(rt.transaction_currency, 'NULL'),
                    COALESCE(CAST(rt.transaction_exchange_rate AS string), 'NULL')
                )
        ) as reporting_id,
        rt.player_id,
        TIMESTAMP_TRUNC(rt.transaction_date, DAY) AS reporting_date,
        rt.franchise_name,
        rt.product,
        rt.acquisition_channel,
        CASE
            WHEN rt.main_classification_name = 'SPORTSBOOK' THEN rt.sb_game_phase
            ELSE rt.main_classification_name
        END game_type,
        CASE WHEN rt.record_type = 'PAYMENT' THEN rt.provider_type ELSE NULL END AS payment_provider,
        CASE WHEN rt.record_type = 'BET' THEN rt.provider_type ELSE NULL END AS game_provider,
        rt.active_player,
        rt.transaction_currency,
        rt.transaction_exchange_rate,
        COUNT(  CASE
                    WHEN rt.record_type = 'BET' AND  product != 'SPORTSBOOK'  THEN rt.transaction_id
                    WHEN rt.record_type = 'BET' THEN rt.sb_bet_type
                    ELSE NULL
                END
            ) AS bet_cnt,
        SUM(rt.turnover_eur) AS turnover_eur,
        SUM(rt.turnover_eur * rt.odds_value) AS weighted_turnover_eur,
        SUM(rt.jackpot_payout_eur) AS jackpot_payout_eur,
		SUM(rt.jackpot_contribution_eur) AS jackpot_contribution_eur,
		SUM(rt.gamewin_eur) AS gamewin_eur,
		SUM(rt.free_spin_bc_eur) AS free_spin_bc_eur,
		SUM(rt.free_bet_bc_eur) AS free_bet_bc_eur,
		SUM(rt.risk_free_bet_bc_eur) AS risk_free_bet_bc_eur,
		SUM(rt.price_boost_bc_eur) AS price_boost_bc_eur,
		SUM(rt.adjustment_bonus_eur) AS adjustment_bonus_eur,
		SUM(rt.bonus_game_win_eur) AS bonus_game_win_eur,
        SUM(rt.final_winnings_allocation_eur) AS final_winnings_allocation_eur,
        SUM(rt.general_bonus_cost_eur) AS general_bonus_cost_eur,
        SUM(rt.total_bonus_cost_eur) AS total_bonus_cost_eur,
        SUM(rt.given_bonus_amt_eur) AS given_bonus_amt_eur,
        COUNT(DISTINCT CASE WHEN source_table = 'F_BONUS' AND rt.given_bonus_amt_eur != 0 THEN rt.transaction_id END) AS given_bonus_cnt,
        COALESCE(SUM(CASE WHEN source_table = 'F_BONUS' AND rt.is_final_bonus_status AND rt.final_status != 'COMPLETED' THEN rt.bonus_amt_eur ELSE 0.0 END), 0.0) AS expired_bonus_amt_eur,
        COUNT(DISTINCT CASE WHEN source_table = 'F_BONUS' AND rt.is_final_bonus_status AND rt.final_status != 'COMPLETED' THEN rt.transaction_id END) AS expired_bonus_cnt,
        COALESCE(SUM(CASE WHEN source_table = 'F_BONUS' AND rt.is_bonus_lost AND rt.is_final_bonus_status  THEN rt.bonus_amt_eur ELSE 0.0 END), 0.0) AS lost_bonus_amt_eur,
        COUNT(DISTINCT CASE WHEN source_table = 'F_BONUS' AND rt.is_bonus_lost AND rt.is_final_bonus_status THEN rt.transaction_id END) AS lost_bonus_cnt,
        COALESCE(SUM(CASE WHEN source_table = 'F_BONUS' AND rt.is_final_bonus_status AND rt.final_status = 'COMPLETED' AND NOT rt.is_bonus_lost  THEN rt.bonus_amt_eur ELSE 0.0 END), 0.0)  AS won_bonus_amt_eur,
        COUNT(DISTINCT CASE WHEN source_table = 'F_BONUS' AND rt.is_final_bonus_status AND rt.final_status = 'COMPLETED' AND NOT rt.is_bonus_lost THEN rt.transaction_id END) AS won_bonus_cnt,
  
        SUM(rt.accounting_revenue_eur) AS accounting_revenue_eur,
        SUM(rt.deposit_initiated_cnt) AS deposit_initiated_cnt,
        SUM(rt.deposit_initiated_amt_eur) AS deposit_initiated_amt_eur,
        SUM(rt.deposit_captured_cnt) AS deposit_captured_cnt,
        SUM(rt.deposit_captured_amt_eur) AS deposit_captured_amt_eur,
        SUM(rt.withdrawal_initiated_cnt) AS withdrawal_initiated_cnt,
        SUM(rt.withdrawal_initiated_amt_eur) AS withdrawal_initiated_amt_eur,
        SUM(rt.withdrawal_captured_cnt) AS withdrawal_captured_cnt,
        SUM(rt.withdrawal_captured_amt_eur) AS withdrawal_captured_amt_eur,
        SUM(rt.deposit_transaction_fee_eur) AS deposit_transaction_fee_eur,
        SUM(rt.withdrawal_transaction_fee_eur) AS withdrawal_transaction_fee_eur,
        SUM(rt.adjustment_amt_eur) AS adjustment_amt_eur,
        SUM(CAST(rt.record_type = 'NRC' AS INT64)) AS nrc_cnt,
        SUM(CAST(rt.record_type = 'NDC' AS INT64)) AS ndc_cnt,
        CASE WHEN product != 'SPORTSBOOK' 
            THEN COUNT(CASE WHEN rt.record_type = 'BET' THEN rt.transaction_id END)
            ELSE COUNT(CASE WHEN rt.record_type = 'BET' AND sb_bet_type = 'SINGLE' THEN rt.transaction_id END) 
        END AS single_bet_cnt,
        CASE WHEN product != 'SPORTSBOOK' 
            THEN 0
            ELSE COUNT(CASE WHEN rt.record_type = 'BET' AND sb_bet_type = 'BET_BUILDER' THEN rt.transaction_id END) 
        END AS bet_builder_bet_cnt,
        CASE WHEN product != 'SPORTSBOOK' 
            THEN 0
            ELSE COUNT(CASE WHEN rt.record_type = 'BET' AND sb_bet_type = 'COMBI' THEN rt.transaction_id END) 
        END AS combi_bet_cnt
    
    FROM {{ ref('f_reporting_transactions') }} AS rt
	WHERE	((rt.record_type = 'BET' and rt.final_status IN ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS'))
                OR (rt.record_type = 'ADJUSTMENT' and rt.final_status = 'APPROVED')
                    OR rt.record_type in ('PAYMENT', 'NDC', 'NRC', 'BONUS'))
                        AND NOT rt.test_account
            {% if is_incremental() %}
                AND TIMESTAMP_TRUNC(rt.transaction_date, DAY) >= (SELECT MAX(reporting_date) FROM {{ this }})
            {% endif -%}
    GROUP BY	reporting_id,
                rt.player_id,
				TIMESTAMP_TRUNC(rt.transaction_date, DAY),
				rt.franchise_name,
				rt.product,
				rt.acquisition_channel,
                game_type,
                payment_provider,
                game_provider,
                rt.active_player,
                rt.transaction_currency,
                rt.transaction_exchange_rate