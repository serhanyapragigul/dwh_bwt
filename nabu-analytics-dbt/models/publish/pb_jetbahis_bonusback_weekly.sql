{{
    config(
        schema='publish',
        materialized='table',
		cluster_by = ['activity_date']
    )
}}

SELECT  bet.player_id,
        plr.test_account,
        bet.bet_id,
        bet.franchise_name,
        bet.final_status_date AS activity_date,
        bet.currency_code,
        bet.total_bet_amount AS bet_amt_lcy,
        bet.total_win_amount AS win_amt_lcy,
        bet.game_win_amount	 AS total_game_win_amt_lcy,
        COALESCE(bns.bonus_game_win, 0) AS bonus_game_win_amt_lcy,
        bet.game_win_amount	 - COALESCE(bns.bonus_game_win, 0) AS real_money_game_win_amt_lcy
FROM    {{ ref('f_bet') }} AS bet
LEFT JOIN (
    SELECT  bb.bet_id,
            SUM(bb.bonus_game_win) AS bonus_game_win
    FROM {{ ref('f_bonus_bet') }} AS bb
    WHERE   DATETIME(bb.date_final, 'Turkey') >= '2022-01-01 00:00:00'
                AND bb.business_unit_id = '24d38a84-b094-4208-8d17-684a847afbdb' -- jetbahis.com
    GROUP BY bb.bet_id
) AS bns
ON  bet.bet_id = bns.bet_id
    LEFT JOIN {{ ref('d_player') }} AS plr
    ON plr.player_id = bet.player_id
WHERE   bet.final_status IN ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS')
            AND bet.business_unit_id = '24d38a84-b094-4208-8d17-684a847afbdb' -- jetbahis.com
                AND DATETIME(bet.final_status_date, 'Turkey') >= '2022-01-01 00:00:00'
