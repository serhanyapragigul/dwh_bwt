{{ config(schema='publish', materialized='table') }}

SELECT	bet.player_id,
		bet.game_id,
		player.business_unit_id,
		bu.business_unit_name,
        bu.business_unit_type,
        player.merchant_name,
        player.brand_name,
        player.franchise_name,
        player.currency_code,
        player.country_code,
		player.test_account,
		player.affiliate_id,
		player.first_used_device_type,
		player.value_segment,
		player.value_seg_30d_highest,
		player.value_seg_ever_highest,
		player.channel,
        game.main_classification_name,
        game.sub_classification_name,
        game.title,
        game.provider_name,
		game.sub_provider_name,
		bet.hourly_time,
		bet.total_win_amount_eur,
		bet.total_bet_amount_eur,
		bet.game_win_amount_eur,
		bet.accounting_revenue_amt_eur,
		bet.jackpot_payout_eur,
		bet.jackpot_contribution_eur,
		bet.free_spin_bonus_cost_eur,
		bet.total_bonus_cost_eur,
		ndc.ndc_date AS first_deposit_date,
		bet.ROUND,
		player.player_status
FROM (
	SELECT  player_id,
			business_unit_id,
			game_id,
			DATETIME_TRUNC(datetime(transaction_date) , HOUR) AS hourly_time,
			SUM(winnings_eur) AS total_win_amount_eur,
			SUM(turnover_eur) AS total_bet_amount_eur, 
			SUM(gamewin_eur) AS game_win_amount_eur,
			SUM(accounting_revenue_eur) AS accounting_revenue_amt_eur,
			SUM(jackpot_payout_eur) AS jackpot_payout_eur,
			SUM(jackpot_contribution_eur) AS jackpot_contribution_eur,
			SUM(free_spin_bc_eur) AS free_spin_bonus_cost_eur,
			SUM(total_bonus_cost_eur) AS total_bonus_cost_eur,
			COUNT(CASE WHEN final_status IN ('CLOSED', 'WON', 'CLOSED_WITH_LATE_WINS') THEN final_status ELSE NULL END) AS ROUND	
	FROM {{ ref('f_reporting_transactions') }}
	WHERE	(record_type, final_status) IN (
											('BET', 'CLOSED'), ('BET', 'WON'), ('BET', 'CLOSED_WITH_LATE_WINS')
					)					
				OR (record_type = 'BONUS' and is_final_bonus_status and total_bonus_cost_eur <> 0 )
				OR (adjustment_bonus_eur > 0 AND record_type = 'ADJUSTMENT' AND final_status = 'APPROVED')
	GROUP BY	player_id,
				business_unit_id,
				game_id, 
				hourly_time
) AS bet
	LEFT JOIN (
		SELECT	DISTINCT pl.player_id,
				pl.business_unit_id,
				pl.merchant_name,
				pl.brand_name,
				pl.franchise_name,
				pl.currency_code,
				pl.country_code,
				pl.test_account,
				pl.value_segment,
				pl.value_seg_30d_highest,
				pl.value_seg_ever_highest,
				pl.affiliate_id,
				pl.first_used_device_type,
				aff.channel,
				pl.player_status
		FROM {{ ref('d_player') }} AS pl
			LEFT JOIN {{ ref('d_affiliate_channel') }} AS aff 
				ON aff.affiliate_general_id = pl.affiliate_id
	) AS player
		ON player.player_id = bet.player_id
			AND player.business_unit_id = bet.business_unit_id
		LEFT JOIN (
			SELECT	DISTINCT LAST_VALUE (business_unit_id IGNORE NULLS ) OVER (PARTITION BY business_unit_id ORDER by start_date asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS business_unit_id,
					LAST_VALUE (business_unit_name IGNORE NULLS ) OVER (PARTITION BY business_unit_id ORDER by start_date asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS business_unit_name,
					LAST_VALUE (business_unit_type IGNORE NULLS ) OVER (PARTITION BY business_unit_id ORDER by start_date asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS business_unit_type
			FROM {{ ref('d_business_unit') }}
		) AS bu
			ON player.business_unit_id = bu.business_unit_id
			LEFT JOIN {{ ref('d_game') }} AS game
				ON bet.game_id = game.game_id
				LEFT JOIN {{ ref('f_ndc') }} AS ndc 
					ON bet.player_id = ndc.player_id
						AND bet.business_unit_id = ndc.business_unit_id
WHERE COALESCE(game.sub_classification_name, 'PLAYER') NOT IN  ('SPORTSBOOK') 
   AND bet.game_id != 'SPORTSBOOK'

