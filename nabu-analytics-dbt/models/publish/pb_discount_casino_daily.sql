{{ config(schema='publish')}}

SELECT
	agg.player_id,
	agg.business_unit_id,
	p.email AS user_email,
	p.country_code AS user_country,
	p.test_account AS is_test_account,
	agg.bet_try,
	agg.win_try,
	agg.bet_eur,
	agg.win_eur,
	agg.game_win_try,
	agg.game_win_eur,
	CASE
		WHEN agg.game_win_try < 0
		THEN 0
		ELSE game_win_try*0.1
	END AS deserved_discount_amount_try,
	CASE
		WHEN agg.game_win_eur < 0
		THEN 0
		ELSE game_win_eur*0.1
	END AS deserved_discount_amount_eur,
	agg.bet_date,
	agg.date_after_bets
FROM 
(
	-- aggreated bets and wins
	SELECT 
		sub.player_id,
		sub.business_unit_id,
		SUM(sub.sum_bet_try) AS bet_try,
		SUM(sub.sum_win_try) AS win_try,
		SUM(sub.sum_bet_eur) AS bet_eur,
		SUM(sub.sum_win_eur) AS win_eur,
		sub.bet_date,
		sub.date_after_bets,
		SUM(sub.sum_bet_try) - SUM(sub.sum_win_try) AS game_win_try,
		SUM(sub.sum_bet_eur) - SUM(sub.sum_win_eur) AS game_win_eur
FROM
(
		-- bets, wins and dates
		SELECT
			player_id,
			business_unit_id,
			CASE 
				WHEN currency_code = 'TRY'
				THEN total_bet_amount
				ELSE 0
			END AS sum_bet_try,
			CASE
				WHEN currency_code = 'TRY'
				THEN total_win_amount
				ELSE 0
			END AS sum_win_try,
			CASE 
				WHEN currency_code = 'EUR'
				THEN total_bet_amount
				ELSE 0
			END AS sum_bet_eur,
			CASE
				WHEN currency_code = 'EUR'
				THEN total_win_amount
				ELSE 0
			END AS sum_win_eur,
			CAST(DATETIME(final_status_date , "Europe/Istanbul") AS DATE) AS bet_date,
			DATE_ADD(CAST(DATETIME(final_status_date , "Europe/Istanbul") AS DATE), INTERVAL + 1 DAY) AS date_after_bets
		FROM {{ ref('f_bet') }}
		WHERE UPPER(final_status) IN ( 'CLOSED', 'WON')
		-- Discountcasino.com
		AND business_unit_id = 'ee5cdc34-f0f3-4b8d-aac2-da24f2d804ce'
        -- Include last day data only
		AND DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY) =  DATETIME_TRUNC(DATETIME(final_status_date, "Europe/Istanbul"), DAY)
) sub
GROUP BY
	sub.player_id,
	sub.business_unit_id,
	sub.bet_date,
	sub.date_after_bets
) agg
LEFT JOIN {{ ref('d_player') }} p
ON p.player_id = agg.player_id
WHERE
CASE
	WHEN agg.game_win_try < 0 THEN 0 ELSE agg.game_win_try END +
CASE
	WHEN agg.game_win_eur < 0 THEN 0 ELSE agg.game_win_eur END > 0 