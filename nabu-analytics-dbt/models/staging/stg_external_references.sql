{{ config(schema='stg')}}

SELECT
    TRIM(body.id) as external_reference_id,
    TRIM(body.playerId) as player_id, 
    TRIM(body.reference) as reference,
    TRIM(body.type) as type,
    TRIM(body.service) as service

FROM {{ source('raw', 'external_references') }}
