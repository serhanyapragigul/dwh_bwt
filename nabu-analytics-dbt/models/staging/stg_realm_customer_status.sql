{{ 
    config(
        schema = 'stg',
        tags = ['daily']
    )
}}

SELECT  Measures_Customer_Guid AS realm_player_id,
        Measures_Customer_Email AS email,
        Measures_Customer_Status_Name AS status_name,
        Retrieval_Date AS retrieval_date,
        Measures_Segment_Value AS realm_segment,
        Measures_Current_Sportsbook_Classification as current_sb_classification
FROM {{ source('cube_mirror_dataset', 'ndc_history') }} 

