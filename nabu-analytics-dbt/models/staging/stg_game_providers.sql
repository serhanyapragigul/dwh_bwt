{{ config(schema='stg')}}

select
    TRIM(type) as type,
    TRIM(body.id) as game_provider_id,
    TRIM(body.name) as name,
    TRIM(body.realUrl) as real_url,
    TRIM(kafkaData.topic) as topic,
    CAST(kafkaData.insertTime as DATETIME) as date_insert_time
   
from {{ source('raw', 'game_providers') }}  