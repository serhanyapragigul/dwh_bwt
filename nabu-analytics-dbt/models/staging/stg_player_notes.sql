{{ config(schema='stg')}}



select 
    TRIM(body.id) as player_notes_id, 
    TRIM(body.playerId) as player_id, 
    TRIM(body.businessUnitId) as business_unit_id, 
    TRIM(body.note) as note, 
    CAST(body.pinned AS bool) as pinned, 
    CAST(body.updated AS TIMESTAMP) as player_notes_date, 
    TRIM(body.updatedBy) as player_notes_updated_by, 
    TRIM(body.updatedByUsername) as player_notes_updated_name,
    TRIM(type) as type
from {{ source('raw', 'player_notes') }}



