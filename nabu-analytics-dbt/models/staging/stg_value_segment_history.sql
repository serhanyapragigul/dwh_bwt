    {{ config(schema='stg')}}

SELECT
    guid as player_id,
    UPPER(brand) as franchise_name,
    evaluation_date, 
    value_segment
FROM
{{ source('techflex_segment', 'value_segment_history') }} a