{{ config(schema='stg')}}

SELECT  FARM_FINGERPRINT(CONCAT(kafkaData.topic,':',kafkaData.PARTITION,':',kafkaData.offset,':',body.id)) AS bonus_log_key,
        TRIM(type) AS type,
        TRIM(body.id) AS bonus_log_id,
        TRIM(body.betId) AS bet_id,
        TRIM(body.betTransactionId) AS bet_transaction_id,
        TRIM(body.transactionId) AS transaction_id,
        TRIM(body.bonusId) AS bonus_id,
        CAST(body.betAmount AS NUMERIC) / 100 AS bet_amount, -- In raw, we receive bet amount in cents. 
        CAST(body.bonusAmountTaken AS NUMERIC) AS bonus_amount_taken,
        CAST(body.lockedAmountTaken AS NUMERIC) AS locked_amount_taken,
        CAST(body.transactionAmountCorrection AS NUMERIC) AS transaction_amount_correction,
        CAST(body.withdrawableAmountTaken AS NUMERIC) AS withdrawable_amount_taken,
        TRIM(body.wagerType) AS wager_type,
        CAST(body.wagered AS NUMERIC) AS wagered,
        TRIM(body.status) AS status,
        CAST(body.created AS TIMESTAMP) AS date_created,
        CAST(kafkaData.insertTime AS TIMESTAMP) AS kafka_insert_time,
        _PARTITIONTIME AS partition_time
    FROM {{ source('raw', 'bonus_logs') }}
    --exclude missing bet, it won't be ever restored, confirmed by Platform
    --https://avossuite.atlassian.net/servicedesk/customer/portal/2/ASTS-4900
    WHERE body.betId != 'c2a59d89-2c66-4238-9f8d-6aa5b6eebfa6'
