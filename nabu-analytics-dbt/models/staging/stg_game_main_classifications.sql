{{ config(schema='stg')}}

select
    TRIM(type) as type,
    TRIM(body.id) as gmc_id,
    TRIM(body.businessunitid) as business_unit_id,
    TRIM(body.name) as name,
    TRIM(kafkaData.topic) as topic,
    CAST(kafkaData.insertTime as DATETIME) as date_insert_time
   
from {{ source('raw', 'game_main_classifications') }}  