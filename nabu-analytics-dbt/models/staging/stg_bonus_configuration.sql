{{ config(schema='stg')}}


SELECT

        TRIM(type) as type,
        TRIM(body.id) as bonus_configuration_id,
        TRIM(body.name) as bonus_configuration_name,
        TRIM(body.businessUnitId) as business_unit_id,
        TRIM(body.bonusType) as bonus_type,
        TRIM(body.bonusDeliveryMethod) as bonus_delivery_method,
        CAST(body.bonusClaimTimeSeconds as INT64) as bonus_claim_time_seconds,
        CAST(body.bonusLifeTimeSeconds as INT64) as bonus_life_time_seconds,
        CAST(body.percentBonusGiven as INT64) as percent_bonus_given,
        CAST(body.bonusMoneyFirst as BOOL) as bonus_money_first,
        TRIM(body.titleKey) as title_key,
        TRIM(body.bodyKey) as body_key,
        TRIM(body.termsAndConditionsKey) as terms_and_conditions_key,
        TRIM(body.imageId) as image_id,
        CAST(body.updated as TIMESTAMP) as date_updated,
        TRIM(body.updatedBy) as updated_by,
        TRIM(body.updatedByUsername) as updated_by_username,
        TRIM(body.status) as status,
        TRIM(body.imageUrl) as image_url,
        TRIM(kafkaData.topic) as topic,
        CAST(kafkaData.offset as INT64) as offset,
        CAST(kafkaData.insertTime as TIMESTAMP) as date_insert_time,
        TRIM(kafkaData.rowUniqueId) as row_unique_id
   



    FROM {{ source('raw', 'bonus_configurations') }}

        