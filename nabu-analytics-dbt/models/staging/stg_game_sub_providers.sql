{{ config(schema='stg')}}

SELECT
    TRIM(type) as type,
    TRIM(body.id) as sub_provider_id,
    TRIM(body.name) as sub_provider_name,
    CAST(body.updated as TIMESTAMP) as date_updated,
    TRIM(body.updatedBy) as updated_by,
    TRIM(body.updatedByUsername) as updated_by_username

FROM {{ source('raw', 'game_sub_providers') }}