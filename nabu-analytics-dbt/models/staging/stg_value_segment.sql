{{ config(schema='stg')}}

SELECT
    guid as player_id,
    UPPER(brand) as franchise_name,
    evaluation_date,
    value_segment,
    value_seg_30d_highest
FROM
{{ source('techflex_segment', 'value_segment') }} a