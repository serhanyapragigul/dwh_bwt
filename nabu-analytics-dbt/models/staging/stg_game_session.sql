{{ config(schema='stg')}}

SELECT	FARM_FINGERPRINT(CONCAT(body.id,':', type,':', COALESCE(body.updated, TIMESTAMP_MILLIS(0)))) AS game_session_key,
		TRIM(`type`) AS type, -- should we have all types in dwh?
		TRIM(body.id) AS game_session_id,
		TRIM(body.businessUnitId) AS business_unit_id,
		TRIM(body.gameId) AS game_id,
		TRIM(body.status) AS status,
		TRIM(body.providerGameId) AS provider_game_id,
		TRIM(body.internalGameId) AS internal_game_id,
		TRIM(body.playerId) AS player_id,
		CAST(body.updated AS TIMESTAMP) AS date_updated,
		TRIM(body.sessionId) AS session_id,
		_PARTITIONTIME AS partition_time,
		kafkaData.insertTime AS kafka_insert_time
FROM	{{ source('raw', 'game_sessions') }}