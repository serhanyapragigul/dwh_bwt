{{ config(schema='stg')}}

SELECT
    value_segment,
    rank_num
FROM
{{ source('techflex_common', 'value_segment_rank') }} 