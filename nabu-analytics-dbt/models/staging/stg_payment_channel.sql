{{ config(schema='stg')}}

	select 
                                    TRIM(type) as type,
									TRIM(body.id) as payment_channel_id, 
                                    TRIM(body.businessUnitId) as business_unit_id,
                                    TRIM(body.type) as payment_channel_type,
									TRIM(body.name) as payment_channel_name,
                                    CAST(body.updated AS TIMESTAMP) as date_updated

							from {{ source('raw', 'payment_channels') }} 
							where type='CREATED'