{{ config(schema='stg')}}

SELECT
    -- businessunit is part of the key because body.businessunitids is unnested and one record coming from Kafka can have multiple businessunit values per message
    FARM_FINGERPRINT(CONCAT(kafkaData.topic,':',kafkaData.PARTITION,':',kafkaData.offset,':', businessunit)) AS player_key,
    TRIM(body.id) AS player_id,
    TRIM(businessunit) AS businessunit_id,
    TRIM(INITCAP(body.firstName)) AS first_name,
    TRIM(INITCAP(body.lastName)) AS last_name,
    TRIM(body.displayName) as display_name,
    TRIM(body.gender) AS gender,
    TRIM(body.address) AS address,
    TRIM(body.country) AS country_code,
    TRIM(INITCAP(body.city)) AS city,
    TRIM(body.postalCode) AS postal_code,
    TRIM(body.currency) AS currency_code,
    TRIM(body.phoneNumber) AS phone_number,
    TRIM(body.email) AS email,
    CAST(body.terms AS BOOL) AS terms,
    CAST(body.privacyPolicy AS BOOL) AS privacy_policy,
    TRIM(body.kycStatus) AS kyc_status,
    TRIM(body.playerStatus) AS player_status,
    body.eligibilities AS eligibilities,
    CAST(body.birthDate AS DATE) AS birth_date,
    CAST(body.registrationDate AS TIMESTAMP) AS registration_date,
    TRIM(body.affiliateTag) AS affiliate_tag,
    CAST(body.updated AS TIMESTAMP) AS date_updated,
    TRIM(body.updatedBy) AS updated_by,
    TRIM(body.updatedByUsername) AS updated_by_username,
    CAST(body.testAccount AS BOOL) AS test_account,
    TRIM(body.manualAffiliateTag) AS manual_affiliate_tag,
    CAST(body.numericId AS NUMERIC) AS numeric_id,
    type
FROM {{ source('raw', 'players') }},
UNNEST(body.businessunitids) AS businessunit