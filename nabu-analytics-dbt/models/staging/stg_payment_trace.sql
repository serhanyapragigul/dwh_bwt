{{ config(schema='stg') }}

SELECT  FARM_FINGERPRINT(CONCAT(kafkaData.topic,':',kafkaData.PARTITION,':',kafkaData.offset,':',body.id)) AS payment_trace_key,
        TRIM(body.id) AS payment_trace_id, 
        TRIM(body.paymentId) AS payment_id,
        CAST(body.created AS TIMESTAMP) AS date_created,
        TRIM(body.type) AS type,
        TRIM(body.status) AS status,
        TRIM(body.title) AS title,
        TRIM(body.body) AS response,
        _PARTITIONTIME as partition_time
FROM {{ source('raw', 'payment_traces') }}