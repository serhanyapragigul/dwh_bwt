{{ config(schema='stg')}}


          
select  
     TRIM(type) as type, 
        TRIM(body.id) as payment_provider_id,
        TRIM(body.name) as name,
        TRIM(body.apiUrl) as api_url, 
        kafkaData.insertTime as date_created, 
        
        TRIM(body.type) as provider_type,
        TRIM(body.businessUnitId) as business_unit_id


     from {{ source('raw', 'payment_providers') }}
union all   
select
 'N/A' as type, 
	'0' as payment_provider_id,
  'N/A' as name,
  'N/A' as api_url, 
  CAST('1900-01-01 00:00:00' AS TIMESTAMP) as created, 
 
  'N/A' as provider_type,
  'N/A' as business_unit_id