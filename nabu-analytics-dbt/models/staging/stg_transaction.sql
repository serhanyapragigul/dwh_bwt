{{ config(schema='stg')}}


SELECT  type,
        FARM_FINGERPRINT(CONCAT(body.id,':', type,':', body.date)) AS transaction_key,
        TRIM(body.id) as transaction_id, 
        TRIM(body.businessUnitId) as business_unit_id ,
        TRIM(body.playerId) as player_id,
        TRIM(body.type) as transaction_type ,
        TRIM(body.currency) as currency,
        CAST(body.amount AS NUMERIC) amount,
        CAST(body.balance AS NUMERIC) balance, 
        CAST(body.locked AS NUMERIC) as locked, 
        CAST(body.bonus AS NUMERIC) as bonus_balance,
        CAST(body.bonusLocked AS NUMERIC) as  bonus_locked,
        TRIM(body.info) as info, 
        TRIM(body.gameId) as internal_game_id,  
        TRIM(body.gameRound) as game_round,
        TRIM(body.internalReference) as internal_reference,
        TRIM(body.externalReference) as external_reference,
        CAST(body.date as timestamp) as date_created,
        TRIM(body.subType) sub_type,
        TRIM(body.category) as category,
        TRIM(body.providerGameId) as provider_game_id, 
        TRIM(body.bonusId) as bonus_id,
        TRIM(body.providerType) as provider_type,
        TRIM(body.title) as title,
        _PARTITIONTIME as partition_time,
        kafkaData.insertTime AS kafka_insert_time
FROM  {{ source('raw', 'transactions') }}  tr




