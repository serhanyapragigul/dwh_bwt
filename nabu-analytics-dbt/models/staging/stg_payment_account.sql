{{ config(schema='stg')}}

SELECT  `type`,
        TRIM(body.id) as payment_accounts_id, 
        TRIM(body.playerId) as player_id, 
        TRIM(body.externalAccountId) as external_account_id,
        TRIM(body.methodType) as method_type,
        TRIM(body.kycStatus) as kyc_status,
        TRIM(body.accountStatus) as account_status,
        CAST(body.multiUsage AS BOOL) as multi_usage,
        CAST(body.updated AS TIMESTAMP) as date_updated,
        TRIM(body.country) as country,
        TRIM(body.cryptoDetails.cryptocurrency) as crypto_currency,
        TRIM(body.cryptoDetails.destinationTag) as destination_tag
    FROM  {{ source('raw', 'payment_accounts_v2') }}
