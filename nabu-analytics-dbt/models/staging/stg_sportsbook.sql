{{ config(schema='stg')}}

SELECT
    TRIM(body.betcashedout.cashoutId) as bet_cashedout_cashout_id,
		TRIM(body.betcashedout.cashoutState) as bet_cashedout_cashout_state,
		TRIM(body.betcashedout.segmentId) as bet_cashedout_segment_id,
		TRIM(body.betcashedout.correlationId) as bet_cashedout_correlation_id,
		TRIM(body.betcashedout.brandId) as bet_cashedout_brand_id,
		CAST(body.betcashedout.dateTimeStamp as TIMESTAMP) as bet_cashedout_date_time_stamp,
		CAST(body.betcashedout.processedDate as TIMESTAMP) as bet_cashedout_processed_date,
		TRIM(body.betcashedout.redeemedStake.userCurrency.currencyCode) as bet_cashedout_redeemed_stake_user_currency_currency_code,
		TRIM(body.betcashedout.redeemedStake.userCurrency.amount) as bet_cashedout_redeemed_stake_user_currency_amount,
		TRIM(body.betcashedout.redeemedStake.baseCurrency.currencyCode) as bet_cashedout_redeemed_base_currency_currency_code, 
		TRIM(body.betcashedout.redeemedStake.baseCurrency.amount) as bet_cashedout_redeemed_base_currencyamount,
		TRIM(body.betcashedout.bet.couponId) as bet_cashedout_bet_coupon_id,
		TRIM(body.betcashedout.bet.betType) as bet_cashedout_bet_bet_type,
		TRIM(body.betcashedout.bet.bonusType) as bet_cashedout_bet_bonus_type,
		TRIM(body.betcashedout.bet.reviewStatus) as bet_cashedout_bet_review_status,
		TRIM(body.betcashedout.bet.betStatus) as bet_cashedout_bet_bet_status,
		TRIM(body.betcashedout.bet.resultStatus) as bet_cashedout_bet_result_status,
		TRIM(body.betcashedout.bet.closedReason) as bet_cashedout_bet_closed_reason,
		TRIM(body.betcashedout.bet.gamePhase) as bet_cashedout_bet_game_phase,
		TRIM(body.betcashedout.bet.channel) as bet_cashedout_bet_channed,
		TRIM(body.betcashedout.bet.betId) as bet_cashedout_bet_bet_id,
		TRIM(body.betcashedout.bet.customer.sportsbookCustomerId) bet_cashedout_bet_customer_sportsbook_customer_id,
		TRIM(body.betcashedout.bet.customer.partnerCustomerId) as bet_cashedout_bet_customer_partner_customer_id,
		TRIM(body.betcashedout.bet.customer.partnerId) as bet_cashedout_bet_customer_partner_id,
		CAST(body.betcashedout.bet.combinedOdds.value as FLOAT64) as bet_cashedout_bet_combined_odds_value,
		CAST(body.betcashedout.bet.combinedOdds.boostedvalue as FLOAT64) as bet_cashedout_bet_combined_odds_boosted_value,
		CAST(body.betcashedout.bet.odds.value as FLOAT64) asbet_cashedout_bet_odds_value,
		CAST(body.betcashedout.bet.odds.boostedValue as FLOAT64) as bet_cashedout_bet_odds_boosted_value,
		TRIM(body.betcashedout.bet.betStake.usercurrency.currencyCode) as bet_cashedout_bet_bet_stake_user_currency_currency_code,
		CAST(body.betcashedout.bet.betStake.usercurrency.amount as FLOAT64) as bet_cashedout_bet_bet_stake_user_currency_amount,
		TRIM(body.betcashedout.bet.betStake.baseCurrency.currencyCode) as bet_cashedout_bet_stake_base_currency_currency_code,
		CAST(body.betcashedout.bet.betStake.basecurrency.amount as FLOAT64) as bet_cashedout_bet_bet_stake_base_currency_amount,
		TRIM(body.betcashedout.bet.totalRedeemedStake.usercurrency.currencyCode) as bet_cashedout_bet_total_redeemed_stake_user_curency_currency_code,
		CAST(body.betcashedout.bet.totalRedeemedStake.usercurrency.amount as FLOAT64) as bet_cashedout_bet_total_redeemed_stake_user_currency_amount,
		TRIM(body.betcashedout.bet.totalRedeemedStake.baseCurrency.currencyCode) as bet_cashedout_bet_total_redeemed_stake_base_currency_currency_code,
		CAST(body.betcashedout.bet.totalRedeemedStake.basecurrency.amount AS FLOAT64) as bet_cashedout_bet_total_redeemed_stake_base_currency_amount,
		body.betcashedout.bet.payoutsTotal as bet_cashedout_bet_payouts_total,
		body.betcashedout.bet.betSelections as bet_cashedout_bet_bet_selections,
    TRIM(body.cashoutStateChanged.cashoutId) as cashout_state_changed_cashout_id,
		TRIM(body.cashoutStateChanged.cashoutState) as cashout_state_changed_cashout_state,
		TRIM(body.cashoutStateChanged.segmentId) as cashout_state_changed_segment_id,
		TRIM(body.cashoutStateChanged.correlationId) as cashout_state_changed_correlation_id,
		TRIM(body.cashoutStateChanged.brandId) as cashout_state_changed_brand_id,
		CAST(body.cashoutStateChanged.dateTimeStamp as TIMESTAMP) as cashout_state_changed_date_time_stamp,
		CAST(body.cashoutStateChanged.changedDate as TIMESTAMP) as cashout_state_changed_changed_date,
		TRIM(body.cashoutStateChanged.redeemedStake.userCurrency.currencyCode) as cashout_state_changed_redeemed_stake_user_currency_currency_code,
		CAST(body.cashoutStateChanged.redeemedStake.userCurrency.amount as FLOAT64) as cashout_state_changed_redeemed_stake_user_currency_amount,
		TRIM(body.cashoutStateChanged.redeemedStake.baseCurrency.currencyCode) as cashout_state_changed_redeemed_stake_base_currency_currency_code,
		CAST(body.cashoutStateChanged.redeemedStake.baseCurrency.amount as FLOAT64) as cashout_state_changed_redeemed_stake_case_currency_amount,
		TRIM(body.cashoutStateChanged.bet.couponId) as cashout_state_changed_bet_coupon_id,
		TRIM(body.cashoutStateChanged.bet.betType) as cashout_state_changed_bet_bet_type,
		TRIM(body.cashoutStateChanged.bet.bonusType) as cashout_state_changed_bet_bonus_type,
		TRIM(body.cashoutStateChanged.bet.reviewStatus) as cashout_state_changed_bet_review_status,
		TRIM(body.cashoutStateChanged.bet.betStatus) as cashout_state_changed_bet_bet_status,
		TRIM(body.cashoutStateChanged.bet.resultStatus) as cashout_state_changed_bet_result_status,
		TRIM(body.cashoutStateChanged.bet.closedReason) as cashout_state_changed_bet_closed_reason,
		TRIM(body.cashoutStateChanged.bet.gamePhase) as cashout_state_changed_bet_game_phase,
		TRIM(body.cashoutStateChanged.bet.channel) as cashout_state_changed_bet_channed,
		TRIM(body.cashoutStateChanged.bet.betId) as cashout_state_changed_bet_bet_id,
		TRIM(body.cashoutStateChanged.bet.customer.sportsbookCustomerId) cashout_state_changed_bet_customer_sportsbook_customer_id,
		TRIM(body.cashoutStateChanged.bet.customer.partnerCustomerId) as cashout_state_changed_bet_customer_partner_customer_id,
		TRIM(body.cashoutStateChanged.bet.customer.partnerId) as cashout_state_changed_bet_customer_partner_id,
		CAST(body.cashoutStateChanged.bet.combinedOdds.value as FLOAT64) as cashout_state_changed_bet_combined_odds_value,
		CAST(body.cashoutStateChanged.bet.combinedOdds.boostedvalue as FLOAT64) as cashout_state_changed_bet_combined_odds_boosted_value,
		CAST(body.cashoutStateChanged.bet.odds.value as FLOAT64) as cashout_state_changed_bet_odds_value,
		CAST(body.cashoutStateChanged.bet.odds.boostedValue as FLOAT64) as cashout_state_changed_bet_odds_boosted_value,
		TRIM(body.cashoutStateChanged.bet.betStake.usercurrency.currencyCode) as cashout_state_changed_bet_bet_stake_user_currency_currency_code,
		CAST(body.cashoutStateChanged.bet.betStake.usercurrency.amount as FLOAT64) as cashout_state_changed_bet_bet_stake_user_currency_amount,
		TRIM(body.cashoutStateChanged.bet.betStake.baseCurrency.currencyCode) as cashout_state_changed_bet_stake_base_currency_currency_code,
		CAST(body.cashoutStateChanged.bet.betStake.basecurrency.amount as FLOAT64) as cashout_state_changed_bet_bet_stake_base_currency_amount,
		TRIM(body.cashoutStateChanged.bet.totalRedeemedStake.usercurrency.currencyCode) as cashout_state_changed_bet_total_redeemed_stake_user_curency_currency_code,
		CAST(body.cashoutStateChanged.bet.totalRedeemedStake.usercurrency.amount as FLOAT64) as cashout_state_changed_bet_total_redeemed_stake_user_currency_amount,
		TRIM(body.cashoutStateChanged.bet.totalRedeemedStake.baseCurrency.currencyCode) as cashout_state_changed_bet_total_redeemed_stake_base_currency_currency_code,
		CAST(body.cashoutStateChanged.bet.totalRedeemedStake.basecurrency.amount AS FLOAT64) as cashout_state_changed_bet_total_redeemed_stake_base_currency_amount,
		body.cashoutStateChanged.bet.payoutsTotal as cashout_state_changed_bet_payouts_total,
		body.cashoutStateChanged.bet.betSelections as cashout_state_changed_bet_bet_selections,
    TRIM(body.betStatusChanged.segmentId) as bet_status_changed_segment_id,
		TRIM(body.betStatusChanged.correlationId) as bet_status_changed_correlation_id,
		TRIM(body.betStatusChanged.brandId) as bet_status_changed_brand_id,
		CAST(body.betStatusChanged.dateTimeStamp as TIMESTAMP) as bet_status_changed_date_time_stamp ,
		CAST(body.betStatusChanged.changedDate as TIMESTAMP)  as bet_status_changed_changed_date,
		TRIM(body.betStatusChanged.bet.couponId) as bet_status_changed_bet_coupon_id,
		TRIM(body.betStatusChanged.bet.betType) as bet_status_changed_bet_bet_type,
		TRIM(body.betStatusChanged.bet.bonusType) as bet_status_changed_bet_bonus_type,
		TRIM(body.betStatusChanged.bet.reviewStatus) as bet_status_changed_bet_review_status,
		TRIM(body.betStatusChanged.bet.betStatus) as bet_status_changed_bet_bet_status,
		TRIM(body.betStatusChanged.bet.resultStatus) as bet_status_changed_bet_result_status,
		TRIM(body.betStatusChanged.bet.closedReason) as bet_status_changed_bet_closed_reason,
		TRIM(body.betStatusChanged.bet.gamePhase) as bet_status_changed_bet_game_phase,
		TRIM(body.betStatusChanged.bet.channel) as bet_status_changed_bet_channed,
		TRIM(body.betStatusChanged.bet.betId) as bet_status_changed_bet_bet_id,
		TRIM(body.betStatusChanged.bet.customer.sportsbookCustomerId) bet_status_changed_bet_customer_sportsbook_customer_id,
		TRIM(body.betStatusChanged.bet.customer.partnerCustomerId) as bet_status_changed_bet_customer_partner_customer_id,
		TRIM(body.betStatusChanged.bet.customer.partnerId) as bet_status_changed_bet_customer_partner_id,
		CAST(body.betStatusChanged.bet.combinedOdds.value as FLOAT64) as bet_status_changed_bet_combined_odds_value,
		CAST(body.betStatusChanged.bet.combinedOdds.boostedvalue as FLOAT64) as bet_status_changed_bet_combined_odds_boosted_value,
		CAST(body.betStatusChanged.bet.odds.value as FLOAT64) as bet_status_changed_bet_odds_value,
		CAST(body.betStatusChanged.bet.odds.boostedValue as FLOAT64) as bet_status_changed_bet_odds_boosted_value,
		TRIM(body.betStatusChanged.bet.betStake.usercurrency.currencyCode) as bet_status_changed_bet_bet_stake_user_currency_currency_code,
		CAST(body.betStatusChanged.bet.betStake.usercurrency.amount as FLOAT64) as bet_status_changed_bet_bet_stake_user_currency_amount,
		TRIM(body.betStatusChanged.bet.betStake.baseCurrency.currencyCode) as bet_status_changed_bet_stake_base_currency_currency_code,
		CAST(body.betStatusChanged.bet.betStake.basecurrency.amount as FLOAT64) as bet_status_changed_bet_bet_stake_base_currency_amount,
		TRIM(body.betStatusChanged.bet.totalRedeemedStake.usercurrency.currencyCode) as bet_status_changed_bet_total_redeemed_stake_user_curency_currency_code,
		CAST(body.betStatusChanged.bet.totalRedeemedStake.usercurrency.amount as FLOAT64) as bet_status_changed_bet_total_redeemed_stake_user_currency_amount,
		TRIM(body.betStatusChanged.bet.totalRedeemedStake.baseCurrency.currencyCode) as bet_status_changed_bet_total_redeemed_stake_base_currency_currency_code,
		CAST(body.betStatusChanged.bet.totalRedeemedStake.basecurrency.amount AS FLOAT64) as bet_status_changed_bet_total_redeemed_stake_base_currency_amount,
		body.betStatusChanged.bet.payoutsTotal as bet_status_changed_bet_payouts_total,
		body.betStatusChanged.bet.betSelections as bet_status_changed_bet_bet_selections,
    TRIM(body.betResultStatusChanged.segmentId) as bet_result_status_changed_bet_segment_id,
		TRIM(body.betResultStatusChanged.correlationId) as bet_result_status_changed_bet_correlation_id ,
		TRIM(body.betResultStatusChanged.brandId) as bet_result_status_changed_bet_brand_id,
		CAST(body.betResultStatusChanged.dateTimeStamp as TIMESTAMP) as bet_result_status_changed_bet_date_time_stamp,
		CAST(body.betResultStatusChanged.changedDate as TIMESTAMP) as bet_result_status_changed_bet_changed_date, 
		TRIM(body.betResultStatusChanged.bet.couponId) as bet_result_status_changed_bet_coupon_id,
		TRIM(body.betResultStatusChanged.bet.betType) as bet_result_status_changed_bet_bet_type,
		TRIM(body.betResultStatusChanged.bet.bonusType) as bet_result_status_changed_bet_bonus_type,
		TRIM(body.betResultStatusChanged.bet.reviewStatus) as bet_result_status_changed_bet_review_status,
		TRIM(body.betResultStatusChanged.bet.betStatus) as bet_result_status_changed_bet_bet_status,
		TRIM(body.betResultStatusChanged.bet.resultStatus) as bet_result_status_changed_bet_result_status,
		TRIM(body.betResultStatusChanged.bet.closedReason) as bet_result_status_changed_bet_closed_reason,
		TRIM(body.betResultStatusChanged.bet.gamePhase) as bet_result_status_changed_bet_game_phase,
		TRIM(body.betResultStatusChanged.bet.channel) as bet_result_status_changed_bet_channed,
		TRIM(body.betResultStatusChanged.bet.betId) as bet_result_status_changed_bet_bet_id,
		TRIM(body.betResultStatusChanged.bet.customer.sportsbookCustomerId) bet_result_status_changed_bet_customer_sportsbook_customer_id,
		TRIM(body.betResultStatusChanged.bet.customer.partnerCustomerId) as bet_result_status_changed_bet_customer_partner_customer_id,
		TRIM(body.betResultStatusChanged.bet.customer.partnerId) as bet_result_status_changed_bet_customer_partner_id,
		CAST(body.betResultStatusChanged.bet.combinedOdds.value as FLOAT64) as bet_result_status_changed_bet_combined_odds_value,
		CAST(body.betResultStatusChanged.bet.combinedOdds.boostedvalue as FLOAT64) as bet_result_status_changed_bet_combined_odds_boosted_value,
		CAST(body.betResultStatusChanged.bet.odds.value as FLOAT64) as bet_result_status_changed_bet_odds_value,
		CAST(body.betResultStatusChanged.bet.odds.boostedValue as FLOAT64) as bet_result_status_changed_bet_odds_boosted_value,
		TRIM(body.betResultStatusChanged.bet.betStake.usercurrency.currencyCode) as bet_result_status_changed_bet_bet_stake_user_currency_currency_code,
		CAST(body.betResultStatusChanged.bet.betStake.usercurrency.amount as FLOAT64) as bet_result_status_changed_bet_bet_stake_user_currency_amount,
		TRIM(body.betResultStatusChanged.bet.betStake.baseCurrency.currencyCode) as bet_result_status_changed_bet_stake_base_currency_currency_code,
		CAST(body.betResultStatusChanged.bet.betStake.basecurrency.amount as FLOAT64) as bet_result_status_changed_bet_bet_stake_base_currency_amount,
		TRIM(body.betResultStatusChanged.bet.totalRedeemedStake.usercurrency.currencyCode) as bet_result_status_changed_bet_total_redeemed_stake_user_curency_currency_code,
		CAST(body.betResultStatusChanged.bet.totalRedeemedStake.usercurrency.amount as FLOAT64) as bet_result_status_changed_bet_total_redeemed_stake_user_currency_amount,
		TRIM(body.betResultStatusChanged.bet.totalRedeemedStake.baseCurrency.currencyCode) as bet_result_status_changed_bet_total_redeemed_stake_base_currency_currency_code,
		CAST(body.betResultStatusChanged.bet.totalRedeemedStake.basecurrency.amount AS FLOAT64) as bet_result_status_changed_bet_total_redeemed_stake_base_currency_amount,
		body.betResultStatusChanged.bet.payoutsTotal as bet_result_status_changed_bet_payouts_total,
		body.betResultStatusChanged.bet.betSelections as bet_result_status_changed_bet_bet_selections,
    TRIM(body.couponPlaced.segmentID) as coupon_placed_segment_id,
		TRIM(body.couponPlaced.correlationId) as coupon_placed_correlation_id,
		TRIM(body.couponPlaced.brandId) as coupon_placed_brand_id,
		CAST(body.couponPlaced.dateTimeStamp as TIMESTAMP) as coupon_placed_date_time_stamp,
		TRIM(body.couponPlaced.coupon.couponId) as coupon_placed_coupon_coupon_id,
		TRIM(body.couponPlaced.coupon.couponStatus) as coupon_placed_coupon_coupon_status,
		CAST(body.couponPlaced.coupon.placedDate AS TIMESTAMP) as coupon_placed_coupon_placed_date,
		body.couponPlaced.coupon.bets as coupon_placed_coupon_bets,
    TRIM(body.couponStateChanged.segmentId) as coupon_state_changed_segment_id,
		TRIM(body.couponStateChanged.correlationId) as coupon_state_changed_correlation_id,
		TRIM(body.couponStateChanged.brandId) as coupon_state_changed_brand_id,
		CAST(body.couponStateChanged.dateTimestamp as TIMESTAMP) as coupon_state_changed_date_time_stamp,
		CAST(body.couponStateChanged.changedDate as TIMESTAMP) as coupon_state_changed_changed_date,
		TRIM(body.couponStateChanged.coupon.couponId) as coupon_state_changed_coupon_coupon_id,
		TRIM(body.couponStateChanged.coupon.couponStatus) as coupon_state_changed_coupon_coupon_status,
        body.couponStateChanged.coupon.bets as coupon_state_changed_coupon_bets,
		kafkaData.insertTime AS kafka_insert_time,
		_PARTITIONTIME as partition_time
FROM {{ source('raw', 'sportsbook_bets') }} 