{{ config(schema='stg')}}

SELECT  FARM_FINGERPRINT(CONCAT(kafkaData.topic,':',kafkaData.PARTITION,':',kafkaData.offset,':',body.id)) AS adjustment_key,
        TRIM(body.id) AS adjustment_id, 
        TRIM(body.playerId) AS player_id, 
        TRIM(body.businessUnitId) AS business_unit_id, 
        TRIM(body.adjustmentTypeId) AS adjustment_type_id, 
        CAST(body.amount AS NUMERIC) AS amount, 
        TRIM(body.currency) AS currency, 
        TRIM(body.strategy) AS strategy, 
        CAST(body.selectedAmount AS NUMERIC) AS transaction_amount, 
        TRIM(body.selectedCurrency) AS transaction_currency, 
        TRIM(body.baseCurrency) AS base_currency, 
        CAST(body.baseAmount AS NUMERIC) AS base_amount, 
        CAST(body.created AS TIMESTAMP) AS date_created, 
        CAST(body.updated AS TIMESTAMP) AS date_updated, 
        TRIM(body.adjustmentRequestId) AS adjustment_request_id, 
        TRIM(body.status) AS status,
        TRIM(body.comment) AS comment, 
        TRIM(body.createdByUsername) AS created_by, 
        TRIM(body.updatedByUsername) AS changed_by,
        _PARTITIONTIME AS partition_time
FROM {{ source('raw', 'adjustments') }}  
--there are some records for DC.in which affects data and we don't have confirmed logic yet
--moreover, DC.in isn't launched yet, so these records can be considered test records in prod env
--this is a temporary solution, will be fixed when DC.in is launched
WHERE body.id NOT IN (
    'd779886d-8b69-481b-baf4-109a4b398a27',
    '3460b3ea-2995-4245-9f69-62247c5f6c1c'
)


