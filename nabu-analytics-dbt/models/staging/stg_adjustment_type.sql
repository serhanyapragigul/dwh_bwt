    {{ config(schema='stg')}}

    
    select 
    TRIM(body.id) as adjustment_type_id, 
    TRIM(body.name) as adjustment_type_name, 
    TRIM(body.businessUnitId) as business_unit_id, 
    TRIM(body.strategy) as strategy, 
    CAST(body.disabled as bool) as disabled, 
    CAST(body.updated as Timestamp) as date_updated, 
    ROW_NUMBER() OVER (PARTITION BY body.id ORDER by body.updated ASC) AS row_no
    from  {{ source('raw', 'adjustment_types') }}  
    group by 
    body.id, 
    body.name, 
    body.businessUnitId, 
    body.strategy, 
    body.disabled, 
    body.updated


    ----