{{ config(schema='stg')}}

SELECT guid AS player_id,
       franchise as franchise_name,
       business_unit_id,
       most_staked_sport,
       most_staked_league,
       most_staked_market,
       most_staked_team,
       odd_preference,
       stake_preference,
       bet_type,
       played_sport_variety,
       execution_time
FROM
{{ source('techflex_segment', 'sb_sub_segment') }} a
