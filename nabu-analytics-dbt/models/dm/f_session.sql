{{
	config(
		schema='dm',
		materialized='incremental',
		unique_key = 'session_id',
		cluster_by = 'player_id'
		)
}}

SELECT
	session_id,
	player_id,
	business_unit_id,
	franchise_name,
	session_start,
	session_end, 
	session_duration,
	country,
	city,
	ip,
	user_agent,
	browser,
	browser_version,
	platform,	
	platform_version,
	device_type
FROM(
with 
 cte_session_log AS (
  SELECT
    DISTINCT
    session_id,
	player_id,
	business_unit_id,
	country,
	city,
	ip,
	user_agent,
	browser,
	browser_version,
	platform,	
	platform_version,
	device_type,
    FIRST_VALUE( IF (type = 'CREATED', date_created,NULL) IGNORE NULLS) OVER (PARTITION BY session_id ORDER BY session_id) AS date_created,
    FIRST_VALUE( IF (type = 'CREATED', date_expiration, NULL) IGNORE NULLS) OVER (PARTITION BY session_id ORDER BY session_id) AS date_expiration_Original,
    MAX( IF (type = 'UPDATED', date_expiration, NULL)) OVER (PARTITION BY session_id) AS date_expiration_Updated,
    -- Multiple deleted ROWS sometimes exist
    MIN( IF (type = 'DELETED', date_expiration, NULL)) OVER (PARTITION BY session_id) AS date_expiration_Deleted 
  FROM {{ref('dwh_session')}}
 	--
  ), 
  cte_session as ( 
    SELECT
    session_id,
	player_id,
	business_unit_id,
	country,
	city,
	ip,
	user_agent,
	browser,
	browser_version,
	platform,	
	platform_version,
	device_type,
    date_created AS session_start,
    CASE  WHEN date_expiration_Deleted is not null then date_expiration_Deleted
            ELSE COALESCE(date_expiration_Updated, date_expiration_Original) 
    END AS session_end,
    date_expiration_Original, date_expiration_Updated, date_expiration_Deleted
    FROM cte_session_log )
    
SELECT
session_id,
	player_id,
	business_unit_id,
	country,
	city,
	ip,
	user_agent,
	browser,
	browser_version,
	platform,	
	platform_version,
	device_type,
	session_start,
	session_end, 
	date_diff(session_end,session_start, MINUTE) as session_duration
 from cte_session
  {% if is_incremental() %}
				-- this filter will only be applied on an incremental run
				where session_end > TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
  {% endif %}
 )

LEFT JOIN {{ ref('d_business_unit_hierarchy') }} buh on business_unit_id = buh.franchise_id where buh.most_recent is TRUE
