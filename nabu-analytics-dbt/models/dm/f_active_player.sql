    {{
    config(
        schema='dm',
        materialized='incremental',
        unique_key = 'activity_id',
        cluster_by = ['player_id', 'business_unit_id', 'activity_date']
        )
    }}

    SELECT  bet.player_id,
            bet.business_unit_id,
            TIMESTAMP_TRUNC(bet.date_created, DAY) AS activity_date,
            FARM_FINGERPRINT(CONCAT(MIN(bet.player_id), MIN(bet.business_unit_id), MIN(TIMESTAMP_TRUNC(bet.date_created, DAY)))) AS activity_id
    FROM {{ ref('f_bet') }} AS bet
        INNER JOIN {{ ref('f_ndc') }} AS ndc
            ON bet.player_id = ndc.player_id AND bet.business_unit_id = ndc.business_unit_id
    WHERE bet.final_status IN ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS')
        AND CAST(bet.date_created AS DATE) >= CAST(ndc.ndc_date AS DATE)
            AND bet.total_bet_amount_eur > 0
                {% if is_incremental() %}
                AND bet.date_created >= (SELECT MAX(activity_date) FROM {{ this }})
                {% endif -%}
    GROUP BY    bet.player_id,
                bet.business_unit_id,
                activity_date
