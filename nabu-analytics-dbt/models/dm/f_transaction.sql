{{
	config(
		schema='dm',
		materialized='incremental',
		unique_key = 'transaction_id',
		cluster_by = 'date_created'
		)
}}

WITH payment AS (
	SELECT	method_type,
			payment_id
	FROM {{ ref('dwh_payment') }}
	WHERE type = 'CREATED'
)
SELECT	tr.transaction_id, 
		tr.business_unit_id ,
		tr.player_id,
		tr.transaction_type ,
		tr.sub_type,
		tr.category,
		tr.provider_type,
		tr.provider_game_id, 
		tr.title,
		buh.franchise_name,
		tr.currency,
		tr.amount,
		tr.amount * er.exchange_rate  as amount_EUR,
		tr.balance, 
		tr.balance * er.exchange_rate  as balance_EUR,
		tr.locked,
		tr.locked * er.exchange_rate  as locked_EUR,
		tr.bonus_balance,
		tr.bonus_balance * er.exchange_rate  as bonus_balance_EUR,
		tr.bonus_locked,
		tr.bonus_locked * er.exchange_rate  as bonus_locked_EUR,
		tr.info, 
		tr.internal_game_id, 
		tr.game_round,
		tr.internal_reference,
		tr.external_reference,
		tr.date_created,
		tr.bonus_id,
		p.method_type
FROM  {{ ref('dwh_transaction') }} AS tr
	LEFT JOIN {{ref('dwh_exchange_rate')}} AS er 
	ON tr.currency = er.base_currency 
		AND date_created >= er.start_date 
			AND date_created < er.end_date 
				AND er.Currency = 'EUR'
		LEFT JOIN payment AS p 
		ON p.payment_id = tr.internal_reference
			LEFT JOIN {{ref('d_business_unit_hierarchy')}} AS buh
				ON tr.business_unit_id = buh.franchise_id 
					AND buh.most_recent
{% if is_incremental() %}
-- this filter will only be applied on an incremental run
WHERE tr.date_created > (SELECT MAX(date_created) FROM {{ this }})
{% endif %}