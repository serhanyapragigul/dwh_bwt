{{
	config(
		schema='dm',
		materialized='incremental',
		unique_key = 'adjustment_id',
		cluster_by = ['date_updated','adjustment_id']
		)
}}


SELECT	adj.adjustment_id
		, adj.business_unit_id
		, buh.franchise_name
		, adj.player_id
		, adj.adjustment_type_id
		, adj.adjustment_type_name
		, adj.date_created
		, adj.transaction_currency
		, er.exchange_rate
		, adj.transaction_amount
		, adj.transaction_amount * er.exchange_rate AS transaction_amount_EUR
		, adj.base_currency
		, adj.baseAmount
		, adj.baseAmount * base_er.exchange_rate AS base_amount_eur
		, adj.created_by
		, adj.date_updated
		, adj.changed_by
		, adj.comment
		, adj.strategy
		-- calculates direction of financial impact of the affected cost allocation, from the revenue perspective, not the customer wallet perspective
		,CASE
			WHEN adj.strategy = 'INCREASE' then -1
			WHEN adj.strategy = 'DECREASE' then 1
			ELSE 0
		END AS allocation_strategy
		, adj.status
		, adj.end_status
		-- assigns affected cost allocation boolean based on adjustment name / type
		-- only one column should be allocated per type, so this will need to be reviewed if new types are added
		, LOWER(adj.adjustment_type_name) NOT LIKE '%acquisition%' and (LOWER(adj.adjustment_type_name) LIKE 'adjustment deposit%' or LOWER(adj.adjustment_type_name) LIKE 'adjustment withdrawal%') AS revenue_product_adjustment
		, LOWER(adj.adjustment_type_name) = 'discount' or LOWER(adj.adjustment_type_name) LIKE 'discount payout%' AS revenue_non_product_adjustment
		, LOWER(adj.adjustment_type_name) LIKE 'fraud transfer%' AS payment_cost_adjustment
		, LOWER(adj.adjustment_type_name) LIKE '%acquisition%' AS marketing_cost_adjustment
		, LOWER(adj.adjustment_type_name) LIKE 'deposit%' AS deposit_adjustment
		, LOWER(adj.adjustment_type_name) LIKE 'withdrawal%' AS withdrawal_adjustment
		, LOWER(adj.adjustment_type_name) LIKE '%test money%' AS test_account_adjustment
		-- extracts coupon id / bet id from the adjustment comments to join back with bet data eg. game provider etc - where adjustment is related to a specific bet transaction
		, COALESCE(regexp_extract(adj.comment, '([0-9]{10})'), regexp_extract(adj.comment, '([0-9]{7})'), regexp_extract(regexp_replace(adj.comment, '([^\\w\\s-])', ''), '([a-f0-9]{8}-[a-f0-9]{4}-[0-5][a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12})')) AS external_id
FROM ( 
		SELECT	DISTINCT adjustment_id
				, LAST_VALUE(business_unit_id IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS business_unit_id
				, LAST_VALUE(player_id IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS player_id
				, LAST_VALUE(adjustment_type_id IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS adjustment_type_id
				, LAST_VALUE(adjustment_type_name IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS adjustment_type_name
				, LAST_VALUE(transaction_currency IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS transaction_currency
				, LAST_VALUE(transaction_amount IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)  AS transaction_amount
				, LAST_VALUE(base_currency IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS base_currency
				, LAST_VALUE(base_amount IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)  AS baseAmount
				, FIRST_VALUE(date_created IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_created
				, FIRST_VALUE(created_by IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS created_by
				, LAST_VALUE(date_updated IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_updated
				, LAST_VALUE(changed_by IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS changed_by
				, LAST_VALUE(comment IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS comment
				, LAST_VALUE(strategy IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS strategy
				, FIRST_VALUE(status IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS status
				, LAST_VALUE(status IGNORE NULLS) OVER (PARTITION BY adjustment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS end_status
		FROM {{ ref('dwh_adjustment') }}
		{% if is_incremental() %}
	        WHERE adjustment_id IN (
				SELECT DISTINCT adjustment_id
				FROM {{ ref('dwh_adjustment') }}
				WHERE date_updated > (SELECT MAX(date_updated) FROM {{ this }})
			)
	    {% endif %}
	) AS adj 
		LEFT JOIN {{ref('dwh_exchange_rate')}} AS er 
			ON transaction_currency = er.base_currency 
				AND date_created >= er.start_date 
					AND date_created < er.end_date 
						AND er.Currency = 'EUR'
		LEFT JOIN {{ ref('d_business_unit_hierarchy') }} AS buh 
			ON adj.business_unit_id = buh.franchise_id 
				AND buh.most_recent				
		LEFT JOIN {{ref('dwh_exchange_rate')}} AS base_er 
			ON adj.base_currency = base_er.base_currency 
				AND adj.date_created >= base_er.start_date 
					AND adj.date_created < base_er.end_date 
						AND base_er.Currency = 'EUR'
--temporary filter out test BU because our so;ution is not ready for multi-BU logic yet
WHERE adj.business_unit_id <> '5bc0e015-7031-4214-9676-1ceaf398dd03'