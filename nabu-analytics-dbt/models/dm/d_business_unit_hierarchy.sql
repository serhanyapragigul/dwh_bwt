{{ config(schema='dm', materialized='view')}}

SELECT 
    platform_id,
    platform_name,
    platform_currency,
    operator_id,
    operator_name,
    operator_currency,
    merchant_id,
    merchant_name,
    brand_id,
    brand_name,
    franchise_id,
    franchise_name,
    host_name,
    start_date,
    end_date,
    SAFE_CAST(end_date AS string) LIKE ('%9999%') AS most_recent
FROM(

SELECT
 pl_business_unit_id as platform_id,
 pl_name as platform_name, 
 pl_curr  as platform_currency,
 op_business_unit_id as operator_id,
 op_name as operator_name, 
 op_curr as operator_currency,
 mr_business_unit_id as merchant_id, 
 mr_name as merchant_name,
 br_business_unit_id as brand_id, 
 br_name as brand_name,
 fr_business_unit_id as franchise_id, 
 fr_name as franchise_name, 
 host_name,
 SAFE_CAST(start_date AS TIMESTAMP) as start_date,
 SAFE_CAST(COALESCE(LEAD(start_date) OVER (PARTITION BY fr_business_unit_id ORDER BY start_date), '9999-12-31 00:00:00') AS TIMESTAMP) as end_date
FROM
(
select
      pl.business_unit_id as pl_business_unit_id, pl.business_unit_name as pl_name, pl.base_currency as pl_curr, 
      op.business_unit_id as op_business_unit_id, op.business_unit_name as op_name, op.base_currency as op_curr,
      mr.business_unit_id as mr_business_unit_id, mr.business_unit_name as mr_name,
      br.business_unit_id as br_business_unit_id, br.business_unit_name as br_name,
      fr.business_unit_id as fr_business_unit_id, fr.business_unit_name as fr_name, fr.host_name, 
      fr.start_date   
from {{ ref('dwh_business_unit') }} pl
left join (select business_unit_id, parent_id,  business_unit_name, base_currency, start_date, end_date 
                from {{ ref('dwh_business_unit') }}
                where UPPER(business_unit_type) = 'OPERATOR' 
                group by business_unit_id, parent_id,  business_unit_name, base_currency, start_date, end_date
            ) op on pl.business_unit_id = op.parent_id and pl.start_date <= op.start_date and pl.end_date > op.start_date 
left join (select business_unit_id, parent_id,  business_unit_name, start_date, end_date 
                from {{ ref('dwh_business_unit') }}
                where UPPER(business_unit_type) = 'MERCHANT' 
                group by business_unit_id, parent_id, business_unit_name, start_date, end_date
            ) mr on op.business_unit_id = mr.parent_id and op.start_date <= mr.start_date and op.end_date > mr.start_date 
left join (select business_unit_id, parent_id,  business_unit_name , start_date, end_date 
                from {{ ref('dwh_business_unit') }}
                where UPPER(business_unit_type) = 'BRAND' 
                group by business_unit_id, parent_id, business_unit_name, start_date, end_date
            ) br  on mr.business_unit_id = br.parent_id and mr.start_date <= br.start_date and mr.end_date > br.start_date
left join (select business_unit_id, parent_id,  business_unit_name, host_name, start_date, end_date 
                from {{ ref('dwh_business_unit') }} 
                where UPPER(business_unit_type) = 'FRANCHISE' 
                group by business_unit_id, parent_id, business_unit_name, host_name, start_date, end_date
            ) fr on br.business_unit_id = fr.parent_id and br.start_date <= fr.start_date and br.end_date > fr.start_date
where UPPER(pl.business_unit_type) = 'PLATFORM'
group by pl.business_unit_id, pl.business_unit_name, pl.base_currency, op.business_unit_id, op.business_unit_name, op.base_currency, mr.business_unit_id, mr.business_unit_name, br.business_unit_id, br.business_unit_name, fr.business_unit_id, fr.business_unit_name, fr.host_name, fr.start_date
) sub
ORDER BY fr_business_unit_id, start_date)
