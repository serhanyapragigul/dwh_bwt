   {{
	config(
		schema='dm',
		materialized='incremental',
		unique_key = 'bet_id',
		cluster_by = ['date_created','bet_id']
		)
}}

-- depends_on: {{ ref('dwh_min_bet_date') }}
-- depends_on: {{ ref('dwh_changed_bet_id') }}

WITH main AS
(
-- fetch main attibutes
SELECT
	bet.bet_id,
	bet.free_ticket,
	bet.business_unit_id,
	bet.player_id,
	bet.currency_code,
	bet.original_currency_code,
	bet.game_uuid,
	bet.game_id,
	bet.subgame_id,
	bet.internal_game_id,
	bet.provider_type,
	bet.draw_id,
	bet.draw_date,
	bet.external_reference,
	bet.transaction_status,
	bet.jackpot_contribution,
	bet.ticket_id,
	bet.ticket_reference,
	bet.ticket_amount,
	bet.ticket_cost,
	bet.game_session_key,
	bet.idempotency_id,
	bet.bet_description,
	bet.date_created,
	bet.date_updated,
	bet.total_bet_amount,
    bet.bet_amount,
    bet.cancel_bet_amount,
    bet.revert_bet_amount, 
    bet.rollback_game_round_amount, 
    bet.win_amount, 
    bet.cancel_win_amount, 
    bet.jackpot_payout_amount, 
    bet.wagered_bonus_amount, 
    bet.bonus_amount, 
	bet.jpc,
	bet.jpp,
	bet.sb_bonus_type,
	bet.sb_bet_id,
	bet.sb_game_phase,
	bet.sb_coupon_id,
	bet.type
FROM 
	(
	SELECT
		b.bet_id,
		LAST_VALUE(b.free_ticket IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS free_ticket,
		LAST_VALUE(b.business_unit_id IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS business_unit_id,
		LAST_VALUE(b.player_id IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS player_id,
		LAST_VALUE(b.currency_code IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS currency_code,
		LAST_VALUE(b.original_currency_code IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS original_currency_code,
		LAST_VALUE(b.game_uuid IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS game_uuid,
		LAST_VALUE(b.game_id IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS game_id,
		LAST_VALUE(b.subgame_id IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS subgame_id,
		LAST_VALUE(b.internal_game_id IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS internal_game_id,
		LAST_VALUE(b.provider_type IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS provider_type,
		LAST_VALUE(b.draw_id IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS draw_id,
		FIRST_VALUE(b.draw_date IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS draw_date,
		LAST_VALUE(b.external_reference IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS external_reference,
		LAST_VALUE(b.transaction_status IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS transaction_status,
		LAST_VALUE(b.jackpot_contribution IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS jackpot_contribution,
		LAST_VALUE(CASE WHEN provider_type = 'SPORTSBOOK' THEN NULL
			WHEN provider_type = 'HELIO' and ticket_id is NOT NULL THEN ticket_id
			WHEN provider_type = 'HELIO' and last_transaction_details_key = 'Ticket' THEN last_transaction_details_value END IGNORE NULLS) OVER (PARTITION BY bet_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as ticket_id,
		LAST_VALUE(b.ticket_reference IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS ticket_reference,
		COALESCE(LAST_VALUE(b.ticket_amount IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING),0) AS ticket_amount,
		COALESCE(LAST_VALUE(b.ticket_cost IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING),0) AS ticket_cost,
		LAST_VALUE(b.game_session_key IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS game_session_key,
		LAST_VALUE(b.idempotency_id IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS idempotency_id,
		LAST_VALUE(b.bet_description IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS bet_description,
		COALESCE(FIRST_VALUE(b.date_created IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING),
			FIRST_VALUE(b.date_updated IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)) AS date_created,
		LAST_VALUE(b.date_updated IGNORE NULLS ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_updated,
        SUM(CASE WHEN last_tran_rn = 1 THEN bet_amount - cancel_bet_amount - revert_bet_amount - rollback_amount ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS total_bet_amount,
        SUM(CASE WHEN last_tran_rn = 1 THEN bet_amount ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)  AS bet_amount,
        SUM(CASE WHEN last_tran_rn = 1 THEN cancel_bet_amount ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)  AS cancel_bet_amount,
        SUM(CASE WHEN last_tran_rn = 1 THEN revert_bet_amount ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)  AS revert_bet_amount, 
        SUM(CASE WHEN last_tran_rn = 1 THEN rollback_amount ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS rollback_game_round_amount, 
        SUM(CASE WHEN last_tran_rn = 1 THEN win_amount ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)  AS win_amount, 
        SUM(CASE WHEN last_tran_rn = 1 THEN cancel_win_amount ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)  AS cancel_win_amount, 
        SUM(CASE WHEN last_tran_rn = 1 THEN jackpot_payout_amount ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)  AS jackpot_payout_amount, 
        SUM(CASE WHEN last_tran_rn = 1 THEN wagered_bonus_amount ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)  AS wagered_bonus_amount, 
        SUM(CASE WHEN last_tran_rn = 1 THEN bonus_amount ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)  AS bonus_amount, 
		MAX(CASE WHEN last_tran_rn = 1 THEN jpc ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS jpc,
		MAX(CASE WHEN last_tran_rn = 1 THEN jpp ELSE 0 END) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS jpp,
		LAST_VALUE(CASE WHEN LOWER(b.last_transaction_details_key) = 'bonustype' THEN b.last_transaction_details_value END IGNORE NULLS) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as sb_bonus_type,
		LAST_VALUE(CASE WHEN LOWER(b.last_transaction_details_key) = 'betid' THEN b.last_transaction_details_value END IGNORE NULLS) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as sb_bet_id,
		LAST_VALUE(CASE WHEN LOWER(b.last_transaction_details_key) = 'selection_0_fixturephase' THEN b.last_transaction_details_value END IGNORE NULLS) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as sb_game_phase,
		LAST_VALUE(CASE WHEN b.provider_type = 'SPORTSBOOK' and b.ticket_id is not null THEN b.ticket_id
		     			WHEN b.provider_type = 'SPORTSBOOK' and b.last_transaction_details_key = 'CouponId' THEN b.last_transaction_details_value END IGNORE NULLS) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as sb_coupon_id,
        type,
		ROW_NUMBER() OVER(PARTITION BY bet_id ORDER BY date_updated asc) AS row_no
		FROM 
        (
            SELECT *,
                   	MAX(IF(b.last_transaction_type = 'WIN', b.last_transaction_amount,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS win_amount,
                    MAX(IF(b.last_transaction_type = 'JACKPOT_PAYOUT', b.last_transaction_amount,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS jackpot_payout_amount,
                    MAX(IF(b.last_transaction_type = 'CANCEL_WIN', b.last_transaction_amount,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS cancel_win_amount,
                    MAX(IF(b.last_transaction_type = 'WAGERED_BONUS', b.last_transaction_amount,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS wagered_bonus_amount,
                    MAX(IF(b.last_transaction_type = 'BET', b.last_transaction_amount,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS bet_amount,
                    MAX(IF(b.last_transaction_type = 'CANCEL_BET', b.last_transaction_amount,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS cancel_bet_amount,
                    MAX(IF(b.last_transaction_type = 'REVERT_BET', b.last_transaction_amount,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS revert_bet_amount,
                    MAX(IF(b.last_transaction_type = 'ROLLBACK_GAME_ROUND', b.last_transaction_amount,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS rollback_amount,
                    MAX(IF(b.last_transaction_type = 'WIN', b.last_transaction_bonus,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS bonus_amount,
						-- jackpot values not included in last_transaction_type = 'JACKPOT_PAYOUT'
					SUM(IF(b.last_transaction_details_key LIKE '%jpc%' OR b.last_transaction_details_key LIKE '%_contribution%', SAFE_CAST(b.last_transaction_details_value AS NUMERIC) ,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS jpc,
					SUM(IF(b.last_transaction_details_key LIKE '%jpp%' OR b.last_transaction_details_key LIKE '%_jackpot%', SAFE_CAST(b.last_transaction_details_value AS NUMERIC) ,CAST(0 AS NUMERIC))) OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS jpp,
                    ROW_NUMBER() OVER (PARTITION BY bet_id, last_transaction_type, last_transaction_id ORDER BY b.date_updated ASC) AS last_tran_rn
              FROM {{ ref('dwh_bet') }} b
                {% if is_incremental() %}
                --  because we do not know when the update will occur looking back in time fixed amount of days/hours alone does not help
                -- we get the latest point in time we need to use from dwh_min_bet_date table
                WHERE b.date_updated >= (SELECT min_date FROM {{ ref('dwh_min_bet_date') }} )
                AND b.bet_id IN (SELECT bet_id FROM {{ ref('dwh_changed_bet_id') }} )
                {% endif %}
        ) b
	) bet
WHERE bet.row_no = 1
)
SELECT 	bet_data.*, 
		bet_data.win_amount - bet_data.cancel_win_amount AS total_win_amount,
		bet_data.win_amount_eur - bet_data.cancel_win_amount_eur AS total_win_amount_eur,
		bet_data.total_bet_amount - (bet_data.win_amount - bet_data.cancel_win_amount) - bet_data.jackpot_contribution + bet_data.jackpot_payout AS game_win_amount,
		bet_data.total_bet_amount_eur - (bet_data.win_amount_eur - bet_data.cancel_win_amount_eur) - bet_data.jackpot_contribution_eur + bet_data.jackpot_payout_eur AS game_win_amount_eur,
		CASE
			WHEN (bet_data.sb_bonus_type IS NULL AND bet_data.bet_amount = 0)
			THEN bet_data.wagered_bonus_amount
			ELSE CAST(0 AS NUMERIC)
		END AS free_spin_bonus_cost,
		CASE
			WHEN (bet_data.sb_bonus_type IS NULL AND bet_data.bet_amount_eur = 0)
			THEN bet_data.wagered_bonus_amount_eur
			ELSE CAST(0 AS NUMERIC)
		END AS free_spin_bonus_cost_eur,
		CASE
			WHEN (bet_data.sb_bonus_type = 'FREE_BET' AND bet_data.bet_amount = 0)
			THEN bet_data.bonus_amount
			ELSE CAST(0 AS NUMERIC)
		END AS free_bet_bonus_cost,
		CASE
			WHEN (bet_data.sb_bonus_type = 'FREE_BET' AND bet_data.bet_amount_eur = 0)
			THEN bet_data.bonus_amount_eur
			ELSE CAST(0 AS NUMERIC)
		END AS free_bet_bonus_cost_eur,
		CASE
			WHEN bet_data.sb_bonus_type = 'RISK_FREE_BET'
			THEN bet_data.bonus_amount
			ELSE CAST(0 AS NUMERIC)
		END AS risk_free_bet_bonus_cost,
		CASE
			WHEN bet_data.sb_bonus_type = 'RISK_FREE_BET'
			THEN bet_data.bonus_amount_eur
			ELSE CAST(0 AS NUMERIC)
		END AS risk_free_bet_bonus_cost_eur,
		CASE
			WHEN bet_data.sb_bonus_type = 'PRICE_BOOST'
			THEN bet_data.bonus_amount
			ELSE CAST(0 AS NUMERIC)
		END AS price_boost_bonus_cost,
		CASE
			WHEN bet_data.sb_bonus_type = 'PRICE_BOOST'
			THEN bet_data.bonus_amount_eur
			ELSE CAST(0 AS NUMERIC)
		END AS price_boost_bonus_cost_eur

FROM 
(
	SELECT 	main.bet_id,
			main.business_unit_id,
			main.player_id,
			main.currency_code,
			main.original_currency_code,
			-- fix for historically missing gameid
			COALESCE(hist.game_uuid,main.game_uuid,main.game_id) AS game_id,
			main.subgame_id,
			main.internal_game_id,
			main.provider_type,
			main.draw_id,
			main.draw_date,
			main.external_reference,
			main.transaction_status,
			main.ticket_id,
			main.ticket_reference,
			main.game_session_key,
			main.idempotency_id,
			main.bet_description,
			main.date_created,
			main.TYPE,
			main.free_ticket,
			CASE
				WHEN main.provider_type = 'NETENT' AND main.transaction_status = 'OPEN' AND main.draw_date < current_timestamp()
					THEN 'CLOSED'
				ELSE main.transaction_status
			END AS final_status,
			CASE
				WHEN main.provider_type = 'NETENT' AND main.transaction_status = 'OPEN' AND main.draw_date < current_timestamp()
					THEN main.draw_date
				ELSE main.date_updated
			END AS final_status_date,
			main.ticket_amount,
			--calculation.total_win_amount,
			--calculation.total_win_amount * er.exchange_rate AS total_win_amount_eur,
			main.total_bet_amount ,
			main.total_bet_amount * er.exchange_rate AS total_bet_amount_eur,
			main.bet_amount,
			main.bet_amount * er.exchange_rate AS bet_amount_eur,
			main.cancel_bet_amount,
			main.cancel_bet_amount * er.exchange_rate AS cancel_bet_amount_eur,
			main.cancel_win_amount,
			main.cancel_win_amount * er.exchange_rate AS cancel_win_amount_eur,
			main.ticket_cost,
			main.ticket_cost * er.exchange_rate AS ticket_cost_eur,
			main.revert_bet_amount,
			main.revert_bet_amount * er.exchange_rate AS revert_bet_amount_eur,
			main.rollback_game_round_amount,
			main.rollback_game_round_amount * er.exchange_rate AS rollback_game_round_amount_eur,
			CASE
				WHEN main.sb_bonus_type = 'RISK_FREE_BET' AND main.bonus_amount > 0
					THEN main.win_amount  + main.bonus_amount
				WHEN main.sb_bonus_type = 'FREE_BET' AND main.bonus_amount = 0 AND main.win_amount != 0
					THEN CAST(0 AS NUMERIC)
				ELSE main.win_amount
			END AS win_amount,
			CASE
				WHEN main.sb_bonus_type = 'RISK_FREE_BET' AND main.bonus_amount > 0
					THEN  main.win_amount + main.bet_amount
				WHEN main.sb_bonus_type = 'FREE_BET' AND main.bonus_amount = 0 AND main.win_amount != 0
					THEN CAST(0 AS NUMERIC)
				ELSE main.win_amount
			END * er.exchange_rate AS win_amount_eur,
			COALESCE(main.jackpot_payout_amount, main.jpp) AS jackpot_payout,
			COALESCE(main.jackpot_payout_amount, main.jpp) * er.exchange_rate AS jackpot_payout_eur,
			COALESCE(main.jackpot_contribution, main.jpc) AS jackpot_contribution,
			COALESCE(main.jackpot_contribution, main.jpc) * er.exchange_rate AS jackpot_contribution_eur,
			main.wagered_bonus_amount,
			main.wagered_bonus_amount * er.exchange_rate AS wagered_bonus_amount_eur,
			-- calculation.bonus_amount,
			-- calculation.bonus_amount * er.exchange_rate AS bonus_amount_eur,
			-- take into account RISK_FREE_BET and FREE_BET logic
			CASE
				WHEN main.sb_bonus_type = 'FREE_BET' and main.bonus_amount = 0 and main.win_amount != 0
					THEN main.win_amount
				WHEN main.sb_bonus_type != 'RISK_FREE_BET'
					THEN main.bonus_amount
				WHEN main.bonus_amount = 0 AND main.bet_amount = main.win_amount
					THEN main.bet_amount
				ELSE CAST(0 AS NUMERIC)
			END AS bonus_amount,
			CASE
				WHEN main.sb_bonus_type = 'FREE_BET' and main.bonus_amount = 0 and main.win_amount != 0
					THEN main.win_amount
				WHEN main.sb_bonus_type != 'RISK_FREE_BET'
					THEN main.bonus_amount
				WHEN main.bonus_amount = 0 AND main.bet_amount = main.win_amount
					THEN main.bet_amount
				ELSE CAST(0 AS NUMERIC)
			END * er.exchange_rate AS bonus_amount_eur,
			main.sb_bet_id,
			main.sb_bonus_type,
			main.sb_game_phase,
			main.sb_coupon_id,
			-- placeholders for working cost / margin model, based on provider price list
			IF( CASE
					WHEN main.provider_type = 'NETENT' AND main.transaction_status = 'OPEN' AND main.draw_date < current_timestamp()
						THEN 'CLOSED'
					ELSE main.transaction_status
				END = 'CLOSED',
				COALESCE(main.subgame_id,main.game_id,hist.game_uuid,main.game_uuid),
				NULL) AS bet_cost_game_id,
			CAST(0 AS NUMERIC) AS bet_cost_amount,
			CAST(0 AS NUMERIC) * er.exchange_rate AS bet_cost_amount_eur,
			CAST(0 AS NUMERIC) AS bet_cost_percent,
			CAST(0 AS NUMERIC) AS bet_margin_amount,
			CAST(0 AS NUMERIC) * er.exchange_rate AS bet_margin_amount_eur,
			CAST(0 AS NUMERIC) AS bet_margin_percent,
			device.device_type,
			buh.franchise_name,
			er.exchange_rate
	-- in addition make calculation which are utilizing columns calculcated in bet_data subquery
	FROM main
	-- join on static table of historic game uuids 
	-- table name insted of reference is on purpose as this table is not loaded via dbt
	LEFT JOIN  dwh.dwh_bet_withgameuuid_20201117 hist 
		ON hist.bet_id = main.bet_id
	LEFT JOIN {{ref('dwh_exchange_rate')}} er 
		ON (main.currency_code = er.base_currency 
			AND main.date_created >= er.start_date 
				AND main.date_created < er.end_date 
					AND er.currency = 'EUR')
	LEFT JOIN {{ ref('d_business_unit_hierarchy') }} buh 
		ON main.business_unit_id = buh.franchise_id 
	LEFT JOIN (		
		SELECT a.game_session_id,
			b.device_type
		FROM {{ ref('f_game_session') }} a
		LEFT JOIN {{ ref('f_session') }} b
			ON a.session_id = b.session_id 
		) device
		ON main.game_session_key = device.game_session_id
	WHERE buh.most_recent IS TRUE
) bet_data

 