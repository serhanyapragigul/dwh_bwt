{{ config(schema='dm', materialized='view')}}


-- d_affiliate_channel table we store affiliate channel names that are provided by business, by default all new affiliateId channels should be set as 'Affiliate'

SELECT  DISTINCT p.affiliate_id as affiliate_general_id, --grab characters before _ only to generate general id
        COALESCE(c.channel, 'Affiliate') as channel, 
        if(c.channel is null or c.confirmed is null , 'Auto Generated Channel Name', 'Business provided channel name') as channel_source
FROM {{ ref('d_player') }} p
    LEFT JOIN  {{ source('dwh', 'dwh_ext_affiliate_channel') }} c 
        ON p.affiliate_id = c.affiliateId
WHERE   p.affiliate_tag is not null
