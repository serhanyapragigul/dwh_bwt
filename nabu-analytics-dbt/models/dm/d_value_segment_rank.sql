{{ config(schema='dm', materialized='view')}}
SELECT  value_segment, 
        rank_num,
        effective_from,
        effective_to
   FROM {{ref('dwh_value_segment_rank_history')}}
 WHERE effective_to = DATE('9999-12-31')