{{
	config(
		schema='dm',
		materialized='incremental',
		unique_key = 'payment_id',
		cluster_by = ['date_updated','payment_id']
		)
}}
{% if is_incremental() %}
	{%- call statement('get_ids_to_update', fetch_result=True) -%}
		SELECT 	ARRAY_AGG(DISTINCT payment_id), 
				CASE 
					WHEN ARRAY_LENGTH(ARRAY_AGG(DISTINCT payment_id)) = 0 THEN FALSE
					ELSE TRUE
				END
		FROM {{ ref('dwh_payment') }}
		WHERE date_updated > (SELECT MAX(date_updated) FROM {{ this }})
	{%- endcall %}
	{%- set ids_to_update = load_result('get_ids_to_update')['data'][0][0] -%}
	{%- set need_update = load_result('get_ids_to_update')['data'][0][1] -%}
{% endif %}
--TODO: sum of balance changes per id from transaction, balance before/after payment
SELECT	cre.payment_id,
		cre.player_id,
		cre.business_unit_id,
		buh.franchise_name,
		cre.session_id,
		session.device_type,
		finalUpd.provider_id,
		p_provider.name AS provider_name,
		finalUpd.sub_provider,
		cre.method_type,
		ptr.payment_init_location,
		finalUpd.payment_account_id,
		cre.external_payment_account_id,
		--finalUpd.amount,
		--cre.amount  AS initial_amount,
		--cre.currency,
		--finalUpd.fee,
		finalUpd.transaction_amount,
		cre.transaction_amount AS initial_transaction_amount,
		finalUpd.transaction_fee,
		cre.transaction_currency, 
		er.exchange_rate,
		finalUpd.transaction_amount * er.exchange_rate AS transaction_amount_EUR,
		cre.transaction_amount * er.exchange_rate AS initial_transaction_amount_EUR,
		finalUpd.transaction_fee * er.exchange_rate AS transaction_fee_EUR,
		finalUpd.base_amount,
		cre.base_currency,
		finalUpd.desired_currency,
		cre.desired_amount AS initial_desired_amount,
		finalUpd.desired_amount,
		cre.desired_amount * erde.exchange_rate AS initial_desired_amount_EUR,
		finalUpd.desired_amount * erde.exchange_rate AS desired_amount_EUR,
		finalUpd.operational_currency,
		cre.operational_amount AS initial_operational_amount,
		finalUpd.operational_amount,
		cre.operational_amount * erop.exchange_rate AS initial_operational_amount_EUR,
		finalUpd.operational_amount * erop.exchange_rate AS operational_amount_EUR,
		cre.payment_type,
		COALESCE(finalUpd.payment_status, 'CREATED') AS payment_status,
		finalUpd.external_reference,
		-- taking latest values for ch_id & ch_name instead of create event ones. JIRA BT-39
		finalUpd.payment_channel_id,
		finalUpd.payment_channel_name,
		cre.date_created,
		finalUpd.date_updated,
		cre.reason,
		cre.adjust_balance,
		finalUpd.last_reason,
		--COALESCE(upd.lastPaymentStatus, final.finalStatus,'CREATED') AS lastPaymentStatus, -- use that field for NDC
		--COALESCE(final.finalStatus, 'CREATED') AS finalPaymentStatus, -- value here differs from lastPaymentStatus only if payment status is changed after CAPTURED or CANCELLED
																		-- in that case there is status what is added after payment is CAPTURED
		wa.date_approved, 
		wa.approved_by, 
		wa.approved_by_username, 
		wa.approved_reason,
		captured.date_captured,
		cre.bonus_id
FROM (
	SELECT  payment_id,
			player_id, 
			business_unit_id, 
			external_payment_account_id,
			method_type,
			payment_channel_id,
			payment_channel_name,
			amount,
			currency, 
			transaction_amount,
			transaction_currency,
			transaction_fee,
			Payment_type, 
			date_updated AS date_created,
			base_currency, 
			reason, 
			session_id, 
			adjust_balance,
			bonus_id,
			desired_currency,
			operational_currency,
			desired_amount,
			operational_amount
    FROM  {{ ref('dwh_payment') }}
    WHERE `type` = 'CREATED'
	{% if is_incremental() and need_update %}
		AND payment_id IN UNNEST({{ ids_to_update }})
	{% elif is_incremental() and not need_update %}
		AND {{ need_update }}
	{% endif %}
	) AS cre
LEFT JOIN (
 -- Approved BY --take values from last row where status i APPROVED
			SELECT	payment_id, 
					date_updated AS date_approved, 
					updated_by AS approved_by, 
					updated_by_username AS approved_by_username, 
					reason AS approved_reason, 
					ROW_NUMBER() OVER (PARTITION BY payment_id ORDER BY date_updated DESC) AS row_no 
				FROM {{ ref('dwh_payment') }}
			WHERE	payment_status = 'APPROVED'
			{% if is_incremental() and need_update %}
				AND payment_id IN UNNEST({{ ids_to_update }})
			{% elif is_incremental() and not need_update %}
				AND {{ need_update }}
			{% endif %}
			) AS wa	
ON	cre.payment_id = wa.payment_id 
	AND	wa.row_no = 1
	LEFT JOIN (
			SELECT	payment_id, 
					date_updated AS date_captured,   
					ROW_NUMBER() OVER (PARTITION BY payment_id ORDER BY date_updated desc) AS row_no
				FROM {{ ref('dwh_payment') }}
			WHERE	payment_status = 'CAPTURED'
			{% if is_incremental() and need_update %}
				AND payment_id IN UNNEST({{ ids_to_update }})
			{% elif is_incremental() and not need_update %}
				AND {{ need_update }}
			{% endif %}
			) AS captured	
	ON cre.payment_id = captured.payment_id 
		AND captured.row_no = 1
		LEFT JOIN (
					SELECT	DISTINCT payment_id,
							LAST_VALUE(amount IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS amount,
							LAST_VALUE(fee IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS fee,
							LAST_VALUE(base_amount IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS base_amount,
							LAST_VALUE(transaction_amount IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS transaction_amount,
							LAST_VALUE(transaction_fee IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS transaction_fee,
							LAST_VALUE(payment_status IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_status,
							LAST_VALUE(provider_id IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS provider_id,
							LAST_VALUE(date_updated IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_updated,
							LAST_VALUE(external_reference IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS external_reference,
							LAST_VALUE(reason IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS last_reason,
							LAST_VALUE(sub_provider IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS sub_provider,
							LAST_VALUE(payment_account_id IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_account_id,
							LAST_VALUE(payment_channel_id IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_channel_id,
							LAST_VALUE(payment_channel_name IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_channel_name,
							LAST_VALUE(desired_amount IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS desired_amount,
							LAST_VALUE(operational_amount IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS operational_amount,
							LAST_VALUE(desired_currency IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS desired_currency,
							LAST_VALUE(operational_currency IGNORE NULLS) OVER (PARTITION BY payment_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS operational_currency
					FROM {{ ref('dwh_payment') }}
					{% if is_incremental() and need_update %}
					WHERE payment_id IN UNNEST({{ ids_to_update }})
					{% elif is_incremental() and not need_update %}
					WHERE {{ need_update }}
					{% endif %}
				) AS finalUpd 
		ON finalUpd.payment_id = cre.payment_id 
			LEFT JOIN {{ref('dwh_exchange_rate')}} AS er 
			ON cre.transaction_currency = er.base_currency 
				AND COALESCE(date_captured,date_updated) >= er.start_date 
					AND COALESCE(date_captured,date_updated) < er.end_date 
						AND er.Currency = 'EUR'
				LEFT JOIN {{ ref('dwh_exchange_rate') }} AS erde
				ON cre.desired_currency = erde.base_currency 
					AND COALESCE(captured.date_captured, finalUpd.date_updated) >= erde.start_date 
						AND COALESCE(captured.date_captured, finalUpd.date_updated) < erde.end_date 
							AND erde.Currency = 'EUR'
					LEFT JOIN {{ ref('dwh_exchange_rate') }} AS erop
					ON cre.operational_currency = erop.base_currency 
						AND COALESCE(captured.date_captured, finalUpd.date_updated) >= erop.start_date 
							AND COALESCE(captured.date_captured, finalUpd.date_updated) <  erop.end_date 
								AND erop.Currency = 'EUR'
						LEFT JOIN {{ ref('d_payment_provider') }} AS p_provider 
						ON finalUpd.provider_id = p_provider.payment_provider_id
							LEFT JOIN {{ ref('d_business_unit_hierarchy') }} AS buh 
							ON cre.business_unit_id = buh.franchise_id 
								AND buh.most_recent
								LEFT JOIN {{ ref('f_session') }} AS session
								ON cre.session_id = session.session_id 
							LEFT JOIN (
								--response field is serialized (string), so we need to deserialize it to json object and picj appropriate fields from there
								SELECT	JSON_EXTRACT_SCALAR(response, '$.paymentInitLocation') AS payment_init_location,  
										payment_id
								FROM {{ ref('dwh_payment_trace') }}
								WHERE type = 'PAYMENT_REQUESTED' 
									AND status = 'SUCCESSFUL' --payment_init_location is currently available for succesfull requested payments with payment type = DEPOSIT
										AND JSON_EXTRACT_SCALAR(response, '$.type') = 'DEPOSIT'
											AND JSON_EXTRACT_SCALAR(response, '$.paymentInitLocation') IS NOT NULL
							) AS ptr --payment_trace
							ON ptr.payment_id = cre.payment_id
