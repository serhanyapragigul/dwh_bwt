{{ config(schema='dm', materialized='view')}}


            select     
            distinct 
            payment_provider_id,
            business_unit_id,
            franchise_name,
            LAST_VALUE (name IGNORE NULLS ) OVER (PARTITION BY payment_provider_id ORDER by date_created asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as name,
            LAST_VALUE (api_url IGNORE NULLS ) OVER (PARTITION BY payment_provider_id ORDER by date_created asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as api_url, 
            FIRST_VALUE (date_created IGNORE NULLS ) OVER (PARTITION BY payment_provider_id ORDER by date_created asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as date_created,
            FIRST_VALUE (provider_type IGNORE NULLS ) OVER (PARTITION BY payment_provider_id ORDER by date_created asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as provider_type
            
            from  {{ ref('dwh_payment_provider') }} 
		
LEFT JOIN
{{ ref('d_business_unit_hierarchy') }}
on business_unit_id = franchise_id
and most_recent is TRUE

        