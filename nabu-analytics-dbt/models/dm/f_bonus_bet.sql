{{
	config(
		schema='dm',
		materialized='incremental',
		unique_key = 'bonus_bet_id',
		cluster_by = ['date_final_status','bonus_id', 'bet_id']
		)
}}
-- depends_on: {{ ref('dwh_changed_bonus_id') }}
-- Known Errors
-- bet amount dupllicated while multiple won rows
SELECT  CAST(FARM_FINGERPRINT(CONCAT(bl.bonus_id, bl.bet_id, MIN(bl.date_created))) AS string) AS bonus_bet_id,
        bl.bonus_id	,	
        b.bonus_configuration_id,
        bl.bet_id,	
        MAX(bl.last_wager_type) as last_wager_type,
        bet.player_id,
        bet.business_unit_id,
        buh.franchise_name,	
        bet.game_id, 
        bet.provider_type,	
        bet.currency_code,
        bet.sb_game_phase,
        er.exchange_rate,
    -- BET 
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.bet_amount ELSE 0 END) AS bet_amount_bet,	
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.bonus_amount_taken ELSE 0 END) AS bonus_amount_taken_bet,	
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.locked_amount_taken ELSE 0 END) AS locked_amount_taken_bet,	
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.transaction_amount_correction ELSE 0 END) AS transaction_amount_correction_bet,	
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.withdrawable_amount_taken ELSE 0 END) AS withdrawable_amount_taken_bet,		
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.wagered ELSE 0 END) AS wagered_bet,	
    -- BET in EUR
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.bet_amount * er.exchange_rate ELSE 0 END) AS bet_amount_bet_eur,	
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.bonus_amount_taken * er.exchange_rate ELSE 0 END) AS bonus_amount_taken_bet_eur,	
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.locked_amount_taken * er.exchange_rate ELSE 0 END)	as locked_amount_taken_bet_eur,	
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.transaction_amount_correction * er.exchange_rate ELSE 0 END) AS transaction_amount_correction_bet_eur,	
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.withdrawable_amount_taken * er.exchange_rate ELSE 0 END) AS withdrawable_amount_taken_bet_eur,		
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.wagered * er.exchange_rate ELSE 0 END) AS wagered_bet_eur,	
    --WIN 
        SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.bet_amount ELSE 0 END) AS bet_amount_win,	
        SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.bonus_amount_taken ELSE 0 END) AS bonus_amount_taken_win,	
        SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.locked_amount_taken ELSE 0 END) AS locked_amount_taken_win,	
        SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.transaction_amount_correction ELSE 0 END) AS transaction_amount_correction_win,	
        SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.withdrawable_amount_taken ELSE 0 END) AS withdrawable_amount_taken_win,		
    --WIN in EUR
        SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.bet_amount * er.exchange_rate ELSE 0 END) AS bet_amount_win_eur,	
        SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.bonus_amount_taken * er.exchange_rate ELSE 0 END) AS bonus_amount_taken_win_eur,	
        SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.locked_amount_taken * er.exchange_rate ELSE 0 END)	as locked_amount_taken_win_eur,	
        SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.transaction_amount_correction * er.exchange_rate ELSE 0 END) AS transaction_amount_correction_win_eur,	
        SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.withdrawable_amount_taken * er.exchange_rate ELSE 0 END) AS withdrawable_amount_taken_win_eur,	
    --CANCEL BET 
        SUM(CASE WHEN bl.wager_type = 'CANCEL_BET' THEN bl.bonus_amount_taken ELSE 0 END) AS bonus_amount_taken_cancel_bet,		
        SUM(CASE WHEN bl.wager_type = 'CANCEL_BET' THEN bl.wagered ELSE 0 END) AS wagered_cancel_bet,	
    --CANCEL BET in EUR
        SUM(CASE WHEN bl.wager_type = 'CANCEL_BET' THEN bl.bonus_amount_taken * er.exchange_rate ELSE 0 END) AS bonus_amount_taken_cancel_bet_eur,		
        SUM(CASE WHEN bl.wager_type = 'CANCEL_BET' THEN bl.wagered * er.exchange_rate ELSE 0 END) AS wagered_cancel_bet_eur,	
    --CANCEL WIN
        SUM(CASE WHEN bl.wager_type = 'CANCEL_WIN' THEN bl.bonus_amount_taken ELSE 0 END) AS bonus_amount_taken_cancel_win,	
        SUM(CASE WHEN bl.wager_type = 'CANCEL_WIN' THEN bl.transaction_amount_correction ELSE 0 END) AS transaction_amount_correction_cancel_win,	
    --CANCEL WIN in EUR
        SUM(CASE WHEN bl.wager_type = 'CANCEL_WIN' THEN bl.bonus_amount_taken * er.exchange_rate ELSE 0 END) AS bonus_amount_taken_cancel_win_eur,	
        SUM(CASE WHEN bl.wager_type = 'CANCEL_WIN' THEN bl.transaction_amount_correction * er.exchange_rate ELSE 0 END) AS transaction_amount_correction_cancel_win_eur,	
        MIN(bl.status) AS status,--CLOSED is before OPEN, not 100% can be changed to LAST, can a status be re-opened
        MIN(bl.date_created) AS date_created,
        MAX(bl.date_created) AS date_final,
        MAX(wagered_weight) AS wagered_weight, -- Max to work in GROUP BY
        b.final_status,
        b.is_bonus_lost,
        b.date_final_status,
        b.is_final_bonus_status,
        b.bonus_type,
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.bonus_amount_taken ELSE 0 END) --bonus_amount_taken_bet
            - SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.bonus_amount_taken ELSE 0 END) --bonus_amount_taken_win
            - SUM(CASE WHEN bl.wager_type = 'CANCEL_BET' THEN bl.bonus_amount_taken ELSE 0 END) --bonus_amount_taken_cancel_bet 
            + SUM(CASE WHEN bl.wager_type = 'CANCEL_WIN' THEN bl.bonus_amount_taken ELSE 0 END) --bonus_amount_taken_cancel_win 
        AS bonus_game_win,
        SUM(CASE WHEN bl.wager_type = 'BET' THEN bl.bonus_amount_taken * er.exchange_rate ELSE 0 END) --bonus_amount_taken_bet_eur
            - SUM(CASE WHEN bl.wager_type = 'WIN' THEN bl.bonus_amount_taken * er.exchange_rate ELSE 0 END) --bonus_amount_taken_win_eur 
            - SUM(CASE WHEN bl.wager_type = 'CANCEL_BET' THEN bl.bonus_amount_taken * er.exchange_rate ELSE 0 END) --bonus_amount_taken_cancel_bet_eur 
            + SUM(CASE WHEN bl.wager_type = 'CANCEL_WIN' THEN bl.bonus_amount_taken * er.exchange_rate ELSE 0 END) --bonus_amount_taken_cancel_win_eur 
        AS bonus_game_win_eur,
        MAX(b.wagered_weight) * SUM(CASE WHEN bl.wager_type = 'BET' 
                                            THEN bl.wagered 
                                         WHEN bl.wager_type = 'CANCEL_BET' 
                                            THEN - bl.wagered 
                                         ELSE 0 END)  AS final_winnings_allocation,  -- validate --wagered_weight * wagered_bet
        MAX(b.wagered_weight) * SUM(CASE WHEN bl.wager_type = 'BET' 
                                            THEN bl.wagered 
                                         WHEN bl.wager_type = 'CANCEL_BET' 
                                            THEN - bl.wagered 
                                         ELSE 0 
                                    END * er.exchange_rate)  AS final_winnings_allocation_eur  -- validate --wagered_weight * wagered_bet_eur
FROM (
    SELECT *, 
           LAST_VALUE(wager_type) OVER (PARTITION BY bet_id ORDER BY date_created ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_wager_type
      FROM  {{ ref('dwh_bonus_log') }} 

    ) AS bl
{% if is_incremental() %}
INNER JOIN {{ ref('dwh_changed_bonus_id') }} AS inc
    ON inc.bonus_id = bl.bonus_id 
{% endif %}
LEFT JOIN {{ ref('f_bet') }} AS bet 
    ON bl.bet_id = bet.bet_id --maybe should read dwh for cleaner flow, but needs data in f_bet
LEFT JOIN {{ref('dwh_exchange_rate')}} AS er 
    ON bet.currency_code = er.base_currency 
        AND bl.date_created >= er.start_date 
            AND bl.date_created < er.end_date 
                AND er.Currency = 'EUR'
LEFT JOIN {{ ref('d_business_unit_hierarchy') }} AS buh 
    ON bet.business_unit_id = buh.franchise_id 
        AND buh.most_recent 
LEFT JOIN  ( -- join f_bonus to get progress amount and wagered
    SELECT  bns.bonus_id, 
            bns.bonus_configuration_id,
            bns.bonus_type,
            bns.final_status,
            bns.is_final_bonus_status,
            bns.date_final_status,
            bns.is_bonus_lost,
            SUM(CASE WHEN bns.final_status = 'COMPLETED' AND bns.progress_amount > 0 THEN bns.progress_amount ELSE NULL END) AS progress_amount,  -- This should always be one row, but to make sure we use a sum (for now), could be a MAX or other if needed
            SUM(CASE WHEN bns.final_status = 'COMPLETED' AND bns.progress_amount > 0 THEN SAFE_DIVIDE(bns.progress_amount, bns.wagered) ELSE NULL END) AS wagered_weight   -- This should always be one row, but to make sure we use a sum (for now), could be a MAX or other if needed        
      FROM {{ ref('f_bonus')}} AS bns
      {% if is_incremental() %}
      INNER JOIN {{ ref('dwh_changed_bonus_id') }} AS inc
        ON inc.bonus_id = bns.bonus_id
      {% endif %}
    GROUP BY    
        bns.bonus_id, 
        bns.bonus_configuration_id, 
        bns.bonus_type, 
        bns.final_status, 
        bns.date_final_status,
        bns.is_final_bonus_status,
        bns.is_bonus_lost
                            ) AS b 
                ON bl.bonus_id = b.bonus_id
WHERE bl.bonus_id IS NOT NULL
    AND bl.status != 'FAILED'
GROUP BY    bet.business_unit_id,
            buh.franchise_name,	
            bet.player_id,
            bet.game_id, 
            bet.provider_type,
            bet.currency_code,
            bet.sb_game_phase,
            er.exchange_rate,
            b.final_status,
            b.date_final_status,
            b.is_bonus_lost,
            bl.bet_id,	
            bl.bonus_id,	
            b.bonus_configuration_id,
            b.bonus_type,
            b.is_final_bonus_status

