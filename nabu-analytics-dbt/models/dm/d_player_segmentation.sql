{{ config(schema='dm')}}

WITH deposit AS 
    (
    SELECT player_id, 
           method_type, 
           SUM(CASE WHEN payment_status = 'CAPTURED' THEN 1 ELSE 0 END) AS captured_deposit_count,
           SUM(CASE WHEN payment_status = 'CAPTURED' THEN transaction_amount_EUR ELSE 0 END) AS captured_deposit_amount,
           COUNT(payment_id) AS total_trial 
      FROM {{ref('f_payment')}}
     WHERE payment_type = 'DEPOSIT'
       AND DATE(date_created) BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 30 DAY) 
       AND CURRENT_DATE()
     GROUP 
        BY player_id,
           method_type
    ),
weekly_activity as # weekly acceptance rate and total trial
    (
    SELECT player_id, 
           method_type, 
           SUM(CASE WHEN payment_status = 'CAPTURED' THEN 1 ELSE 0 END) AS captured_deposit_count,
           COUNT(payment_id) AS total_trial,
           SUM(CASE WHEN payment_status = 'CAPTURED' THEN 1 ELSE 0 END) / COUNT(payment_id) AS acceptance_rate,
      FROM {{ref('f_payment')}}
     WHERE payment_type = 'DEPOSIT'
       AND DATE(date_created) BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 7 DAY) AND CURRENT_DATE()
     GROUP 
        BY player_id,
           method_type
    HAVING total_trial > 0
    ),
weekly_acceptance_rate_distribution AS # more than six trials took into account to reflect the accurate acceptance_rate distribution of payment methods
    (
    SELECT player_id, 
           method_type, 
           COUNT(payment_id) AS total_trial,
           SUM(CASE WHEN payment_status = 'CAPTURED' THEN 1 ELSE 0 END) / COUNT(payment_id) AS acceptance_rate,
           NTILE(4) OVER (PARTITION BY method_type 
                          ORDER BY (SUM(CASE WHEN payment_status = 'CAPTURED' THEN 1 ELSE  0 END)/ COUNT(payment_id))
                         ) AS ntile_acceptance_rate, ---- acceptance rate quartiles
      FROM {{ref('f_payment')}}
     WHERE payment_type = 'DEPOSIT'
       AND DATE(date_created) BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 7 DAY) AND CURRENT_DATE()
     GROUP 
        BY player_id,
           method_type
    HAVING total_trial > 6
    ),
weekly_trial_distribution AS # more than two trials took into account to reflect the accurate trial distribution of payment methods
    (
    SELECT player_id, 
           method_type,
           COUNT(payment_id) AS total_trial,
           NTILE(4) OVER (PARTITION BY method_type 
                          ORDER BY COUNT(payment_id) 
                         ) AS ntile_trial ---- total trial quartiles
      FROM {{ref('f_payment')}}
     WHERE payment_type = 'DEPOSIT'
       AND DATE(date_created) BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 7 DAY) and CURRENT_DATE()
     GROUP 
        BY player_id,
           method_type
    HAVING total_trial > 2
    ),
most_deposited_methods AS ---last 7 days, for offering generic method
    (
    SELECT method_type, 
           SUM(CASE WHEN payment_status = 'CAPTURED' THEN transaction_amount_EUR ELSE 0 END) AS captured_deposit_amount,
           ROW_NUMBER() OVER (ORDER BY SUM(CASE WHEN payment_status = 'CAPTURED' THEN transaction_amount_EUR ELSE 0 END) DESC) AS deposit_rank
      FROM {{ref('f_payment')}}
     WHERE method_type NOT IN ('CRYPTO_CONVERSION') 
       AND DATE(date_created) BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 7 DAY) AND CURRENT_DATE()
     GROUP
        BY method_type  
    ),
total_trial AS -- trial distribution by payment method + threshold 
    (
    SELECT method_type,
           MAX(CASE WHEN ntile_trial = 3 THEN total_trial END) trial_q3,
           MAX(CASE WHEN ntile_trial = 2 THEN total_trial END) trial_q2,
           MAX(CASE WHEN ntile_trial = 1 THEN total_trial END) trial_q1 
      FROM weekly_trial_distribution
     GROUP 
        BY method_type
    ),
total_deposit as
    (
    SELECT player_id, 
           SUM(captured_deposit_count) AS captured_deposit_count_total,
           SUM(captured_deposit_amount) AS captured_deposit_amount_total
      FROM deposit
     GROUP 
        BY player_id
    HAVING captured_deposit_count_total > 0
    ),
preferences AS
    (
    SELECT d.player_id, 
           d.method_type,
           d.captured_deposit_amount,
           td.captured_deposit_amount_total,
           d.captured_deposit_count,
           td.captured_deposit_count_total,
           d.total_trial,
           ROW_NUMBER() OVER (PARTITION BY d.player_id ORDER BY d.captured_deposit_amount DESC) AS rn,
           d.captured_deposit_count / td.captured_deposit_count_total AS count_volume, -- deposit count percentage to all
           d.captured_deposit_amount / td.captured_deposit_amount_total AS amount_volume, -- deposit amount percentage to all
      FROM deposit AS d     
      JOIN total_deposit AS td 
        ON td.player_id = d.player_id
    ),
quartiles AS  -- acceptance rate distribution by payment method 
    (
    SELECT method_type,
           MAX(CASE WHEN ntile_acceptance_rate = 3 THEN acceptance_rate END) AS acceptance_rate_q3,
           MAX(CASE WHEN ntile_acceptance_rate = 2 THEN acceptance_rate END) AS acceptance_rate_q2,
           MAX(CASE WHEN ntile_acceptance_rate = 1 THEN acceptance_rate END) AS acceptance_rate_q1,
           AVG(acceptance_rate) AS average_acceptance_rate
      FROM weekly_acceptance_rate_distribution
     GROUP 
        BY method_type
    ),
main_table AS
    ( 
    SELECT DISTINCT 
           player_id,
           first_method_type,
           first_method_score,
           second_method_type,
           second_method_score,
           CASE WHEN second_method_score >= 4 
                    THEN second_method_type
                WHEN second_method_score > 1 
                 AND second_method_score < 4 
                 AND second_method_acceptance_rate > (CASE WHEN second_method_type NOT IN ('CRYPTO_CONVERSION','PAYKWIK','ZIMPLER') 
                                                              THEN COALESCE(acceptance_rate_q1, 0.15) 
                                                           ELSE COALESCE(average_acceptance_rate, 0.5) END) 
                    THEN second_method_type
                WHEN second_method_score > 1 
                 AND second_method_score < 4  
                 AND second_method_acceptance_rate <= (CASE WHEN second_method_type NOT IN ('CRYPTO_CONVERSION','PAYKWIK','ZIMPLER') 
                                                               THEN COALESCE(acceptance_rate_q1, 0.15) 
                                                            ELSE COALESCE(average_acceptance_rate, 0.5) 
                                                       END)  
                 AND second_method_total_trial <= trial_q1 
                    THEN second_method_type
                WHEN second_method_score > 1 
                 AND second_method_score < 4  
                 AND second_method_acceptance_rate <= (CASE WHEN second_method_type NOT IN ('CRYPTO_CONVERSION','PAYKWIK','ZIMPLER') 
                                                               THEN COALESCE(acceptance_rate_q1, 0.15) 
                                                            ELSE COALESCE(average_acceptance_rate, 0.5) 
                                                       END) 
                 AND second_method_total_trial > trial_q1 
                    THEN 'generic_method'
                WHEN second_method_total_trial IS NULL OR second_method_score <= 1  
                    THEN 'generic_method'
           END AS recommendation 
      FROM
        (
        SELECT player_id,
               first_method_type,
               (first_method_amount_score * 0.6) + (first_method_count_score * 0.4) AS first_method_score,
               second_method_type,
               second_method_total_trial,
               (second_method_amount_score * 0.6) + (second_method_count_score * 0.4) AS second_method_score,
               second_method_acceptance_rate,
               acceptance_rate_q1 AS acceptance_rate_q1,
               average_acceptance_rate,
               trial_q1,
          FROM
             (
             SELECT p1.player_id,
                    p1.method_type AS first_method_type,
                    waf.acceptance_rate AS first_method_acceptance_rate,
                    p1.count_volume AS first_method_count_vol,
                    p1.amount_volume AS first_method_amount_vol,
                    CASE WHEN p1.amount_volume <= 0.4 THEN 0
                         WHEN p1.amount_volume > 0.4 and p1.amount_volume <= 0.5 THEN 1
                         WHEN p1.amount_volume > 0.5 and p1.amount_volume <= 0.6 THEN 2
                         WHEN p1.amount_volume > 0.6 and p1.amount_volume <= 0.7 THEN 3
                         WHEN p1.amount_volume > 0.7 and p1.amount_volume <= 0.8 THEN 4
                         WHEN p1.amount_volume > 0.8 THEN 5 
                    END AS first_method_amount_score,
                    CASE 
                         WHEN p1.count_volume <= 0.4 THEN 0
                         WHEN p1.count_volume > 0.4 and p1.count_volume <= 0.5 THEN 1
                         WHEN p1.count_volume > 0.5 and p1.count_volume <= 0.6 THEN 2
                         WHEN p1.count_volume > 0.6 and p1.count_volume <= 0.7 THEN 3
                         WHEN p1.count_volume > 0.7 and p1.count_volume <= 0.8 THEN 4
                         WHEN p1.count_volume > 0.8 THEN 5 
                    END AS first_method_count_score,
                    p2.method_type AS second_method_type,
                    p2.amount_volume AS second_method_amount_vol,
                    p2.count_volume AS second_method_count_vol,
                    was.total_trial AS second_method_total_trial,
                    CASE 
                        WHEN p2.amount_volume <= 0.05 THEN 0
                        WHEN p2.amount_volume > 0.05 and p2.amount_volume <= 0.1 THEN 1
                        WHEN p2.amount_volume > 0.1 and p2.amount_volume <= 0.15 THEN 2
                        WHEN p2.amount_volume > 0.15 and p2.amount_volume <= 0.20 THEN 3
                        WHEN p2.amount_volume > 0.2 and p2.amount_volume <= 0.4 THEN 4
                        WHEN p2.amount_volume > 0.4 THEN 5 
                    END AS second_method_amount_score,
                    CASE 
                        WHEN p2.count_volume <= 0.05 THEN 0
                        WHEN p2.count_volume > 0.05 and p2.count_volume <= 0.1 THEN 1
                        WHEN p2.count_volume > 0.1 and p2.count_volume <= 0.15 THEN 2
                        WHEN p2.count_volume > 0.15 and p2.count_volume <= 0.20 THEN 3
                        WHEN p2.count_volume > 0.2 and p2.count_volume <= 0.4 THEN 4
                        WHEN p2.count_volume > 0.4 THEN 5 
                    END AS second_method_count_score,
                    tt.trial_q1,
                    tt.trial_q2,
                    tt.trial_q3,
                    was.acceptance_rate AS second_method_acceptance_rate,
                    q.acceptance_rate_q3,
                    q.acceptance_rate_q2,
                    q.acceptance_rate_q1,
                    q.average_acceptance_rate
               FROM (SELECT * FROM preferences WHERE rn = 1 ) p1 # first preference
               LEFT  
               JOIN (SELECT * FROM preferences WHERE rn = 2) AS p2 
                 ON p2.player_id = p1.player_id # second preference table
               LEFT 
               JOIN quartiles AS q 
                 ON q.method_type = p2.method_type # second acceptance rate quartiles
               LEFT 
               JOIN weekly_activity AS waf 
                 ON waf.player_id = p1.player_id 
                AND waf.method_type = p1.method_type # first preference weekly acceptance rate
               LEFT 
               JOIN weekly_activity AS was 
                 ON was.player_id = p2.player_id 
                AND was.method_type = p2.method_type # second preference weekly acceptance rate
               LEFT 
               JOIN total_trial AS tt 
                 ON tt.method_type = p2.method_type # total trial threshold
              WHERE p2.count_volume > 0
             )
        )
    ),
payment_segmentation AS
(
SELECT player_id,
       first_method_type,
       first_method_score,
       second_method_type,
       second_method_score,
       recommendation,
       CASE WHEN recommendation = 'generic_method' AND first_method_type = first_popular.method_type THEN second_popular.method_type
            WHEN recommendation = 'generic_method' AND first_method_type <> first_popular.method_type THEN first_popular.method_type 
            WHEN recommendation <> 'generic_method' THEN NULL
            ELSE NULL 
       END AS first_generic_method_offer,
       CASE WHEN recommendation = 'generic_method' AND first_method_type IN (first_popular.method_type,second_popular.method_type) THEN third_popular.method_type
            WHEN recommendation = 'generic_method' AND first_method_type IN (third_popular.method_type,forth_popular.method_type) THEN second_popular.method_type
            WHEN recommendation = 'generic_method' AND first_method_type NOT IN (first_popular.method_type, second_popular.method_type,third_popular.method_type,forth_popular.method_type) THEN second_popular.method_type
            WHEN recommendation <> 'generic_method' then NULL
            ELSE NULL 
       END AS second_generic_method_offer,
       CASE WHEN recommendation = 'generic_method' AND first_method_type IN (first_popular.method_type,second_popular.method_type,third_popular.method_type) THEN forth_popular.method_type
           WHEN recommendation = 'generic_method' AND first_method_type = forth_popular.method_type THEN third_popular.method_type
           WHEN recommendation = 'generic_method' AND first_method_type NOT IN (first_popular.method_type, second_popular.method_type,third_popular.method_type,forth_popular.method_type) THEN third_popular.method_type
           WHEN recommendation <> 'generic_method' THEN NULL
           ELSE NULL 
       END AS third_generic_method_offer
  from
      ( 
      SELECT player_id,
             first_method_type,
             first_method_score,
             second_method_type,
             second_method_score,
             recommendation
        FROM main_table
           
       UNION ALL --- joining customers with only one and more than one payment method
       
      SELECT  --- customers with only one payment method directed to the generic methods
             p.player_id,
             p.method_type AS first_method_type,
             5 AS first_method_score,
             NULL AS second_method_type,
             NULL AS second_method_score,
            'generic_method' AS recommendation
        FROM preferences AS p 
        JOIN (
             SELECT player_id, COUNT (DISTINCT method_type) count_method_type 
               FROM deposit 
              GROUP BY player_id 
             HAVING count_method_type = 1 
             ) mt 
          ON mt.player_id = p.player_id
       )
       CROSS JOIN (SELECT method_type FROM most_deposited_methods WHERE deposit_rank = 1) AS first_popular
       CROSS JOIN (SELECT method_type FROM most_deposited_methods WHERE deposit_rank = 2) AS second_popular
       CROSS JOIN (SELECT method_type FROM most_deposited_methods WHERE deposit_rank = 3) AS third_popular
       CROSS JOIN (SELECT method_type FROM most_deposited_methods WHERE deposit_rank = 4) AS forth_popular 
)
SELECT p.player_id, 
       p.business_unit_id, 
       ps.first_method_type AS payment_first_method_type, 
       ps.first_method_score AS payment_first_method_score, 
       ps.second_method_type AS payment_second_method_type, 
       ps.second_method_score AS payment_second_method_score, 
       ps.recommendation AS payment_recommendation,
       ps.first_generic_method_offer as payment_first_generic_method_offer,
       ps.second_generic_method_offer as payment_second_generic_method_offer,
       ps.third_generic_method_offer as payment_third_generic_method_offer,
       ch.evaluation_day AS churn_last_evaluation_day,
       ch.inactive_days AS churn_inactive_days,
       ch.churn_segment, 
       ch.churn_segment_previous, 
       ss.most_staked_sport,
       ss.most_staked_league,
       ss.most_staked_market,
       ss.most_staked_team,
       ss.odd_preference AS sb_sub_odd_preference,
       ss.stake_preference AS sb_sub_stake_preference,
       ss.bet_type AS sb_sub_bet_type,
       ss.played_sport_variety AS sb_sub_played_sport_variety,
       ss.execution_date AS sb_sub_execution_date,
       ss.execution_time AS sb_sub_execution_time
  FROM {{ref('d_player')}} p 
  LEFT 
  JOIN payment_segmentation ps 
    ON p.player_id = ps.player_id 
  LEFT 
  JOIN (
        SELECT player_id,
               franchise_name,
               evaluation_day, 
               inactive_days,
               churn_segment, 
               churn_segment_previous, 
               RANK() OVER (ORDER BY evaluation_day DESC) AS rn 
          FROM {{ref('dwh_player_churn_segment')}}
         WHERE 1 =1 
       QUALIFY rn = 1
       ) ch
    ON p.player_id = ch.player_id
   AND p.franchise_name = ch.franchise_name
  LEFT 
  JOIN (
       SELECT distinct
              player_id,
              business_unit_id,
              most_staked_sport,
              most_staked_league,
              most_staked_market,
              most_staked_team,
              odd_preference,
              stake_preference,
              bet_type,
              played_sport_variety,
              execution_date,
              execution_time,
              RANK() OVER (ORDER BY execution_time DESC) AS rn 
         FROM {{ref('dwh_player_sb_sub_segment')}}
        WHERE 1=1 
      QUALIFY rn = 1
       ) ss
    ON p.player_id = ss.player_id 
   AND p.business_unit_id = ss.business_unit_id