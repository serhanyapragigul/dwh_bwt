{{
    config(
        schema='dm',
        materialized='incremental',
        unique_key = 'transaction_id',
        cluster_by = ['transaction_date','transaction_id'])
}}

-- depends_on: {{ ref('f_bet') }}
-- depends_on: {{ ref('f_bonus_bet') }}
-- depends_on: {{ ref('f_adjustment') }}
-- depends_on: {{ ref('d_player') }}
-- depends_on: {{ ref('dwh_player_registration') }}
-- depends on: {{ ref('dwh_min_bet_date') }}
-- depends_on: {{ ref('dwh_changed_bet_id') }}
-- depends_on: {{ ref('f_ndc') }}

{% set models =  {'f_bet':'bet', 'f_payment':'payment', 'f_bonus_bet':'bonus', 'f_adjustment':'adjustment', 'd_player':'nrc', 'f_ndc':'ndc', 'f_bonus':'bonus'} %}

SELECT  uda.player_id,
        uda.game_id,
		uda.business_unit_id,
		uda.franchise_name,
        uda.date_created,
		uda.transaction_date,
		uda.provider_type,
		uda.payment_sub_provider,
		uda.payment_method_type,
        uda.payment_account_id,
        uda.transaction_currency,
        uda.exchange_rate AS transaction_exchange_rate,
		uda.turnover_eur,
		uda.winnings_eur,
		uda.jackpot_payout_eur,
		uda.jackpot_contribution_eur,
		uda.gamewin_eur,
		uda.free_spin_bc_eur,
		uda.free_bet_bc_eur,
		uda.risk_free_bet_bc_eur,
		uda.price_boost_bc_eur,
		uda.adjustment_bonus_eur,
        uda.is_final_bonus_status,
		uda.bonus_game_win_eur,
        uda.final_winnings_allocation_eur,
        uda.general_bonus_cost_eur,
        uda.given_bonus_amt_eur,
        (
        	coalesce(uda.free_spin_bc_eur, 0) +
            coalesce(uda.free_bet_bc_eur, 0) +
            coalesce(uda.risk_free_bet_bc_eur, 0) +
            coalesce(uda.price_boost_bc_eur, 0) +
            coalesce(uda.adjustment_bonus_eur, 0) +
            coalesce(uda.bonus_game_win_eur, 0) +
            coalesce(uda.final_winnings_allocation_eur, 0)
        ) as total_bonus_cost_eur,
		uda.transaction_id,
        uda.deposit_initiated_amt_eur,
        uda.deposit_initiated_cnt,
        uda.deposit_captured_amt_eur,
        uda.deposit_captured_cnt,
        uda.withdrawal_initiated_amt_eur,
        uda.withdrawal_initiated_cnt,
        uda.withdrawal_captured_amt_eur,
        uda.withdrawal_captured_cnt,
        uda.deposit_transaction_fee_eur,
        uda.withdrawal_transaction_fee_eur,
		uda.adjustment_amt_eur,
		uda.adjustment_accounting_revenue_amt_eur,
        uda.bonus_amt_eur,
        uda.is_bonus_lost,
        uda.record_type,
        uda.transaction_type,
        uda.product,
		uda.main_classification_name,
		uda.sb_game_phase,
        uda.sb_bet_type,
		uda.final_status,
		uda.status_after_capture,
		uda.status_after_capture_same_day,
		uda.external_reference,
        uda.odds_value,
        plr.registration_date,
	    ndc.ndc_date,
        apl.player_id IS NOT NULL as active_player,
	    plr.test_account,
        (
            coalesce(uda.gamewin_eur, 0) 
            - (
        	coalesce(uda.free_spin_bc_eur, 0) +
            coalesce(uda.free_bet_bc_eur, 0) +
            coalesce(uda.risk_free_bet_bc_eur, 0) +
            coalesce(uda.price_boost_bc_eur, 0) +
            coalesce(uda.adjustment_bonus_eur, 0) + -- discount payout adjustments
            coalesce(uda.bonus_game_win_eur, 0) +
            coalesce(uda.final_winnings_allocation_eur, 0)
            )
    	    + coalesce(uda.adjustment_accounting_revenue_amt_eur, 0) --other revenue impact adjustments
        ) as accounting_revenue_eur,
        aff.affiliate_general_id,
        aff.channel AS acquisition_channel,
        plr.reference as player_netrefer_id,
        plr.email, 
        uda.source_table
FROM (
{% for model, label in models.items() %}
    SELECT	{{ label }}.player_id,
            {% if model in ['f_bet', 'f_bonus_bet'] -%} {{ label }}.game_id {% else -%} '-1' {%- endif %} as game_id,
            {{ label }}.business_unit_id,
            {{ label }}.franchise_name,
            {%- if model == 'd_player' -%} 
                {{ label }}.registration_date
            {%- elif model == 'f_ndc' -%} 
                {{ label }}.ndc_date    
            {% else -%} {{ label }}.date_created {%- endif %} as date_created,
            {%- if model == 'f_bet' -%}
                {{ label }}.final_status_date 
            {%- elif model == 'f_payment' -%}
                {{ label }}.date_updated 
            {%- elif model in ['f_bonus_bet', 'f_bonus'] -%}
                {{ label }}.date_final_status 
            {%- elif model == 'f_adjustment' -%}
                coalesce({{ label }}.date_updated, {{ label }}.date_created) 
            {%- elif model == 'd_player' -%}
                {{ label }}.registration_date
            {%- elif model == 'f_ndc' -%}
                {{ label }}.ndc_date
            {%- endif %} as transaction_date,
            {% if model == 'f_bet' -%} 
                {{ label }}.provider_type
            {%- elif model in ['f_payment', 'f_ndc'] -%}
                {{ label }}.provider_name
            {%- elif model == 'd_player' -%}
                NULL
            {% else -%} upper('{{ label }}') {%- endif %} as provider_type,
            {% if model in ['f_payment', 'f_ndc'] -%} {{ label }}.sub_provider {% else -%} NULL {%- endif %} as payment_sub_provider,
            {% if model in ['f_payment', 'f_ndc'] -%} {{ label }}.method_type {% else -%} NULL {%- endif %} as payment_method_type,
            {% if model in ['f_payment', 'f_ndc'] -%} {{ label }}.payment_account_id {% else -%} NULL {%- endif %} as payment_account_id,
            {%- if model in ['f_bet', 'f_bonus_bet'] -%}
                    {{ label }}.currency_code 
                {%- elif model in ['f_payment', 'f_adjustment'] -%}
                    {{ label }}.transaction_currency 
                {%- elif model == 'f_bonus' -%}
                    {{ label }}.currency
            {%- else -%}
                NULL
            {%- endif %} as transaction_currency,
            {% if model != 'd_player' %} {{ label }}.exchange_rate {% else -%} NULL {%- endif %} as exchange_rate,
            {% if model == 'f_bet' -%} {{ label }}.total_bet_amount_eur {% else -%} 0.0 {%- endif %} as turnover_eur,
            {% if model == 'f_bet' -%} {{ label }}.total_win_amount_eur {% else -%} 0.0 {%- endif %} as winnings_eur,
            {% if model == 'f_bet' -%} {{ label }}.jackpot_payout_eur {% else -%} 0.0 {%- endif %} as jackpot_payout_eur,
            {% if model == 'f_bet' -%} {{ label }}.jackpot_contribution_eur {% else -%} 0.0 {%- endif %} as jackpot_contribution_eur,
            {% if model == 'f_bet' -%} {{ label }}.total_bet_amount_eur - {{ label }}.total_win_amount_eur 
                - {{ label }}.jackpot_contribution_eur + {{ label }}.jackpot_payout_eur {% else -%} 0.0 {%- endif %} as gamewin_eur,
            {% if model == 'f_bet' -%} {{ label }}.free_spin_bonus_cost_eur {% else -%} 0.0 {%- endif %} as free_spin_bc_eur,
            {% if model == 'f_bet' -%} {{ label }}.free_bet_bonus_cost_eur {% else -%} 0.0 {%- endif %} as free_bet_bc_eur,
            {% if model == 'f_bet' -%} {{ label }}.risk_free_bet_bonus_cost_eur {% else -%} 0.0 {%- endif %} as risk_free_bet_bc_eur,
            {% if model == 'f_bet' -%} {{ label }}.price_boost_bonus_cost_eur {% else -%} 0.0 {%- endif %} as price_boost_bc_eur,
            {% if model == 'f_bet' -%} coup.sb_bet_type {% else -%} null {%- endif %} as sb_bet_type,
            
            {% if model == 'f_adjustment' -%}
            -- only account for discount payouts in bonus if they are categorised as revenue non-product adjustments by Finance
            CASE 
                WHEN {{ label }}.revenue_non_product_adjustment THEN {{ label }}.base_amount_eur
                ELSE 0.0 
            END {% else -%} 0.0 {% endif -%} as adjustment_bonus_eur,
            {% if model == 'f_bonus_bet' -%}
            {{ label }}.bonus_game_win_eur
            {% else -%} 0.0 {%- endif %} as bonus_game_win_eur,
            {% if model == 'f_bonus_bet' -%}
            {{ label }}.final_winnings_allocation_eur
            {% else -%} 0.0 {%- endif %} as final_winnings_allocation_eur,
            {% if model == 'f_bonus' -%}
            CASE WHEN is_active_bonus_status THEN  {{ label }}.amount_eur ELSE 0.0 END 
            {% else -%} 0.0 {%- endif %} as general_bonus_cost_eur,
            {% if model == 'f_bonus' -%} {{ label }}.amount_eur {% else -%} 0.0 {%- endif %} as bonus_amt_eur,
            {% if model == 'f_bonus' -%}
            CASE WHEN claim_timestamp IS NOT NULL THEN  {{ label }}.amount_eur ELSE 0.0 END 
            {% else -%} 0.0 {%- endif %} as given_bonus_amt_eur,
            {% if model == 'd_player' -%} 
                reg.player_registration_id
            {% elif model == 'f_bonus_bet' -%} 
                {{ label }}.bonus_bet_id
            {%- else -%} {{ label }}.{{ label }}_id {%- endif %} as transaction_id,
            {% if model == 'f_payment' -%}
            CASE
				WHEN {{ label }}.payment_type = 'DEPOSIT' THEN {{ label }}.transaction_amount_EUR 
				ELSE 0 
			END {% else -%} 0.0 {% endif -%} as deposit_initiated_amt_eur,
            {% if model == 'f_payment' -%}
			CASE
				WHEN {{ label }}.payment_type = 'DEPOSIT' THEN 1 
				ELSE 0 
			END {% else -%} 0 {% endif -%} as deposit_initiated_cnt,
            {% if model == 'f_payment' -%}
			CASE 
				WHEN {{ label }}.payment_type = 'DEPOSIT' 
					AND {{ label }}.payment_status = 'CAPTURED' THEN {{ label }}.transaction_amount_EUR 
				ELSE 0 
			END {% else -%} 0.0 {% endif -%} as deposit_captured_amt_eur,
            {% if model == 'f_payment' -%}
			CASE 
				WHEN {{ label }}.payment_type = 'DEPOSIT'
					AND {{ label }}.payment_status = 'CAPTURED' THEN 1 
				ELSE 0 
			END {% else -%} 0 {% endif -%} as deposit_captured_cnt,
            {% if model == 'f_payment' -%}
			CASE 
				WHEN {{ label }}.payment_type = 'WITHDRAWAL' THEN {{ label }}.transaction_amount_EUR 
				ELSE 0 
			END {% else -%} 0.0 {% endif -%} as withdrawal_initiated_amt_eur,
            {% if model == 'f_payment' -%}
			CASE 
				WHEN {{ label }}.payment_type = 'WITHDRAWAL' THEN 1 
				ELSE 0 
			END {% else -%} 0 {% endif -%} as withdrawal_initiated_cnt,
            {% if model == 'f_payment' -%}
			CASE 
				WHEN {{ label }}.payment_type = 'WITHDRAWAL' 
					AND {{ label }}.payment_status = 'CAPTURED' THEN {{ label }}.transaction_amount_EUR 
				ELSE 0 
			END {% else -%} 0.0 {% endif -%} as withdrawal_captured_amt_eur,
            {% if model == 'f_payment' -%}
			CASE 
				WHEN {{ label }}.payment_type = 'WITHDRAWAL' 
					AND {{ label }}.payment_status = 'CAPTURED' THEN 1 
				ELSE 0 
			END {% else -%} 0 {% endif -%} as withdrawal_captured_cnt,
            {% if model == 'f_payment' -%}
			CASE 
				WHEN {{ label }}.payment_type = 'DEPOSIT' 
					AND {{ label }}.payment_status = 'CAPTURED' THEN {{ label }}.transaction_fee_EUR 
				ELSE 0 
			END {% else -%} 0.0 {% endif -%} as deposit_transaction_fee_eur,
            {% if model == 'f_payment' -%}
			CASE 
				WHEN {{ label }}.payment_type = 'WITHDRAWAL' 
					AND {{ label }}.payment_status='CAPTURED' THEN {{ label }}.transaction_fee_EUR 
				ELSE 0 
			END {% else -%} 0.0 {% endif -%} as withdrawal_transaction_fee_eur,
            {% if model == 'f_adjustment' -%}
             -- only deduct adjustments from revenue if they are categorised as revenue product adjustments by Finance -- 
            CASE 
                WHEN {{ label }}.revenue_product_adjustment THEN {{ label }}.allocation_strategy * {{ label }}.base_amount_eur 
                ELSE 0.0 
            END {% else -%} 0.0 {% endif -%} as adjustment_accounting_revenue_amt_eur,
            {% if model == 'f_adjustment' -%}
            -- everything but Discount Payout should go to Adjustments
            CASE 
                WHEN {{ label }}.revenue_non_product_adjustment THEN 0.0 
                ELSE coalesce({{ label }}.allocation_strategy * {{ label }}.base_amount_eur, 0.0) 
            END  {% else -%} 0.0 {% endif -%} as adjustment_amt_eur,
            {% if model in ['f_bonus', 'f_bonus_bet'] -%} {{ label }}.is_final_bonus_status {% else -%} NULL {%- endif %} as is_final_bonus_status,
            upper('{{ label }}') as record_type,
            {% if model == 'f_bet' -%}
                upper('{{ label }}')
            {%- elif model in ['f_bonus_bet', 'f_bonus'] -%} 
                {{ label }}.bonus_type
            {%- elif model == 'f_payment' -%}
                {{ label }}.payment_type
            {%- elif model == 'f_adjustment' -%}
                {{ label }}.adjustment_type_name
            {%- elif model in ['d_player', 'f_ndc'] -%}
                upper('{{ label }}')
            {% else -%} NULL {%- endif %} as transaction_type,
            {% if model in ['f_bet', 'f_bonus_bet'] -%}
            CASE 
                WHEN {{ label }}.provider_type = 'SPORTSBOOK' THEN 'SPORTSBOOK' 
                ELSE 'GOC' 
            END
            {% else -%} 'NON PRODUCT SPECIFIC' {%- endif %} as product,
            {% if model in ['f_bet', 'f_bonus_bet'] -%}
            CASE 
                WHEN {{ label }}.provider_Type = 'SPORTSBOOK' THEN 'SPORTSBOOK' 
                ELSE coalesce(game.main_classification_name, 'Casino') 
            END {% else -%} NULL {%- endif %} as main_classification_name,
            {% if model in ['f_bet', 'f_bonus_bet'] -%} {{ label }}.sb_game_phase {% else -%} NULL {%- endif %} as sb_game_phase,
            {% if model == 'f_bet' -%} coup.odds_value {% else -%} NULL {%- endif %} as odds_value,
            {% if model in ['f_bonus', 'f_bonus_bet'] -%} {{ label }}.is_bonus_lost {% else -%} NULL {%- endif %} AS is_bonus_lost, 
            {{ label }}.
            {%- if model in ['f_bet', 'f_bonus_bet', 'f_bonus'] -%}
                final_status 
            {%- elif model == 'f_payment' -%}
                payment_status 
            {%- elif model == 'f_adjustment' -%}
                end_status 
            {%- elif model in ['d_player', 'f_ndc'] -%}
                kyc_status 
            {%- endif %} as final_status,
            {% if model == 'f_payment' -%} coalesce({{ label }}.date_updated > {{ label }}.date_captured, false) {% else -%} NULL {%- endif %} as status_after_capture,
            {% if model == 'f_payment' -%} coalesce({{ label }}.date_updated > {{ label }}.date_captured, false) 
                AND (cast({{ label }}.date_captured as date) = cast({{ label }}.date_updated as date)) {% else -%} NULL {%- endif %} as status_after_capture_same_day,
            {% if model == 'f_adjustment' -%}
            coalesce(   regexp_extract(comment, '([0-9]{10})'), 
                        regexp_extract(comment, '([0-9]{7})'), 
                        regexp_extract(regexp_replace(comment, '([^\\w\\s-])', ''), 
                        '([a-f0-9]{8}-[a-f0-9]{4}-[0-5][a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12})')) {% else -%} NULL {% endif -%} as external_reference,
            upper('{{model}}') as source_table
    FROM {{ ref(model) }} as {{ label }}
    {% if model == 'f_bet' -%}
        LEFT JOIN {{ ref('d_game') }} as game
            ON game.game_id = {{ label }}.game_id
        LEFT JOIN 
            (
            SELECT DISTINCT
                   bet_id, 
                   MAX(combinedodds_value) OVER (PARTITION BY bet_id ORDER BY date_coupon_placed ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS odds_value,
                   LAST_VALUE(bet_type) OVER (PARTITION BY bet_id ORDER BY date_coupon_placed ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS sb_bet_type
              FROM {{ ref('f_sb_coupon')}} 
            
            ) AS coup
            ON {{label}}.sb_bet_id = coup.bet_id
        {% if is_incremental() %}
	        --  because we do not know when the update will occur looking back in time fixed amount of days/hours alone does not help
	        --  we get the latest point in time we need to use from dwh_min_bet_date table
	        WHERE {{ label }}.final_status_date > (SELECT min_date FROM {{ ref('dwh_min_bet_date') }} )
	        AND {{ label }}.bet_id IN (SELECT bet_id FROM {{ ref('dwh_changed_bet_id') }} )
	    {% endif %}
    {% elif model == 'd_player' -%}
        LEFT JOIN {{ ref('dwh_player_registration') }} AS reg 
	        ON {{ label }}.player_id = reg.player_id
        {% if is_incremental() %}
            -- only load new records from d_player where updated_date is gt the maximum nrc date in current materialization of the target table
            -- trunc timestamps to consider entire day to eliminate cases when player's activity appears later than first event of another type
            WHERE TIMESTAMP_TRUNC({{ label }}.date_updated, DAY) >= (SELECT MAX(TIMESTAMP_TRUNC(transaction_date, DAY)) FROM {{ this }} WHERE record_type = upper('{{ label }}'))
        {% endif -%}
    {% elif model == 'f_bonus_bet' -%}  
    LEFT JOIN {{ ref('d_game') }} as game
    ON game.game_id = {{ label }}.game_id
        WHERE {{ label }}.is_final_bonus_status
        {% if is_incremental() %}
            -- only load new records from f_bonus_bet where date_final_status is gt the maximum date in current materialization of the target table
            -- trunc timestamps to consider entire day to eliminate cases when player's activity appears later than first event of another type
            AND TIMESTAMP_TRUNC({{ label }}.date_final_status, DAY) >= (SELECT MAX(TIMESTAMP_TRUNC(transaction_date, DAY)) FROM {{ this }} WHERE record_type = upper('{{ label }}'))
        {% endif -%}
    {% elif is_incremental() %}
        -- only load new records from f_payment or f_adjustment where the relevant timestamp is gt the maximum date in current materialization of the target table
        -- trunc timestamps to consider entire day to eliminate cases when player's activity appears later than first event of another type
        {% if model == 'f_payment' -%}
            WHERE TIMESTAMP_TRUNC({{ label }}.date_updated, DAY)  >= (SELECT MAX(TIMESTAMP_TRUNC(transaction_date, DAY)) FROM {{ this }} WHERE record_type = upper('{{ label }}'))
        {% elif model == 'f_bonus' -%}
            WHERE TIMESTAMP_TRUNC({{ label }}.date_final_status, DAY) >= (SELECT MAX(TIMESTAMP_TRUNC(transaction_date, DAY)) FROM {{ this }} WHERE record_type = upper('{{ label }}'))
        {% elif model == 'f_adjustment' -%}
            WHERE TIMESTAMP_TRUNC(coalesce({{ label }}.date_updated, {{ label }}.date_created), DAY)  >= (SELECT MAX(TIMESTAMP_TRUNC(transaction_date, DAY)) FROM {{ this }} WHERE record_type = upper('{{ label }}'))
        {% elif model == 'f_ndc' -%}
            WHERE TIMESTAMP_TRUNC({{ label }}.ndc_date, DAY) >= (SELECT MAX(TIMESTAMP_TRUNC(transaction_date, DAY)) FROM {{ this }} WHERE record_type = 'NDC')
        {% endif -%}
    {% endif -%}

    {% if not loop.last %} 
        UNION ALL 
    {%- endif %}
{% endfor %}
) as uda --union data
    LEFT JOIN {{ ref('d_player') }} plr
        ON plr.player_id = uda.player_id and plr.business_unit_id = uda.business_unit_id
        LEFT JOIN (
            SELECT  pmt.player_id, pmt.business_unit_id,
                    MIN(CASE 
                            WHEN pmt.payment_type = 'DEPOSIT' and pmt.date_captured IS NOT NULL THEN pmt.date_captured 
                            ELSE NULL 
                        END) AS ndc_date
                FROM {{ ref('f_payment') }} pmt
            GROUP BY pmt.player_id, pmt.business_unit_id
        ) AS ndc
            ON ndc.player_id = plr.player_id and ndc.business_unit_id = plr.business_unit_id
            LEFT JOIN {{ ref('d_affiliate_channel') }} AS aff 
   		        ON plr.affiliate_id = aff.affiliate_general_id
                LEFT JOIN {{ ref('f_active_player') }} AS apl
                    ON apl.player_id = uda.player_id
                        AND apl.business_unit_id = uda.business_unit_id
                            AND apl.activity_date = timestamp_trunc(uda.transaction_date, day)
--temporary patch for players with multiple business unit ids and the same player id
WHERE uda.transaction_id IS NOT NULL