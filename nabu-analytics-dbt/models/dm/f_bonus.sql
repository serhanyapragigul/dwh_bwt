{{
	config(
		schema='dm',
		materialized='incremental',
		unique_key = 'bonus_id',
		cluster_by = ['date_updated','bonus_id']
		)
}}

SELECT   fact.bonus_id,	
         fact.player_id,
         fact.business_unit_id,
         buh.franchise_name,
         fact.bonus_configuration_id,
         fact.bonus_type,
         fact.claim_timestamp,
         fact.claim_deadline,
         fact.lifetime_deadline,
         fact.amount,
         fact.amount * er.exchange_rate AS amount_EUR,
         fact.locked_amount,
         fact.locked_amount * er.exchange_rate AS locked_amount_EUR,
         fact.progress_amount,
         fact.progress_amount * er.exchange_rate AS progress_amount_EUR,
         fact.locked_progress_amount,
         fact.locked_progress_amount * exchange_rate AS locked_progress_amount_EUR,
         fact.wagered,
         fact.currency,
         er.exchange_rate,
         fact.min_lockup_amount,
         fact.min_lockup_amount * er.exchange_rate AS min_lockup_amount_EUR,
         fact.max_lockup_amount,
         fact.max_lockup_amount * er.exchange_rate AS max_lockup_amount_EUR,
         fact.end_balance,
         fact.end_balance * er.exchange_rate AS end_balance_EUR,
         fact.bonus_lifetime_seconds,
         fact.payment_id,
         fact.percent_bonus_given,
         fact.bonus_money_first,
         fact.date_updated,
         fact.date_created,
         fact.updated_by,
         fact.updated_by_username,
         fact.comment,
         fact.budget_code,
         CASE WHEN fact.progress_amount <= fact.end_balance AND fact.final_status = 'COMPLETED' THEN TRUE ELSE FALSE END AS is_bonus_lost,
         fact.external_reference,
         fact.pre_saved,
         fact.date_delivered,
         fact.date_first_claimed,
         fact.date_last_claimed,
         COALESCE(sm.dwh_bonus_status, 'N/A') AS final_status,
         sm.is_final_bonus_status, 
         sm.is_active_bonus_status,
         fact.date_final_status
FROM (
   SELECT   DISTINCT bonus_id,	
            player_id,
            business_unit_id,
            bonus_configuration_id,
            bonus_type,
            claim_timestamp,
            claim_deadline,
            lifetime_deadline,
            amount,
            locked_amount,
            progress_amount,
            locked_progress_amount,
            wagered,
            currency,
            min_lockup_amount,
            max_lockup_amount,
            end_balance,
            bonus_lifetime_seconds,
            payment_id,
            percent_bonus_given,
            bonus_money_first,
            date_updated,
            date_created,
            updated_by,
            updated_by_username,
            comment,
            budget_code,
            external_reference,
            pre_saved,
            date_delivered,
            date_first_claimed,
            date_last_claimed,
            final_status,
            FIRST_VALUE(date_updated_final_status IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated_final_status ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_final_status,--calculate First transaction date of the final status   
   FROM(  
         SELECT   DISTINCT bonus_id, 
                  LAST_VALUE(player_id IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS player_id,
                  LAST_VALUE(business_unit_id IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS business_unit_id,
                  LAST_VALUE(bonus_configuration_id IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS bonus_configuration_id,
                  LAST_VALUE(bonus_type IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS bonus_type,
                  LAST_VALUE(claim_timestamp IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS claim_timestamp,
                  LAST_VALUE(claim_deadline IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS claim_deadline,
                  LAST_VALUE(lifetime_deadline IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS lifetime_deadline,
                  LAST_VALUE(amount IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS amount,
                  LAST_VALUE(locked_amount IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS locked_amount,
                  LAST_VALUE(progress_amount IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS progress_amount,
                  LAST_VALUE(locked_progress_amount IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS locked_progress_amount,
                  LAST_VALUE(wagered IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS wagered,
                  LAST_VALUE(currency IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS currency,
                  LAST_VALUE(min_lockup_amount IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS min_lockup_amount,		
                  LAST_VALUE(max_lockup_amount IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS max_lockup_amount,	
                  LAST_VALUE(end_balance IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS end_balance,	
                  LAST_VALUE(bonus_lifetime_seconds IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS bonus_lifetime_seconds,	
                  LAST_VALUE(payment_id IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_id,	
                  LAST_VALUE(percent_bonus_given IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS percent_bonus_given,	
                  LAST_VALUE(bonus_money_first IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS bonus_money_first,	
                  LAST_VALUE(date_updated IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_updated,	
                  FIRST_VALUE(date_updated IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_created,	
                  LAST_VALUE(updated_by IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS updated_by,	
                  LAST_VALUE(updated_by_username IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS updated_by_username,	
                  LAST_VALUE(comment IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS comment,	
                  LAST_VALUE(budget_code IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS budget_code,	
                  LAST_VALUE(external_reference IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS external_reference,	
                  LAST_VALUE(pre_saved IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS pre_saved,	
                  LAST_VALUE(CASE WHEN status = 'DELIVERED' THEN date_updated ELSE NULL END IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_delivered,
                  FIRST_VALUE(CASE WHEN status ='CLAIMED' THEN date_updated ELSE NULL END IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_first_claimed,       
                  LAST_VALUE(CASE WHEN status = 'CLAIMED' THEN date_updated ELSE NULL END IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_last_claimed,   
                  LAST_VALUE(status IGNORE NULLS) OVER (PARTITION BY bonus_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS final_status,
                  status,
                  date_updated AS date_updated_final_status
		   FROM {{ ref('dwh_bonus') }}
         {% if is_incremental() %}
	      WHERE bonus_id IN (
				SELECT DISTINCT bonus_id
				FROM {{ ref('dwh_bonus') }}
				WHERE date_updated > (SELECT MAX(date_updated) FROM {{ this }})
			)
	    {% endif %}
   )
   WHERE final_status = status	  
) AS fact
   LEFT JOIN {{ref('dwh_exchange_rate')}} AS er 
      ON fact.currency = er.base_currency 
         AND date_final_status >= er.start_date 
            AND date_final_status < er.end_date 
               AND er.Currency = 'EUR'
   LEFT JOIN {{ ref('d_business_unit_hierarchy') }} AS buh 
      ON business_unit_id = buh.franchise_id 
         AND buh.most_recent is TRUE
   LEFT JOIN {{ref('dwh_bonus_status_mapping')}} AS sm
      ON fact.final_status = sm.platform_bonus_status
--this BU is filtered out because it is an operator rather than a franchise
--these bonuses were created as test ones and shouldn't be counted anywhere
WHERE fact.business_unit_id != 'b15b25bb-ce24-4a4e-ac42-786f3081bd08'


