{{
	config(
		schema='dm',
		materialized='table',
		cluster_by = 'player_id'
		)
}}

SELECT	session_entity_id,
		SPLIT(MAX(sessions_list)) AS sessions_list,
		player_id,
		business_unit_id,
		franchise_name,
		created,
		SPLIT(MAX(devices_list)) AS devices_list,
		SPLIT(MAX(platform_list)) AS platform_list,
		expiration,
		session_entity_duration_seconds,
		CAST(ROUND(AVG(session_entity_duration_seconds) OVER (PARTITION BY player_id), 0) AS INT64) AS average_session_entity_duration_seconds,
		CAST(COALESCE(round(DATE_DIFF(created, LAG(expiration) OVER (PARTITION BY player_id ORDER BY created), SECOND), 2),0) AS INT64) AS seconds_since_previous_session_entity
FROM (
	SELECT	se.session_entity_id,
			se.sessions_list,
			se.player_id,
			se.business_unit_id,
			bu.franchise_name,
			se.devices_list,
			se.platform_list,
			se.created,
		-- Replace the session entity expiration with the lowest of either the expiration or the last player activity (bet or payment)
			LEAST(COALESCE(LAST_VALUE(activity.created IGNORE NULLS) OVER (PARTITION BY se.session_entity_id ORDER BY activity.created ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING), se.expiration), se.expiration) AS expiration,
			CAST(CEILING(
				TIMESTAMP_DIFF(
					LEAST(COALESCE(LAST_VALUE(activity.created IGNORE NULLS) OVER(PARTITION BY se.session_entity_id ORDER BY activity.created ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING), se.expiration), se.expiration),
					se.created,	SECOND)
			)AS INT64) AS session_entity_duration_seconds
	FROM (
		SELECT -- Generate a unique ID seeded on player_id, business_unit_id, and the session_entity number
				FARM_FINGERPRINT(CONCAT(player_id, business_unit_id, session_entity)) AS session_entity_id,
				session_entities.player_id,
				session_entities.business_unit_id,
				-- set created and expiration for the session entity based on all children sessions
				MIN(session_entities.created) AS created,
				MAX(session_entities.expiration) AS expiration,
				STRING_AGG(session_id) AS sessions_list,
				STRING_AGG(DISTINCT device_type) AS devices_list,
				STRING_AGG(DISTINCT platform) AS platform_list
		FROM (
			SELECT new_sessions.*,
					-- session entity is the running sum of non-contiguous sessions commenced by the user (or where the user has changed device/platform
					SUM(CAST(new_session AS INT64)) OVER (PARTITION BY player_id, business_unit_id ORDER BY created ASC) AS session_entity
			FROM (
				SELECT	sessions.*,
						-- checks if previous session by user is contiguous (overlaps the start of the next session by +/- 1 minute and that the device / platform remained the same), otherwise it treats it as a new session entity
						COALESCE(
							NOT(
								LAG(expiration) OVER (PARTITION BY player_id, business_unit_id ORDER BY created ASC) BETWEEN TIMESTAMP_SUB(created, INTERVAL 1 MINUTE) AND TIMESTAMP_ADD(created, INTERVAL 1 MINUTE)
								--AND LAG(CONCAT(device_type, platform, channel)) OVER (PARTITION BY player_id, business_unit_id ORDER BY created ASC) = CONCAT(device_type, platform, channel)
							), TRUE
						) AS new_session
				FROM (
					SELECT	s.session_id, 
							s.player_id, 
							s.business_unit_id, 
							s.device_type,
							s.platform, 
							CASE WHEN s.device_type IN ('Desktop', 'Unknown') THEN'DESKTOP' ELSE 'MOBILE' END AS channel, 
							s.date_created AS created,
							-- ends chronoogically overlapping sessions with the start time time of the next session
							COALESCE(LEAST(MAX(s.date_expiration) OVER (PARTITION BY session_id), LEAD(s.date_created) OVER (PARTITION BY s.player_id, s.business_unit_id ORDER BY s.date_created)), MAX(s.date_expiration) OVER (PARTITION BY session_id)) AS expiration
					  FROM nabuminds.dwh.dwh_session AS s
					WHERE s.player_id IS NOT NULL
						--AND player_id = '0002e394-c2d8-4acd-a685-5166ed50a131' -- for demonstration purposes only
				) AS sessions
			) AS new_sessions
		) AS session_entities
		GROUP BY player_id, 
				 business_unit_id, 
				 session_entity
	) AS se
-- add franchise name
	LEFT JOIN (SELECT franchise_name, franchise_id FROM {{ ref('d_business_unit_hierarchy') }} WHERE most_recent) AS bu
		ON se.business_unit_id = bu.franchise_id
	-- uses either a bet creation or payment creation for checking player interaction during the session
	LEFT JOIN (
		SELECT b.player_id, b.business_unit_id, b.date_created AS created FROM {{ ref('f_bet') }} AS b
		UNION ALL
		SELECT p.player_id, p.business_unit_id, p.date_created AS created FROM {{ ref('f_payment') }} AS p
		) AS activity
		ON activity.player_id = se.player_id
		AND activity.business_unit_id = se.business_unit_id
		AND activity.created BETWEEN se.created AND se.expiration	
)
GROUP BY session_entity_id,
		 player_id,
		 business_unit_id,
		 franchise_name,
		 created,
		 expiration,
		 session_entity_duration_seconds

/* TODO: need to research incremental option

{% if is_incremental() %}
				-- this filter will only be applied on an incremental run
				where expiration > TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
  {% endif %}

*/