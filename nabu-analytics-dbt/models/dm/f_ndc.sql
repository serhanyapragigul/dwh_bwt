{{
config(
    schema='dm',
    materialized='incremental',
    cluster_by=['ndc_date', 'ndc_id'],
    )
}}
WITH main AS (
    SELECT  DISTINCT cast(FARM_FINGERPRINT(CONCAT(pmt.player_id, plr.business_unit_id)) as string) as ndc_id,
            pmt.player_id,
            plr.business_unit_id,
            plr.franchise_name,
            plr.kyc_status,
            FIRST_VALUE(pmt.date_captured IGNORE NULLS) OVER (PARTITION BY pmt.player_id ORDER BY pmt.date_captured ASC 
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS ndc_date,
            FIRST_VALUE(pmt.payment_id IGNORE NULLS) OVER (PARTITION BY pmt.player_id ORDER BY pmt.date_captured ASC 
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS ndc_payment_id,
            FIRST_VALUE(pmt.provider_name IGNORE NULLS) OVER (PARTITION BY pmt.player_id ORDER BY pmt.date_captured ASC 
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS provider_name,
            FIRST_VALUE(pmt.sub_provider IGNORE NULLS) OVER (PARTITION BY pmt.player_id ORDER BY pmt.date_captured ASC 
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS sub_provider,
            FIRST_VALUE(pmt.method_type IGNORE NULLS) OVER (PARTITION BY pmt.player_id ORDER BY pmt.date_captured ASC 
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS method_type,
            FIRST_VALUE(pmt.payment_account_id IGNORE NULLS) OVER (PARTITION BY pmt.player_id ORDER BY pmt.date_captured ASC 
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_account_id,
            FIRST_VALUE(pmt.transaction_amount_EUR IGNORE NULLS) OVER (PARTITION BY pmt.player_id ORDER BY pmt.date_captured ASC 
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS ndc_amount_eur,
            FIRST_VALUE(pmt.transaction_amount IGNORE NULLS) OVER (PARTITION BY pmt.player_id ORDER BY pmt.date_captured ASC 
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS ndc_amount_lcy,
            FIRST_VALUE(pmt.transaction_currency IGNORE NULLS) OVER (PARTITION BY pmt.player_id ORDER BY pmt.date_captured ASC 
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS ndc_local_currency_code,
            plr.currency_code as player_currency_code
    FROM {{ ref('f_payment') }} AS pmt
        LEFT JOIN {{ ref('d_player') }} AS plr
            ON pmt.player_id = plr.player_id
    WHERE pmt.date_captured IS NOT NULL 
        AND pmt.payment_type = 'DEPOSIT'
        -- only load new records from subquery where updated_date is gt the maximum ndc date in current materialization of the target table
        -- and a player doesn't already have ndc in the current table
    {% if is_incremental() %}
        AND pmt.payment_id IN (
            SELECT payment_id
            FROM {{ ref('f_payment') }}
            WHERE date_updated > (SELECT MAX(ndc_date) FROM {{ this }})
                AND player_id NOT IN (SELECT DISTINCT player_id from {{ this }})
                    AND date_captured IS NOT NULL 
                        AND payment_type = 'DEPOSIT'
        )
    {% endif %}
)
SELECT  main.ndc_id,
        main.player_id,
        main.business_unit_id,
        main.franchise_name,
        main.kyc_status,
        main.ndc_date,
        main.ndc_payment_id,
        main.provider_name,
        main.sub_provider,
        main.method_type,
        main.payment_account_id,
        main.ndc_amount_eur,
        main.ndc_amount_lcy,
        main.ndc_local_currency_code,
        er.exchange_rate,
        main.ndc_amount_eur / er.exchange_rate AS ndc_amount_player_currency,
        main.player_currency_code
FROM main
    LEFT JOIN {{ ref('dwh_exchange_rate') }} AS er
        ON main.player_currency_code = er.base_currency
            AND main.ndc_date >= er.start_date
                AND main.ndc_date < er.end_date
WHERE er.currency = 'EUR'