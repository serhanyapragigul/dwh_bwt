{{
	config(
		schema='dm',
		materialized='incremental',
		unique_key = 'game_session_id',
		cluster_by = ['game_session_start']
		)
}}

--filtering logic here should maybe be moved to dwh_game_session

  SELECT
    cre.game_session_id,
    cre.business_unit_id,
	buh.franchise_name,
    COALESCE(cre.game_id,game.game_id) as game_id,
    cre.internal_game_id,
    player_id,
    cre.session_id,
    cre.date_updated as game_session_start,
    upd.game_session_end,
	DATETIME_DIFF(DATETIME (upd.game_session_end), DATETIME (cre.date_updated), MINUTE) AS game_session_duration

FROM (
		SELECT 
			game_session_id,
			business_unit_id,
			game_id,
			internal_game_id,
			player_id,
			date_updated,
			session_id
    	FROM  {{ ref('dwh_game_session') }}  
		WHERE type='CREATED'
		{% if is_incremental() %}
		-- 	let's look back 3 days in order to be sure we do not miss any row. 3 is not
		--	the result of sophisticated calculations and can be changed if needed
		AND date_updated > TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
		{% endif %}
		) cre

LEFT JOIN
	--select game_session which is most recently updated 
	(
		SELECT
			game_session_id,
			MAX(date_updated) AS game_session_end
		FROM {{ ref('dwh_game_session') }}  
		WHERE type='UPDATED' and status='INACTIVE'
		{% if is_incremental() %}
		-- 	let's look back 3 days in order to be sure we do not miss any row. 3 is not
		--	the result of sophisticated calculations and can be changed if needed
		AND date_updated > TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
		{% endif %}
		GROUP BY game_session_id
	) upd
ON cre.game_session_id=upd.game_session_id
LEFT JOIN {{ ref('d_game') }} game
ON  game.internal_game_id = cre.internal_game_id AND game.internal_game_id = 'SPORTSBOOK'
LEFT JOIN {{ ref('d_business_unit_hierarchy') }} buh
ON cre.business_unit_id = buh.franchise_id AND buh.most_recent IS TRUE



