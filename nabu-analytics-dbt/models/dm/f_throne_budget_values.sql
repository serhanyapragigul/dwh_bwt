{{ config( schema='dm', materialized='table')}}

WITH pivot_values AS (
SELECT 
    tbv.*,
    ds.reporting_date,
    CASE
        WHEN REGEXP_CONTAINS(unit, r'%') AND ds.reporting_date = DATE_TRUNC(ds.reporting_date, MONTH) THEN tbv.budget_amount * 100
        WHEN REGEXP_CONTAINS(unit, r'%') THEN 0
        ELSE ROUND(tbv.budget_amount / ds.pro_rata_days, 5)
    END AS daily_budget_amount,
    CASE
        WHEN REGEXP_CONTAINS(unit, r'%') THEN tbv.budget_amount * 100
        ELSE SUM(ROUND(tbv.budget_amount / ds.pro_rata_days, 5)) OVER(PARTITION BY DATE_TRUNC(ds.reporting_date, MONTH), tbv.franchise, tbv.product, tbv.metric_name  ORDER BY ds.reporting_date ASC )
    END AS cumulative_budget_amount
FROM {{ref('dwh_throne_budget_values')}} AS tbv
LEFT JOIN (
    SELECT
        reporting_date,
        COUNT(reporting_date) OVER (PARTITION BY DATE_TRUNC(reporting_date, MONTH)) AS pro_rata_days
    FROM UNNEST(
    GENERATE_DATE_ARRAY(
        (SELECT MIN(reporting_month) FROM {{ref('dwh_throne_budget_values')}}),
        (SELECT DATE_ADD(MAX(reporting_month), INTERVAL 1 MONTH) FROM {{ref('dwh_throne_budget_values')}}),
        INTERVAL 1 DAY)
    ) AS reporting_date
) AS ds
ON tbv.reporting_month = DATE_TRUNC(ds.reporting_date, MONTH)
)

		SELECT
            franchise,
            product,
            budget_year,
            budget_month,
            reporting_month,
            metric_name,
            unit,
            budget_amount,
            reporting_date,
            daily_budget_amount,
            cumulative_budget_amount
		FROM pivot_values
		
		UNION DISTINCT
		
		SELECT
            franchise,
            'ALL' AS product,
            budget_year,
            budget_month,
            reporting_month,
            metric_name,
            unit,
            CASE WHEN metric_name != 'Margin' THEN SUM(budget_amount) ELSE AVG(budget_amount) END AS budget_amount,
            reporting_date,
			CASE WHEN metric_name != 'Margin' THEN SUM(daily_budget_amount) ELSE AVG(daily_budget_amount) END AS daily_budget_amount,
            CASE WHEN metric_name != 'Margin' THEN SUM(cumulative_budget_amount) ELSE AVG(cumulative_budget_amount) END AS cumulative_budget_amount
		FROM pivot_values
        WHERE product != 'ALL'
		GROUP BY franchise, product, budget_year, budget_month, reporting_month, metric_name, unit, reporting_date