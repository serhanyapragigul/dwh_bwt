{{ config(schema='dwh', materialized='table')}}

SELECT
    external_reference_id,
    player_id, 
    reference,
    type,
    service
FROM {{ ref('stg_external_references') }}