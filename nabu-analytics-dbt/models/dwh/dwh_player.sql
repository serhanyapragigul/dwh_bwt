{{ config(schema='dwh')}}

SELECT	DISTINCT player_key,
		player_id,
		businessunit_id,
		first_name,
		last_name,
		display_name,
		gender,
		address,
		country_code,
		city,
		postal_code,
		currency_code,
		phone_number,
		email,
		terms,
		privacy_policy,
		kyc_status,
		player_status,
		--eligibilities, no need for now, it is an array and should me moved to a separate fact table if need be
		birth_date,
		registration_date,
		affiliate_tag,
		manual_affiliate_tag,
		date_updated,
		updated_by,
		updated_by_username,
		test_account,
		numeric_id,
		type
FROM {{ ref('stg_player') }}
--temp solution to avoid multiple franchizes per player_id. it's a test BU and doesn't appear anywhere downstream
WHERE businessunit_id <> '5bc0e015-7031-4214-9676-1ceaf398dd03'