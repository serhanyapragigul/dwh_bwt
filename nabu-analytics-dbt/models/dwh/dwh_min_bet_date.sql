{{ 
    config(
        schema='dwh'
        )
}}

-- we'll look back in time 3 days to identify all changed bet_id values
WITH delta AS
(
SELECT
    bet_id,
FROM {{ ref('dwh_bet') }}
WHERE date_updated > TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
)
SELECT
-- find the oldest date we have to use for scanning dwh_bet when f_bet incremental loading is executed
	MIN(b.date_updated) AS min_date
FROM {{ ref('dwh_bet') }} b
JOIN delta ON delta.bet_id = b.bet_id