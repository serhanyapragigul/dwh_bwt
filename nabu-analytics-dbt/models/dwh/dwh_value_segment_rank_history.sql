{{ 
    config(
      schema='dwh', 
      materialized='incremental',
      unique_key='value_segment_rank_history_id',
      cluster_by=['effective_to', 'value_segment'],
      pre_hook="{% if not is_incremental() %} 
          DROP TABLE IF EXISTS 
          nabuminds. 
					{% if target.name == 'prod' %}
					dwh
					{% else %}
					{{target.schema}}
					{% endif %}.tmp_dwh_value_segment_rank_history;
          CREATE TABLE  
					nabuminds.
          {% if target.name == 'prod' %}
					dwh
					{% else %}
					{{target.schema}}
					{% endif %}.tmp_dwh_value_segment_rank_history 
          AS SELECT * FROM  
          nabuminds.
          {% if target.name == 'prod' %}
					dwh
					{% else %}
					{{target.schema}}
					{% endif %}.dwh_value_segment_rank_history {% endif %}"	,
      post_hook="{% if not is_incremental() %} DROP TABLE IF EXISTS 
          nabuminds. 
					{% if target.name == 'prod' %}
					dwh
					{% else %}
					{{target.schema}}
					{% endif %}.tmp_dwh_value_segment_rank_history {% endif %}"		
    )
}}
-- depends_on: {{ ref('stg_value_segment_rank') }}

{%- set tmp_table_name = 'nabuminds.dwh.tmp_dwh_value_segment_rank_history' if target.name == 'prod' else 'nabuminds.' + target.schema + '.tmp_dwh_value_segment_rank_history' -%}
{%- set table_name = 'nabuminds.dwh.dwh_value_segment_rank_history' if target.name == 'prod' else 'nabuminds.' + target.schema + '.dwh_value_segment_rank_history' -%}

{%- call statement('need_update', True) -%}
{% if not is_incremental() %}
        CREATE TABLE IF NOT EXISTS 
          nabuminds. 
					{% if target.name == 'prod' %}
					dwh
					{% else %}
					{{target.schema}}
					{% endif %}.dwh_value_segment_rank_history
          (
              value_segment_rank_history_id STRING,
              value_segment STRING, 
              rank_num INTEGER,
              effective_from DATE,
              effective_to DATE
          )
          CLUSTER BY effective_to, value_segment; 
    {% endif %}
    SELECT CASE WHEN count(*) > 0 THEN true ELSE FALSE END as res
      FROM {{ref('stg_value_segment_rank')}} AS new_t
      FULL 
      JOIN (select * from {{table_name}} where effective_to = DATE('9999-12-31')) AS old_t
        ON new_t.value_segment = old_t.value_segment
       AND new_t.rank_num = old_t.rank_num
     WHERE new_t.value_segment IS NULL 
        OR old_t.value_segment IS NULL 
{%- endcall -%}
{% set need_update = load_result('need_update')['data'][0][0] %}

{% if need_update-%}
SELECT CAST(FARM_FINGERPRINT(CONCAT(value_segment, rank_num, effective_from)) AS string) AS value_segment_rank_history_id,
       value_segment,
       rank_num,
       effective_from,
       effective_to
from (
  SELECT
          value_segment,
          rank_num,
          effective_from,
          COALESCE(CASE WHEN effective_to = DATE('9999-12-31') THEN DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY)  ELSE effective_to END, DATE('9999-12-31')) AS effective_to
          FROM {% if not is_incremental() %} {{tmp_table_name}} {% else %} {{this}} {% endif %}
           {% if is_incremental() %}
          WHERE effective_to = DATE('9999-12-31')
                  {% endif %}
       
  UNION ALL 
    
  SELECT  value_segment,
          rank_num,
            CASE WHEN cnt.cnt > 0 THEN CURRENT_DATE() ELSE DATE('1900-01-01') END AS effective_from,
            DATE('9999-12-31') AS effective_to

    FROM  {{ref('stg_value_segment_rank')}}   AS new_t
    CROSS JOIN (select count(*) as cnt FROM {% if not is_incremental() %} {{tmp_table_name}} {% else %} {{this}} {% endif %}) as cnt 
)

{%- else -%}
SELECT value_segment_rank_history_id,
       value_segment,
       rank_num,
       effective_from,
       effective_to
   FROM {% if is_incremental() %} {{table_name}} {% else %} {{tmp_table_name}} {% endif %}
   WHERE {{not is_incremental()}}
{% endif %}
