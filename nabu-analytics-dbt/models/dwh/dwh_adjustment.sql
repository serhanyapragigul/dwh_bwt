{{
	config(
		schema='dwh',
		materialized='incremental',
		unique_key = 'adjustment_key',
		cluster_by = ['date_updated']
		)
}}



SELECT 
DISTINCT 
    a.adjustment_key, 
    a.adjustment_id, 
    a.player_id, 
    a.business_unit_id, 
    t.adjustment_type_id,
    t.adjustment_type_name, 
    a.amount, 
    a.currency,
     a.strategy, 
     a.transaction_amount, 
     a.transaction_currency, 
     a.base_currency, 
     a.base_amount, 
     a.date_created, 
     a.date_updated, 
     a.adjustment_request_id, 
     a.status, 
     a.comment, 
     a.created_by, 
     a.changed_by

FROM {{ ref('stg_adjustment') }} a


left join
{{ ref('stg_adjustment_type') }}  t
    on a.adjustment_type_id = t.adjustment_type_id and t.row_no = 1
    --reads first row from Adjustment_type, that row holds the name
	
{% if is_incremental() %}
    -- this filter will only be applied on an incremental run
	-- in order to be able to benefit from clustering we do not query the MAX(date_update) value but use
	-- look back 3 days
WHERE partition_time >= TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
{% endif %}
