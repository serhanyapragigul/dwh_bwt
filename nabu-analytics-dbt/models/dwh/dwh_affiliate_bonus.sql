{{ config(schema='dwh', materialized='table') }}

SELECT 0 as bonus_type_id, 'Generic Bonus' as	bonus_type_name, 'GENERIC' as Bonus_type
UNION ALL
SELECT 1, 'Welcome Bonus', 'WELCOME'
UNION ALL
SELECT 2, 'Amount Bonus', 'AMOUNT'
UNION ALL
SELECT 3, 'Deposit Bonus', 'DEPOSIT'
UNION ALL
SELECT 4, 'Lockup Bonus', 'LOCKUP'
UNION ALL
SELECT 99, 'Customer Rewards', 'CUSTOMER_REWARD'