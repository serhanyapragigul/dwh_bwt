{{ config(schema='dwh', materialized='table')}}


SELECT

        type,
        bonus_configuration_id,
        bonus_configuration_name,
        business_unit_id,
        bonus_type,
        bonus_delivery_method,
        bonus_claim_time_seconds,
        bonus_life_time_seconds,
        percent_bonus_given,
        bonus_money_first,
        --bonus_currency_configs,
        --bonus_valid_games,
        --method_types,
        title_key,
        body_key,
        terms_and_conditions_key,
        image_id,
        date_updated,
        updated_by,
        updated_by_username,
        --bonus_valid_game_subclassifications,
        status,
        image_url
        --valid_game_subclassifications,
       


    FROM {{ ref('stg_bonus_configuration') }}

        