{{
	config(
		schema='dwh',
		materialized='incremental',
        unique_key='event_id',
        cluster_by=['event_id']
		)
}}

-- depends_on: {{ ref('dwh_min_bet_date') }}
-- depends_on: {{ ref('dwh_changed_bet_id') }}

/*
We get last received event_start_date from the last placed bet, it increases probability of picking the right date
If a match was moved to another date, for example, and some bets were already placed,
they can still have incorrect dates even in further kafka UPDATE messages
*/
SELECT  DISTINCT event_id, 
        LAST_VALUE(CAST(event_start_date AS TIMESTAMP) IGNORE NULLS) OVER (PARTITION BY event_id ORDER BY date_created, date_updated ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as event_start_date 
FROM (
    /*
    Here we still need all transcations withing a single bet becuase for cashed_out bets event_id changes (to 7-digit numeric one)
    We can't find any explanation or documentation on this
    */
    SELECT  DISTINCT b.coupon_id, 
            b.bet_id,
            b.date_updated,
            b.date_created,
            b.selection_num, 
            LOWER(ARRAY_TO_STRING(REGEXP_EXTRACT_ALL(b.selection_attribute, '[A-Z]*[a-z]*'), '_')) AS selection_attribute, --changing CamelCase to snake_case
            b.last_transaction_details_value  
    FROM    (
                /*
                EventId and EventStartDate can be found in last_transaction_details key-value pairs
                They are enumerated by selection numbers in keys, e.g. Selection_0_EventId
                */
                SELECT  SPLIT(b.last_transaction_details_key, '_')[SAFE_ORDINAL(2)] AS selection_num,
                        SPLIT(b.last_transaction_details_key, '_')[SAFE_ORDINAL(3)] AS selection_attribute,
                        b.last_transaction_details_value,
                        b.bet_id,
                        b.date_updated,
                        /*
                        coupon_id and date_created should be defined at bet level because:
                        1. coupon_id might not be provided in kafka UPDATE messages
                        2. original created_date field is not provided in kafka UPDATE messages, so we should use first date_updated at bet level
                        */
                        LAST_VALUE(
                            COALESCE(   
                                    b.ticket_id,
                                    CASE WHEN b.last_transaction_details_key = 'CouponId' THEN b.last_transaction_details_value END 
                                    ) 
                        IGNORE NULLS
                        ) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS coupon_id,
                        FIRST_VALUE(b.date_updated IGNORE NULLS) OVER (PARTITION BY b.bet_id ORDER BY b.date_updated ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_created 
                FROM    {{ ref('dwh_bet') }} AS b
                WHERE   b.provider_type = 'SPORTSBOOK'
                            AND b.last_transaction_details_key IS NOT NULL
                            {% if is_incremental() %}
                            -- this filter will only be applied on an incremental run
                                AND b.date_updated >= (SELECT min_date FROM {{ ref('dwh_min_bet_date') }}) -- this filter reduces bytes billed by three times
                                    AND b.bet_id IN (SELECT bet_id FROM {{ ref('dwh_changed_bet_id') }}) -- this filter only reduces number of records to be updated, doesn't affect bytes billed      
                            {% endif %}
    ) AS b
) AS attributes
/*
we need only event_start_date from bets because it's missing in sportsbook_bets topic
*/
PIVOT(MAX(attributes.last_transaction_details_value) FOR attributes.selection_attribute IN ('event_id', 'event_start_date'))
WHERE       event_id IS NOT NULL

