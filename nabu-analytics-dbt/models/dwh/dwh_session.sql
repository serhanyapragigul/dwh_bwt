
{{ config(
		schema='dwh',
		materialized='incremental',
		unique_key = 'session_key',
		cluster_by = 'date_created')
}}

SELECT
DISTINCT
        type,
        session_key,
        session_id,
        player_id,
        business_unit_id,
        date_created,
        date_expiration,
        country,
        city,
        ip,
        user_agent,
        browser,
        browser_version,
        platform,	
        platform_version,
        device_type
FROM  {{ ref('stg_session') }} s
{% if is_incremental() %}
    -- this filter will only be applied on an incremental run
	-- in order to be able to benefit from clustering we do not query the MAX(date_update) value but use
	-- look back 3 days
WHERE s.partition_time >= TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
{% endif %}

	


