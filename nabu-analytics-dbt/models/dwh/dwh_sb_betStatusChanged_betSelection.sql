{{ config(schema='dwh', materialized='table')}}

SELECT
bet_Status_Changed_bet_coupon_Id as coupon_id,
bet_Status_Changed_bet_bet_Id as bet_id,
bet_status_changed_bet_bet_status as bet_status,
bet_status_changed_bet_result_status result_status,
bet_Status_Changed_changed_Date as date_changed,
bet_Status_Changed_date_time_stamp as date_time_stamp,
bet_status_changed_bet_closed_reason as bet_closed_reason,
TRIM(betSelections.betSelectionId) as bet_selection_id,
TRIM(betSelections.marketSelection.marketSelectionId) as market_selection_id,
TRIM(betSelections.marketSelection.marketSelectionName) as market_selection_name,
TRIM(betSelections.marketSelection.eventId) as event_id,
REGEXP_REPLACE(NORMALIZE(TRIM(betSelections.marketSelection.eventName), NFD), r'\pM', '') as event_name,
TRIM(betSelections.marketSelection.gamePhase) as game_phase,
TRIM(betSelections.marketSelection.sportId) as sport_id,
TRIM(betSelections.marketSelection.sportName) sport_name,
TRIM(betSelections.marketSelection.competitionId) as competition_id,
REGEXP_REPLACE(NORMALIZE(TRIM(betSelections.marketSelection.competitionName), NFD), r'\pM', '') as competition_name,
TRIM(betSelections.marketSelection.marketId) as market_id,
TRIM(betSelections.marketSelection.marketName) as market_name,
REGEXP_REPLACE(NORMALIZE(TRIM(betSelections.marketSelection.participant), NFD), r'\pM', '') as participant,
TRIM(betSelections.settledStatus) as settled_status,
TRIM(betSelections.voidReason) as void_reason,
CAST(betSelections.odds.value as NUMERIC)  as odds_value,
CAST(betSelections.odds.boostedValue as  NUMERIC)  as odds_boostedValue

FROM {{ ref('stg_sportsbook') }}  
join UNNEST(bet_Status_Changed_bet_bet_Selections) as betSelections
