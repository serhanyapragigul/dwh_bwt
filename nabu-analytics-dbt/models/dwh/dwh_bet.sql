{{
	config(
		schema='dwh',
		materialized='incremental',
		unique_key = 'bet_key',
		cluster_by = ['date_updated','bet_id']
		)
}}

SELECT
	stg_data.bet_key,
	stg_data.bet_id,
	stg_data.business_unit_id,
	stg_data.player_id,
	stg_data.currency_code,
	stg_data.original_currency_code,
	stg_data.game_id,
	stg_data.subgame_id,
	stg_data.internal_game_id,
	stg_data.provider_type,
	stg_data.draw_id,
	stg_data.external_reference,
	stg_data.transaction_status,
	stg_data.initial_bet_amount,
	stg_data.total_win_amount,
	stg_data.jackpot_contribution,
	stg_data.ticket_id,
	stg_data.ticket_reference,
	stg_data.ticket_amount,
	stg_data.ticket_cost,
	stg_data.game_session_key,
	stg_data.idempotency_id,
	stg_data.bet_description,
	stg_data.last_transaction_type,
	stg_data.last_transaction_id,
	stg_data.last_transaction_amount,
	stg_data.last_transaction_original_currency,
	stg_data.last_transaction_timestamp,
	stg_data.last_transaction_bonus,
	stg_data.last_transaction_base_amount,
	stg_data.draw_date,
	stg_data.date_updated,
	stg_data.date_created,
	stg_data.game_uuid,
	stg_data.last_transaction_details_key,
	stg_data.last_transaction_details_value,
	stg_data.type,
	stg_data.kafka_insert_time,
	CASE WHEN stg_data.last_transaction_details_key = 'FreeTicketBatchID_0' THEN 'free ticket' ELSE NULL END as free_ticket
FROM (
	SELECT
		b.bet_key,
		-- In case of recieving duplicate rows from Kafka we need to assign row numbers to duplicate rows so that we can pick just one of them later.
		ROW_NUMBER () OVER (PARTITION BY b.bet_key ORDER BY b.kafka_insert_time desc) AS row_nr,
		b.bet_id,
		b.business_unit_id,
		b.player_id,
		b.currency_code,
		b.original_currency_code,
		b.game_id,
		b.subgame_id,
		b.internal_game_id,
		b.provider_type,
		b.draw_id,
		b.external_reference,
		b.transaction_status,
		b.initial_bet_amount,
		b.total_win_amount,
		b.jackpot_contribution,
		b.ticket_id,
		b.ticket_reference,
		b.ticket_amount,
		b.ticket_cost,
		b.game_session_key,
		b.idempotency_id,
		b.bet_description,
		b.last_transaction_type,
		b.last_transaction_id,
		b.last_transaction_amount,
		b.last_transaction_original_currency,
		b.last_transaction_timestamp,
		b.last_transaction_bonus,
		b.last_transaction_base_amount,
		b.draw_date,
		b.date_updated,
		b.date_created,
		b.game_uuid,
		b.last_transaction_details_key,
		b.last_transaction_details_value,
		b.type,
		b.kafka_insert_time
	FROM {{ ref('stg_bet') }} b
	{% if is_incremental() %}
		-- this filter will only be applied on an incremental run
		-- in order to be able to benefit from clustering we do not query the MAX(date_update) value but use
		-- look back 3 days
	WHERE b.partition_time >= TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
	{% endif %}
) stg_data
-- in case of duplicates take only on row
WHERE stg_data.row_nr = 1