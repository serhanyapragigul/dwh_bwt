{{ config(schema='dwh', materialized='table') }}

SELECT 1 as brand_id, 'DiscountCasino' as brand_name, 'ee5cdc34-f0f3-4b8d-aac2-da24f2d804ce' as brand_uuid
UNION ALL
SELECT 2, 'Jetbahis', '24d38a84-b094-4208-8d17-684a847afbdb'
UNION ALL
SELECT 3, 'Rexbet', 'ea74e6a7-10aa-4cc3-9945-a1eb4738c291'
UNION ALL
SELECT 4, 'Jackburst', '9d9374c2-2eae-45cf-87bf-5a1e50cbda06'