{{ config(schema='dwh', materialized='table') }}

/* old classification table
SELECT  0 as ProductID, 0 as SubProductID, 'Player Account' as SubProductName, '-' as ProductName, '-' as BetProvider, '-' as BetSubProduct
UNION ALL
SELECT 1, 1, 'Lottery (Helio)', 'HELIO', 'HELIO', ''
UNION ALL
SELECT 1, 2, 'Casino (Yggrasil)', 'YGGDRASIL'
UNION ALL
SELECT 1, 3, 'Casino (Netent)', 'NETENT'
UNION ALL 
SELECT 1, 4, 'Live Casino (Evolution Gaming)', 'EVOLUTION'
UNION ALL
SELECT 1, 5, 'Live Casino (Netent)', 'NETENT-LIVE'
UNION ALL
SELECT 1, 6, '(iSoftbet)', 'ISOFTBET'
UNION ALL
SELECT 2, NULL, '', 'Sportsbook'
*/


-- new classification tabel to allow direct joining to bets data on providertype and mainclassficiation
--- providerType	mainclassificationname	ProductID	SubProductID	SubProductName	ProductName
            SELECT  'Player Account' as provider_type,	NULL as main_classification_name, 0 as product_id,	0 as sub_product_id,	'Player Account' AS sub_product_name,	'-' as product_name
UNION ALL   SELECT  'EVOLUTION',	NULL,	        1,	2,	    'EVOLUTION',	'GoC (Evolution Gaming)'
UNION ALL   SELECT  'EVOLUTION',	'Casino',	    1,	2,	    'EVOLUTION',	'Casino (Evolution Gaming)'
UNION ALL   SELECT  'EVOLUTION',	'Live Casino',	1,	4,	    'EVOLUTION',	'Live Casino (Evolution Gaming)'
UNION ALL   SELECT  'HELIO',	    NULL,   	    1,	1,	    'HELIO',	    'Lottery (Helio)'
UNION ALL   SELECT  'HELIO',	    'Lotto',	    1,	1,	    'HELIO',	    'Lottery (Helio)'
UNION ALL   SELECT  'ISOFTBET',	    NULL,	        1,	6,  	'ISOFTBET',	    'GoC (iSoftbet)'
UNION ALL   SELECT  'ISOFTBET',	    'Casino',	    1,	6,	    'ISOFTBET',	    'Casino (iSoftbet)'
UNION ALL   SELECT  'NETENT',	    NULL,	        1,	3,	    'NETENT',	    'GoC (Netent)'
UNION ALL   SELECT  'NETENT',	    'Casino',	    1,	3,	    'NETENT',	    'Casino (Netent)'
UNION ALL   SELECT  'NETENT',	    'Live Casino',	1,	5,	    'NETENT-LIVE',	'Live Casino (Netent)'
UNION ALL   SELECT  'SPORTSBOOK',	NULL,	        2,	NULL,	'SPORTSBOOK',	'Sportsbook'
UNION ALL   SELECT  'SPORTSBOOK',	'SPORTSBOOK',	2,	NULL,	'SPORTSBOOK',	'Sportsbook'
UNION ALL   SELECT  'YGGDRASIL',	NULL,	        1,	2,	    'YGGDRASIL',	'GoC (Yggdrasil)'
UNION ALL   SELECT  'YGGDRASIL',	'Casino',	    1,	2,	    'YGGDRASIL',	'Casino (Yggdrasil)'
UNION ALL   SELECT  'PLAYNGO',      'Casino',       1,  7,      'PLAYNGO',      'Casino (PlayNGo)'
UNION ALL   SELECT  'PLAYNGO',      NULL,           1,  7,      'PLAYNGO',      'Casino (PlayNGo)'
UNION ALL   SELECT  'PLAYNGO',      'Live Casino',  1,  7,      'PLAYNGO',      'Live Casino (PlayNGo)'
UNION ALL   SELECT  'PRAGMATIC',    'Casino',       1,  8,      'PRAGMATIC',      'Casino (PRAGMATIC)'
UNION ALL   SELECT  'PRAGMATIC',    NULL,           1,  8,      'PRAGMATIC',      'Casino (PRAGMATIC)'
UNION ALL   SELECT  'PRAGMATIC',    'Live Casino',  1,  8,      'PRAGMATIC',      'Live Casino (PRAGMATIC)'
UNION ALL   SELECT  'MICROGAMING',  NULL,           1,  9,      'MICROGAMING',  'GoC (Microgaming)'
UNION ALL   SELECT  'MICROGAMING',  'Casino',       1,  9,      'MICROGAMING',  'Casino (Microgaming)'

