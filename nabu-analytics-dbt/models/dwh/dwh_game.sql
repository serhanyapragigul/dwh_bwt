{{ config(schema='dwh')}}

SELECT
DISTINCT
	type,
    game_id,
	internal_game_id, 
	sub_classification_name, 
	slug,
	provider_game_id, 
	title, 
	date_published, 
	date_updated,
	gmc.name as main_classification_name,
	main_classification_id,
	gp.name as provider_name,
	provider_id,
	gm.sub_provider_id
FROM {{ ref('stg_game') }} as gm

left join
	(SELECT gmc_id, name from {{ ref('stg_game_main_classifications') }} where type = 'CREATED') gmc
	on gm.main_classification_id = gmc.gmc_id
	
left join
	(SELECT game_provider_id, name from {{ ref('stg_game_providers')}} where type = 'CREATED') gp
	on gm.provider_id = game_provider_id
