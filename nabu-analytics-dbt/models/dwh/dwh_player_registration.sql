{{ config(schema='dwh')}}

SELECT
DISTINCT
  player_registration_id, 
  player_id, 
  business_unit_id,
  email, 
  first_name, 
  last_name, 
  gender, 
  address, 
  postal_code, 
  city, 
  country, 
  phone_code, 
  phone_number, 
  currency,
  language, 
  birth_date, 
  terms,
  registered_date,
  session_id,
  user_ip, 
  privacy_policy,
  affiliate_tag 
FROM  {{ ref('stg_player_registration') }} 