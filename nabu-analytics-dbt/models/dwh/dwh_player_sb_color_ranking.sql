{{ config(schema='dwh', materialized='table') }}

SELECT 1 AS rank_id, 'red' AS color_name, 'Winner' AS color_desc, DATE('1900-01-01 00:00:00') AS effective_from, DATE('9999-12-31 00:00:00') AS effective_to
UNION ALL
SELECT 2 AS rank_id, 'pink' AS color_name, 'Pro' AS colore_desc, DATE('1900-01-01 00:00:00') AS effective_from, DATE('9999-12-31 00:00:00') AS effective_to
UNION ALL
SELECT 3 AS rank_id, 'purple' AS color_name, 'Leisure - playing in free time, not addicted' AS colore_desc, DATE('1900-01-01 00:00:00') AS effective_from, DATE('9999-12-31 00:00:00') AS effective_to
UNION ALL
SELECT 4 AS rank_id, 'blue' AS color_name, 'New' AS colore_desc, DATE('1900-01-01 00:00:00') AS effective_from, DATE('9999-12-31 00:00:00') AS effective_to
UNION ALL
SELECT 5 AS rank_id, 'green' AS color_name, 'Big Loser' AS color_desc, DATE('1900-01-01 00:00:00') AS effective_from, DATE('9999-12-31 00:00:00') AS effective_to
UNION ALL
SELECT 6 AS rank_id, 'grey' AS color_name, 'Normal' AS color_desc, DATE('1900-01-01 00:00:00') AS effective_from, DATE('9999-12-31 00:00:00') AS effective_to
