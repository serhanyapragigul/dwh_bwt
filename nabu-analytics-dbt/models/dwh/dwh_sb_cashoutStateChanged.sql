{{ config(schema='dwh', materialized='table')}}


SELECT
    DISTINCT
    cashout_state_changed_cashout_state as changed_cashout_state,
    cashout_state_changed_bet_bet_id as bet_id

FROM {{ ref('stg_sportsbook') }}  
