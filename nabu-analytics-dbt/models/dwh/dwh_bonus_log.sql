{{
	config(
		schema='dwh',
		materialized='table',
        unique_key = 'bonus_log_key',
        cluster_by = 'date_created'
		)
}}

SELECT  DISTINCT bonus_log_key,
        type, 
        bonus_log_id,
        bet_id,
        bet_transaction_id,
        transaction_id ,
        bonus_id ,
        bet_amount, 
        bonus_amount_taken, 
        locked_amount_taken, 
        transaction_amount_correction, 
        withdrawable_amount_taken,
        wager_type,
        wagered,
        status,
        date_created
FROM {{ ref('stg_bonus_log') }}


