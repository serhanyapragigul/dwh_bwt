{{ config(schema='dwh')}}

SELECT
    type,
    sub_provider_id,
    sub_provider_name,
    date_updated,
    updated_by,
    updated_by_username
FROM {{ref('stg_game_sub_providers')}}