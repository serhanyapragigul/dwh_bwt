{{
config(
    schema='dwh',
    materialized='table',
    cluster_by=['base_currency', 'start_date', 'end_date'],
    )
}}
-- distinct avoids possible duplicates as start_date ,
SELECT	DISTINCT base_currency,
		currency,
		exchange_rate ,
		start_date,
		LEAD(start_date, 1, '9999-12-31') OVER (PARTITION BY base_currency,currency ORDER BY start_date ASC) AS end_date
		--make this load incremental in future
FROM {{ ref('stg_exchange_rate') }}
WHERE currency = 'EUR'