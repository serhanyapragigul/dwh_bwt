{{ config(
    schema='dwh',
    materialized='incremental',
    unique_key = 'segmentation_guid',
    partition_by={
      "field": "execution_date",
      "data_type": "date",
      "granularity": "day"
    }
)}}


SELECT {{ dbt_utils.surrogate_key(['player_id', 'business_unit_id', 'execution_date'])}} as segmentation_guid, 
       player_id,
       franchise_name,
       business_unit_id,
       most_staked_sport,
       most_staked_league,
       most_staked_market,
       most_staked_team,
       odd_preference,
       stake_preference,
       bet_type,
       played_sport_variety,
       DATE(execution_time) AS execution_date, 
       execution_time
  FROM 
      (
      SELECT player_id,
             franchise_name,
             business_unit_id,
             most_staked_sport,
             most_staked_league,
             most_staked_market,
             most_staked_team,
             odd_preference,
             stake_preference,
             bet_type,
             played_sport_variety,
             date(execution_time) as execution_date, 
             execution_time
        FROM {{ ref('stg_sb_sub_segment') }} a
        {% if is_incremental() %}
       WHERE execution_time > (SELECT MAX(execution_time) FROM {{this}})
        {% endif %}
      ) a
  WHERE 1 = 1 
  QUALIFY ROW_NUMBER() OVER (PARTITION BY player_id, business_unit_id, execution_time) = 1