{{
	config(
		schema='dwh',
		materialized='incremental',
		unique_key = 'transaction_key',
		cluster_by = 'date_created'
		)
}}
SELECT	type,
		transaction_key,
		transaction_id, 
		business_unit_id ,
		player_id,
		transaction_type ,
		sub_type,
		category,
		provider_type,
		provider_game_id, 
		title,
		currency,
		amount,
		balance, 
		locked,
		bonus_balance,
		bonus_locked,
		info, 
		internal_game_id, 
		game_round,
		internal_reference,
		external_reference,
		date_created,
		bonus_id, 
		kafka_insert_time
FROM  {{ ref('stg_transaction') }}  tr
WHERE true
{% if is_incremental() %}
    -- this filter will only be applied on an incremental run
	-- in order to be able to benefit from clustering we do not query the MAX(date_update) value but use
	-- look back 3 days
AND tr.partition_time >= TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
{% endif %}
QUALIFY row_number() OVER (PARTITION BY transaction_key ORDER BY kafka_insert_time desc) = 1
