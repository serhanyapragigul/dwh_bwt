{{ config(
		schema='dwh',
		materialized='incremental',
		unique_key = 'game_session_key',
		cluster_by = 'date_updated')
}}

SELECT	type,
		game_session_key,
		game_session_id,
		business_unit_id,
		game_id,
		internal_game_id,
		player_id,
		date_updated,
		session_id,
		status,
		kafka_insert_time
FROM	{{ ref('stg_game_session') }} AS s
WHERE TRUE
{% if is_incremental() %}
    -- this filter will only be applied on an incremental run
	-- in order to be able to benefit from clustering we do not query the MAX(date_update) value but use
	-- look back 3 days
	AND s.partition_time >= TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
--and tr.date_updated > (SELECT MAX(date_updated) FROM {{ this }})
{% endif %}
QUALIFY ROW_NUMBER() OVER (PARTITION BY game_session_key ORDER BY kafka_insert_time) = 1