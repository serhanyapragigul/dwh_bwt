{{
	config(
		schema='dwh',
		materialized='incremental', 
		unique_key = 'payment_key',
		cluster_by = 'date_updated'
		)
}}
--should be incremental
SELECT  DISTINCT    pay.`type`,
	    pay.payment_key,
        pay.payment_id, 
        pay.business_unit_id, 
        pay.player_id, 
        pay.provider_id,
        pay.method_type,
        pay.payment_account_id,
        pay.session_id,
        pay.transaction_amount,
        pay.transaction_currency,
        pay.transaction_fee,
        pay.amount,
        pay.currency, 
        pay.fee,
        pay.base_amount,
        pay.base_currency, 
        pay.payment_type, 
        pay.payment_status,
        pay.external_reference,
        pay.reason, 
        pay.external_payment_account_id,
        pay.payment_channel_id,
        chan.payment_channel_name,
        pay.adjust_balance,
        pay.date_updated,
        pay.updated_by,
        pay.updated_by_username,
        pay.bonus_id,
        pay.sub_provider,
        pay.desired_amount,
        pay.desired_currency,
        pay.operational_amount,
        pay.operational_currency
    FROM {{ ref('stg_payment') }} as pay
        LEFT JOIN (
                    SELECT  payment_channel_id, 
                            payment_channel_name 
                        FROM {{ ref('stg_payment_channel') }}  
                    WHERE `type` = 'CREATED'
                ) as chan 
            ON pay.payment_channel_id = chan.payment_channel_id
{% if is_incremental() %}
    -- this filter will only be applied on an incremental run
--WHERE COALESCE (date_updated) > (SELECT MAX(COALESCE (date_updated)) FROM {{ this }})
WHERE   pay.partition_time >= TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
        AND pay.date_updated > (SELECT MAX(date_updated) FROM {{ this }})
{% endif %}