{{ config(schema='dwh', materialized='table') }}

SELECT  
        distinct
        bet_cashedout_cashout_id as cashout_id,
        bet_cashedout_cashout_state as cashout_state,
        bet_cashedout_date_time_stamp,
        bet_cashedout_processed_date,
        bet_cashedout_bet_coupon_id as coupon_id,
        bet_cashedout_bet_bet_id as bet_id,
        payout.amount.baseCurrency.currencyCode as currencyCode_EUR,
        CAST(payout.amount.baseCurrency.amount as NUMERIC) as amount_EUR,
        payout.amount.userCurrency.currencyCode,
        CAST(payout.amount.userCurrency.amount as NUMERIC) as amount 

FROM {{ ref('stg_sportsbook') }}
JOIN UNNEST(bet_cashedout_bet_payouts_total) as payout

