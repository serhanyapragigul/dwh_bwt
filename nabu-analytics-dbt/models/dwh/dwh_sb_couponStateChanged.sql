{{ config(schema='dwh', materialized='table')}}


SELECT

coupon_state_changed_segment_id as segment_id,
coupon_state_changed_correlation_id as correlation_id,
coupon_state_changed_brand_id as brand_id,
coupon_state_changed_date_time_stamp as date_time_stamp,
coupon_state_changed_changed_date as date_changed,
coupon_state_changed_coupon_coupon_id as coupon_id,
coupon_state_changed_coupon_coupon_status as coupon_status,
TRIM(bets.betId) as bet_id

FROM {{ ref('stg_sportsbook') }}  
left join unnest(coupon_state_changed_coupon_bets) as bets
where coupon_state_changed_coupon_coupon_id is not null

