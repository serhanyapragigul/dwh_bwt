{{
	config(
		schema='dwh',
		materialized='incremental',
		unique_key = 'bet_id'
		)
}}

SELECT
    bet_id
  , game_uuid
  , game_providerGameId
  , provider_name
  , provider_id
  , provider_type
  , sub_classification_name
  , bets_provider_game_id
  , bets_provider_type
  , bets_internal_game_id
  , bets_sub_game_id
  , bets_created
  , bets_kafka_topic
  , bets_kafka_partition
  , bets_kafka_offset
  , bets_kafka_insert_time
  , bets_kafka_row_unique_id
FROM {{ ref('stg_bet_withgameuuid_20201117') }} b
WHERE 1 = 1
{% if is_incremental() %}

   AND b.bets_kafka_insert_time > (SELECT MAX(bets_kafka_insert_time) FROM {{ this }})

{% endif %}

QUALIFY ROW_NUMBER () OVER (PARTITION BY b.bet_id ORDER BY b.bets_kafka_insert_time DESC) = 1