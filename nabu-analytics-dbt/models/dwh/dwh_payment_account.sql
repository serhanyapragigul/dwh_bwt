{{ config(schema='dwh')}}

SELECT      DISTINCT    `type`,
            payment_accounts_id,
            player_id,
            external_account_id,
            method_type,
            kyc_status,
            account_status,
            multi_usage,
            date_updated,
            country,
            crypto_currency,
            destination_tag
      FROM {{ ref('stg_payment_account') }}
    